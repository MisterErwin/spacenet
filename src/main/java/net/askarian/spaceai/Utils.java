package net.askarian.spaceai;

/**
 * Created by alex on 10/29/15.
 */
public class Utils {

    public static <T> boolean contains(final T[] array, final T v) {
        if (v == null) {
            for (final T e : array)
                if (e == null)
                    return true;
        } else {
            for (final T e : array)
                if (e == v || v.equals(e))
                    return true;
        }

        return false;
    }

    public static boolean containsPrimitive(final int[] array, final Integer v) {
        for (final int e : array)
            if (e == v || v.equals(e))
                return true;
        return false;
    }

    public static boolean containsPrimitive(final Integer[] array, final int v) {
        for (final Integer e : array)
            if (e == v || e.equals(v))
                return true;

        return false;
    }

    public static int maxValue(final int[] array) {
        int ret = 0;
        for (int i : array)
            if (i > ret)
                ret = i;
        return ret;
    }

    public static int maxIndex(final int[] array) {
        int ret = 0;
        int index = -1;
        for (int i = 0; i < array.length; ++i)
            if (array[i] > ret) {
                ret = array[i];
                index = i;
            }
        return index;
    }
}
