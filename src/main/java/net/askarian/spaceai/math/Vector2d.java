package net.askarian.spaceai.math;

import org.apache.commons.lang.math.RandomUtils;

/**
 * Created by alex on 9/28/15.
 */
public class Vector2d{
    private double x,y;


    public Vector2d(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2d(){
        this(0,0);
    }

    public Vector2d addSelf(Vector2d other){
        x += other.x;
        y += other.y;
        return this;
    }

    public Vector2d addScaledSelf(Vector2d other, double scale){
        x += other.x*scale;
        y += other.y*scale;
        return this;
    }

    public Vector2d divideSelf(double divisor){
        x /= divisor;
        y /= divisor;
        return this;
    }

    public Vector2d multSelf(double multiplier){
        x *= multiplier;
        y *= multiplier;
        return this;
    }

    public double distanceSQR(Vector2d other){
        return quad(x-other.x) + quad(y-other.y);
    }

    public double distanceSQR(double otherX, double otherY){
        return quad(x-otherX) + quad(y-otherY);
    }


    public double distance(Vector2d other){
        return Math.sqrt(distanceSQR(other));
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    private static double quad(double d){
        return d*d;
    }

    public void clear() {
        x=y=0;
    }

    public double length(){
        return Math.sqrt(x*x + y*y);
    }

    public Vector2d normalizeSelf(){
        double length = length();
        x = x/length;
        y = y/length;
        return this;
    }


    public Vector2d trimSelf(double desiredLength){
        return normalizeSelf().multSelf(desiredLength);
    }

    @Override
    public Vector2d clone() {
        return new Vector2d(x,y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector2d vector2d = (Vector2d) o;

        if (Double.compare(vector2d.x, x) != 0) return false;
        return Double.compare(vector2d.y, y) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public boolean isZero() {
        return x==0&&y==0;
    }

    @Override
    public String toString() {
        return "Vector2d{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public static Vector2d random(double length) {
        return new Vector2d(RandomUtils.nextDouble()-.5, RandomUtils.nextDouble()-.5).trimSelf(length);
    }

    public double toAngle() {
        return Math.atan2(y,x);
    }
}
