package net.askarian.spaceai.FlowAIServer.programs;

import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Created by alex on 11/3/15.
 */
public class ProgramNode<T extends FlowComponent> {
    public static int FLAG_RUNNING = 1;
    Program program;
    int ID;
    IP[] inputPorts; //{<portNr>: value, ...}
    PortRef[][] outputPortPointers; //{<portNr>: [<NodeAID>,<NodeBID>,...], ...}
    public int flags = 0;
    T flowComponent;


    public ProgramNode(int ID, IP[] inputPorts, PortRef[][] outputPortPointers, T flowComponent) {
        this.ID = ID;
        this.inputPorts = inputPorts;
        this.outputPortPointers = outputPortPointers;
        this.flowComponent = flowComponent;
    }

    public void finish(Object[] output) {
        this.finish(output, 0);
    }

    public void finish(Object[] output, long delay) {
        if ((this.flags & FLAG_RUNNING)==FLAG_RUNNING){
            return;
        }
        for (int portNR = 0; portNR < output.length && portNR < outputPortPointers.length; ++portNR)
            if (output[portNR] != null && outputPortPointers[portNR] != null)
                for (PortRef nextNode : outputPortPointers[portNR]) {
                    this.flags |= FLAG_RUNNING;
                    program.notifyAboutQueuedUpdate(ID, portNR, nextNode.nextNode, nextNode.portNr, output[portNR], delay);
                    program.queueUpdate(ID, nextNode.nextNode, nextNode.portNr, output[portNR], delay);
                }
        if ((this.flags & FLAG_RUNNING)!=FLAG_RUNNING) {
            program.checkIfEnded();
        }

    }

    public void clearPorts() {
        JSONObject ret = new JSONObject();
        ret.put("ID", getID());
        ret.put("type", "clear");
        JSONArray a = new JSONArray();
        for (int i = 0; i < inputPorts.length; ++i)
            if (!(inputPorts[i] instanceof IP.ConstantIP)) {
                inputPorts[i] = null;
                a.add(i);
            }
        ret.put("ports", a);
        program.sendUpdate(ret);
    }

    public int getOutputsOnPort(int port){
        return port < outputPortPointers.length?  outputPortPointers[port].length:0;
    }

    public void setPortData(int port, Object data) {
        inputPorts[port] = new IP.IPImpl(data);
    }

    public Object getPortData(int port) {
        IP ip = getPortIP(port);
        return (ip != null) ? ip.get() : null;
    }

    public Object getPortDataOrDefault(int port, Object deff) {
        IP ip = getPortIP(port);
        return (ip != null) ? ip.get() : deff;
    }

    public IP getPortIP(int port) {
        return inputPorts[port];
    }

    public int getID() {
        return ID;
    }


    public JSONObject getJSONData(){
        JSONObject ret = new JSONObject();
        ret.put("ID", getID());
        ret.put("type", "set");

        JSONObject a = new JSONObject();
        for (int p=0;p<inputPorts.length;++p){
            if (inputPorts[p] != null)
                a.put(p, inputPorts[p].get());
        }
        ret.put("IP", a);

        return ret;
    }

    public Program getProgram() {
        return program;
    }


    static class PortRef {
        int nextNode, portNr;

        public PortRef(int nextNode, int portNr) {
            this.nextNode = nextNode;
            this.portNr = portNr;
        }
    }
}
