package net.askarian.spaceai.FlowAIServer.programs;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Table;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IComponentDataHolder;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.components.impl.cam.CameraComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.cam.CameraTriggerComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.core.*;
import net.askarian.spaceai.FlowAiServer.components.impl.math.AddComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.math.DivComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.math.MultComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.math.SubComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.out.ColorComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.out.ThrusterComponent;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Map;
import java.util.function.BiConsumer;

/**
 * Created by alex on 11/14/15.
 */
public class ProgramBuilder {

//    private static Map<String, FlowComponent> componentMap = new HashMap<>();

    //Name | Group-Parent or root | self
    private static Table<String, String, IComponentDataHolder> componentTable = HashBasedTable.create();
    private static String root="root";
    static {
        componentTable.put("core", root, new IComponentDataHolder.ComponentGroup("Core functions"));
        componentTable.put("math", root, new IComponentDataHolder.ComponentGroup("Math functions"));
        componentTable.put("array", root, new IComponentDataHolder.ComponentGroup("Collection functions"));

        componentTable.put("struct", root, new Struct());

        componentTable.put("kick", "core", new KickComponent());
        componentTable.put("condition", "core", new CondComponent());
        componentTable.put("store", "core", new StoreComponent());
        componentTable.put("wait", "core", new WaitComponent());
        componentTable.put("add", "math", new AddComponent());
        componentTable.put("sub", "math", new SubComponent());
        componentTable.put("div", "math", new DivComponent());
        componentTable.put("mult", "math", new MultComponent());

        componentTable.put("arraymax", "array", new ArrayMaxComponent());

        componentTable.put("camera", root,new CameraComponent());
        componentTable.put("camera_selftrigger", root,new CameraTriggerComponent());

        componentTable.put("color",root, new ColorComponent());
        componentTable.put("thrusters", root, new ThrusterComponent());


        componentTable.put("debug","core", new DebugComponent());

    }

    public ProgramBuilder() {
        setupChildren(root, null);
    }

    private void setupChildren(String group, JSONObject putinto){
        forEachColumn(group, (name, holder) ->{
            if (putinto != null)
                putinto.put(name, holder.getComponentData());

            if (holder instanceof IComponentDataHolder.ComponentGroup){
                setupChildren(name,(JSONObject) holder.getComponentData().get("children"));
            }
        });
    }

    public void forEachCell(BiConsumer<String, ? super FlowComponent> action) {
        for (Table.Cell<String, String, IComponentDataHolder> c : componentTable.cellSet())
            if (c.getValue() instanceof FlowComponent)
                action.accept(c.getRowKey(), (FlowComponent) c.getValue());
    }

    public void forEachColumn(String column, BiConsumer<String, ? super IComponentDataHolder> action) {
        componentTable.column(column==null?root:column).forEach(action);
    }

    public Program buildProgram(JSONArray components) {
        ProgramNode[] allNodes = new ProgramNode[components.size()];

        components.forEach(e -> buildNode((JSONObject) e, allNodes));

        Program program = new Program(allNodes);
        for (ProgramNode pn : allNodes)
            pn.program = program;

        return program;
    }

    public FlowComponent getComponentByName(String name) {
        Map.Entry<String, IComponentDataHolder> e = Iterables.getFirst(componentTable.row(name).entrySet(), null);
        return e == null ? null : e.getValue() instanceof FlowComponent ? (FlowComponent) e.getValue() : null;
    }

    private boolean buildNode(JSONObject jsonObject, ProgramNode[] ret) {
        int ID = ((Long) jsonObject.get("id")).intValue();
        FlowComponent flowComponent = getComponentByName((String) jsonObject.get("type"));
        if (flowComponent == null)
            System.out.println(jsonObject.get("type"));
        IP[] ips = new IP[flowComponent.getInputPortSize()];

        JSONObject portJO = (JSONObject) jsonObject.get("iP");
        if (portJO != null)
            for (Object port : portJO.keySet())
                ips[Integer.parseInt((String) port)] = new IP.ConstantIPImpl(((JSONArray) ((JSONArray) portJO.get(port)).get(0)).get(1));

        ProgramNode.PortRef[][] outports = new ProgramNode.PortRef[flowComponent.getOutputPortSize()][];

        portJO = (JSONObject) jsonObject.get("oP");
        if (portJO != null)
            for (int port = 0; port < flowComponent.getOutputPortSize(); ++port) {
                JSONArray ars = (JSONArray) portJO.get(Long.toString(port));
                if (ars != null && ars.size() > 0) {
                    ProgramNode.PortRef[] thisPort = new ProgramNode.PortRef[ars.size()];

                    for (int i = 0; i < ars.size(); ++i)
                        thisPort[i] = new ProgramNode.PortRef(((Long) ((JSONArray) ars.get(i)).get(0)).intValue(), ((Long) ((JSONArray) ars.get(i)).get(1)).intValue());

                    outports[port] = thisPort;
                }
            }

        ProgramNode pn = new ProgramNode(ID, ips, outports, flowComponent);

        ret[ID] = pn;
        return true;
    }


    public String getInfo(String name) {
        FlowComponent fc = getComponentByName(name);
        if (fc == null)
            return "Not found";
        else return fc.getInfo();
    }
}
