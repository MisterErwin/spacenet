package net.askarian.spaceai.FlowAIServer.programs;

/**
 * Created by alex on 10/27/15.
 */
public class ProgramTestServer {

    public static void main(String[] args){
        WSStandAloneProgramServer wsProgramServer = new WSStandAloneProgramServer(1122);
        wsProgramServer.start();

        System.out.println("Awaiting");

        while (true)
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }
}