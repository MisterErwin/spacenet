package net.askarian.spaceai.FlowAiServer.components.impl.core;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;

import java.util.Arrays;

/**
 * Created by alex on 11/13/15.
 */
public class DebugComponent extends FlowComponent {

    public DebugComponent(String name) {
        super(name,1,0, new CompJSONHelper("debug").groups("core").colour("gray").newIP("text").required().allowsPort().trigger().build());
    }

    @Override
    public void onInput(final ProgramNode me, final int port) {
        System.out.print("Debug (" + me.getID() + ")>");
        Object o = me.getPortData(port);
        System.out.println(o ==null?"null": o instanceof int[]? Arrays.toString((int[])o) : o instanceof double[]? Arrays.toString((double[])o) : o.toString());
//        me.finish(new Object[]{me.getPortData(port)});
    }

    @Override
    public String getInfo() {
        return "DEBUG \\n\\n Outputs the value to the console";
    }

}
