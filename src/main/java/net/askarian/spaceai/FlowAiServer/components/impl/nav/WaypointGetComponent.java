package net.askarian.spaceai.FlowAiServer.components.impl.nav;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;
import net.askarian.spaceai.server.components.input.Waypoint;

/**
 * Created by alex on 11/15/15.
 */
public class WaypointGetComponent extends FlowComponent {
    public WaypointGetComponent(String name) {
        super(name,1, 1, new CompJSONHelper("getwaypoint").groups("nav waypoint").colour("blue")
                .newIP("#Waypoint\nnumber").required().trigger().newOP("angle\n(in degrees)").newOP("distance").build());
    }


    @Override
    public void onInput(ProgramNode me, int port) {
        if (!IP.isNumber(me.getPortIP(0))) return;

        Waypoint waypoint = me.getProgram().getArtemisEntity().getComponent(Waypoint.class);
        if (waypoint == null) {
            me.getProgram().setAsEnded();
            return;
        }
        int nr = ((Number) me.getPortIP(0)).intValue();
        if (nr >= waypoint.amount || nr<0) return;
        me.finish(new Object[]{waypoint.angle[nr], waypoint.dist[nr]});
    }


}
