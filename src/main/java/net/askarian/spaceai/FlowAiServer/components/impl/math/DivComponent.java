package net.askarian.spaceai.FlowAiServer.components.impl.math;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;

/**
 * Created by alex on 11/14/15.
 */
public class DivComponent extends FlowComponent {
    public DivComponent(String name) {
        super(name,3, 1, new CompJSONHelper("Div").groups("arithmetic").newIP("dividend").all().newIP("divisor").all().newIP("clear").allowsPort().newOP("quotient").build());
    }

    @Override
    public void onInput(ProgramNode me, int port) {
        if (port == 2)
            me.clearPorts();
        else if (IP.isNumber(me.getPortIP(0)) && IP.isNumber(me.getPortIP(1))) {
            double b = ((Number) me.getPortData(1)).doubleValue();
            me.finish(new Object[]{b != 0 ? ((Number) me.getPortData(0)).doubleValue() / b : 0});
        }
    }

    @Override
    public String getInfo() {
        return "Divides the dividend by the divisor and outputs the quotient \\n--------------------------\\nclear: Resets the values stored for the dividend and divisor\\ndividend / divisor -> quotient\\nIf the divisor is 0, the result will be 0!";
    }
}
