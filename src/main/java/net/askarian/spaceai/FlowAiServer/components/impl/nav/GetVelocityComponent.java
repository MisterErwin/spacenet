package net.askarian.spaceai.FlowAiServer.components.impl.nav;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;
import net.askarian.spaceai.server.components.physics.Velocity;

/**
 * Created by alex on 11/15/15.
 */
public class GetVelocityComponent extends FlowComponent {
    public GetVelocityComponent(String name) {
        super(name,1, 2, new CompJSONHelper("getvelocity").groups("nav velocity").colour("blue")
                .newIP("trigger").trigger().newOP("angle\n(in degrees)").newOP("length").build());
    }


    @Override
    public void onInput(ProgramNode me, int port) {

        Velocity v = me.getProgram().getArtemisEntity().getComponent(Velocity.class);
        if (v == null) {
            me.getProgram().setAsEnded();
            return;
        }
        me.finish(new Object[]{Math.toDegrees(v.vector.toAngle()), v.vector.length()});
    }


}
