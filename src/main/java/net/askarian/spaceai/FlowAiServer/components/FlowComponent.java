package net.askarian.spaceai.FlowAiServer.components;

import lombok.AccessLevel;
import lombok.Getter;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;
import org.json.simple.JSONObject;

/**
 * Created by alex on 11/3/15.
 */
public abstract class FlowComponent implements IComponentDataHolder {

    private final int IPS, OPS;
    private final JSONObject jsonDesc;
    private @Getter(AccessLevel.PUBLIC) final String name;

    public FlowComponent(String name, int inputPortSize, int outputPortSize, JSONObject jsonDesc) {
        this.IPS = inputPortSize;
        this.OPS = outputPortSize;
        this.jsonDesc = jsonDesc;
        this.name = name;
    }

    public FlowComponent(String name, int inputPortSize, int outputPortSize, CompJSONHelper compJSONHelper) {
        this.IPS = inputPortSize;
        this.OPS = outputPortSize;
        this.jsonDesc = compJSONHelper.build();
        this.name = name;
    }


    /**
     * @param me   The ProgramNode
     * @param port The port the new data came on
     */
    public abstract void onInput(ProgramNode me, int port);

    public int  getInputPortSize() {
        return IPS;
    }

    public int getOutputPortSize() {
        return OPS;
    }


    public JSONObject getComponentData() {
        return jsonDesc;
    }

    public String getInfo() {
        return "Someone forgot to Implement the info \\n Uups";
    }

//        kick: {
//            title: 'Kick', groups: 'core', inports: [], outports: [{title: 'impulse'}], col: "red"
//        },
//    }

}
