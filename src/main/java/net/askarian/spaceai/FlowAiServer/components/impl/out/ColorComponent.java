package net.askarian.spaceai.FlowAiServer.components.impl.out;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;
import net.askarian.spaceai.server.components.output.Color;
import net.askarian.spaceai.server.components.rendering.Rendered;

/**
 * Created by alex on 11/15/15.
 */
public class ColorComponent extends FlowComponent{
    public ColorComponent(String name) {
        super(name,1, 0, new CompJSONHelper("color").newIP("color").required().allowsPort().trigger().build());
    }

    @Override
    public void onInput(ProgramNode me, final int port) {
        if (!IP.isNumber(me.getPortIP(0)))return;
        Color color = me.getProgram().getComponent(Color.class);
        if (color != null)
            color.setFromIndex(me.getProgram().getComponent(Rendered.class),((Number)me.getPortData(0)).intValue());
    }
}
