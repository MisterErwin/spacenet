package net.askarian.spaceai.FlowAiServer.components.impl.core;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;

/**
 * Created by alex on 11/13/15.
 */
public class Struct extends FlowComponent {

    public Struct(String name) {
        super(name,1, 1, new CompJSONHelper("struct").colour("aqua").groups("pretty flow").newIP("IP").allowsPort().newOP("OP").build());
    }

    @Override
    public void onInput(final ProgramNode me, final int port) {
        me.finish(new Object[]{me.getPortData(0)}, (long) (me.getProgram().getWait() * -0.8));
        me.clearPorts();
    }

    @Override
    public String getInfo() {
        return "Use this component to make your programs pretty";
    }

}
