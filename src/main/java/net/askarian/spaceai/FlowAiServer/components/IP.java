package net.askarian.spaceai.FlowAiServer.components;

import lombok.NoArgsConstructor;
import org.apache.commons.lang.math.NumberUtils;

/**
 * Created by alex on 11/13/15.
 */
public interface IP {
    Object get();

    interface ConstantIP extends IP {
    }

    class ConstantIPImpl extends IPImpl implements ConstantIP {
        public ConstantIPImpl(Object value) {
            super(value);
        }

        public ConstantIPImpl(){
            super(null);
        }
    }

    @NoArgsConstructor
    class IPImpl implements IP {
        public Object o;

        public IPImpl(Object value) {
            if (value instanceof String) {
                int i = NumberUtils.toInt((String) value, Integer.MIN_VALUE);
                if (i != Integer.MIN_VALUE) {
                    o = i;
                    return;
                }
                double d = NumberUtils.toDouble((String) value, Double.MIN_VALUE);
                if (d != Double.MIN_VALUE) {
                    o = d;
                    return;
                }
            }

            this.o = value;
        }

        @Override
        public Object get() {
            return o;
        }
    }

    static boolean isInt(IP ip) {
        return (ip != null && ip.get() instanceof Integer);
    }

    static boolean isNumber(IP ip) {
        return (ip != null && ip.get() instanceof Number);
    }

}


