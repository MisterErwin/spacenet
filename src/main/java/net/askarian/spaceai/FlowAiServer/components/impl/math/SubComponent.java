package net.askarian.spaceai.FlowAiServer.components.impl.math;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;

/**
 * Created by alex on 11/14/15.
 */
public class SubComponent extends FlowComponent {
    public SubComponent(String name) {
        super(name,3, 1, new CompJSONHelper("sub").groups("arithmetic").newIP("minuend").all().newIP("subtrahend").all().newIP("clear").allowsPort().newOP("difference").build());
    }

    @Override
    public void onInput(ProgramNode me, int port) {
        if (port == 2)
            me.clearPorts();
        else if (IP.isNumber(me.getPortIP(0)) && IP.isNumber(me.getPortIP(1)))
            me.finish(new Object[]{((Number)me.getPortData(0)).doubleValue() - ((Number)me.getPortData(1)).doubleValue()});
    }


    @Override
    public String getInfo() {
        return "Subtracts the minuend by the subtrahend and outputs the difference \\n--------------------------\\nclear: Resets the values stored for the minuend and subtrahend\\nminuend - subtrahend -> difference";
    }
}
