package net.askarian.spaceai.FlowAiServer.components.impl.cam;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.programs.Program;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;

/**
 * Created by alex on 11/15/15.
 */
public class CameraTriggerComponent extends FlowComponent {
    public CameraTriggerComponent(String name) {
        super(name,0, 1, new CompJSONHelper("camera_selftriggering").groups("input view").colour("blue")
                .newOP("rawdata\\nType: Array of ints").build());
    }

    @Override
    public void onInput(ProgramNode me, int port) {

    }


    public static void notifyChange(Program program, int[] data){
        try {
            program.onNodeType(CameraTriggerComponent.class, pn -> pn.finish(new Object[]{data}));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
