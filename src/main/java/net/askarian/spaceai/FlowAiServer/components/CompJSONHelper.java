package net.askarian.spaceai.FlowAiServer.components;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Created by alex on 11/14/15.
 */
public class CompJSONHelper {

    private final JSONObject JSON = new JSONObject();

    public CompJSONHelper(String title) {
        JSON.put("title", title);
        JSON.put("inports", new JSONArray());
        JSON.put("outports", new JSONArray());
    }

    public CompJSONHelper groups(String g) {
        JSON.put("groups", g);
        return this;
    }

    public CompJSONHelper colour(String c) {
        JSON.put("col", c);
        return this;
    }

    public InHelper newIP(String title) {
        return new InHelper(this, title);
    }

    public OutHelper newOP(String title) {
        return new OutHelper(this, title);
    }

    public JSONObject build() {
        return JSON;
    }

    public final class OutHelper extends PortHelper {
        public OutHelper(CompJSONHelper p, String title) {
            super(p, title);
        }

        public CompJSONHelper build() {
            JSONArray a = (JSONArray) parent.JSON.get("outports");
            if (a == null)
                parent.JSON.put("inports", (a = new JSONArray()));
            a.add(port);
            return parent;
        }

    }

    public final class InHelper extends PortHelper {
        public InHelper(CompJSONHelper p, String title) {
            super(p, title);
        }

        public InHelper values(String... c) {
            JSONArray a = new JSONArray();
            for (String s : c)
                a.add(s);
            port.put("values", a);
            return allowsConstant();
        }

        public InHelper allowsPort() {
            Integer f = (Integer) port.get("f");
            if (f == null)
                port.put("f", 1);
            else
                port.put("f", f | 1);
            return this;
        }

        public InHelper allowsConstant() {
            Integer f = (Integer) port.get("f");
            if (f == null)
                port.put("f", 2);
            else
                port.put("f", f | 2);
            return this;
        }

        public InHelper required() {
            Integer f = (Integer) port.get("f");
            if (f == null)
                port.put("f", 4);
            else
                port.put("f", f | 4);
            return this;
        }

        public InHelper trigger() {
            Integer f = (Integer) port.get("f");
            if (f == null)
                port.put("f", 8);
            else
                port.put("f", f | 8);
            return this;
        }

        public InHelper all(){
            Integer f = (Integer) port.get("f");
            if (f == null)
                port.put("f", 1|2|4|8);
            else
                port.put("f", f | 1|2|4|8);
            return this;
        }


        public CompJSONHelper build() {
            JSONArray a = (JSONArray) parent.JSON.get("inports");
            if (a == null)
                parent.JSON.put("inports", (a = new JSONArray()));
            a.add(port);
            return parent;
        }

    }

    public abstract class PortHelper {
        CompJSONHelper parent;
        JSONObject port;

        public PortHelper(CompJSONHelper p, String title) {
            this.parent = p;
            this.port = new JSONObject();
            port.put("title", title);
        }

        public PortHelper colour(String c) {
            port.put("col", c);
            return this;
        }

        public abstract CompJSONHelper build();

        public InHelper newIP(String title) {
            return build().newIP(title);
        }

        public OutHelper newOP(String title) {
            return build().newOP(title);
        }

    }


}
