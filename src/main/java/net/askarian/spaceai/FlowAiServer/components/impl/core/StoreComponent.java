package net.askarian.spaceai.FlowAiServer.components.impl.core;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;

/**
 * Created by alex on 11/13/15.
 */
public class StoreComponent extends FlowComponent {

    public StoreComponent(String name) {
        super(name,2, 1, new CompJSONHelper("store").groups("core").newIP("value").required().allowsConstant().allowsPort().newIP("trigger").trigger().newOP("output (Value)").build());
    }

    //value & trigger
    @Override
    public void onInput(final ProgramNode me, final int port) {
        if (port == 1)
            me.finish(new Object[]{me.getPortDataOrDefault(0,0)});
//        else if (port == 0)
//            me.setPortData(0, me.getPortData(0));
    }


    @Override
    public String getInfo() {
        return "Stores a value and outputs it on request \\n Default: 0";
    }
}
