package net.askarian.spaceai.FlowAiServer.components.impl.math;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;

/**
 * Created by alex on 11/14/15.
 */
public class MultComponent extends FlowComponent {
    public MultComponent(String name) {
        super(name,3, 1, new CompJSONHelper("Mult").groups("arithmetic").newIP("multiplier").all().newIP("multiplicand").all().newIP("clear").allowsPort().newOP("product").build());
    }

    @Override
    public void onInput(ProgramNode me, int port) {
        if (port == 2)
            me.clearPorts();
        else if (IP.isNumber(me.getPortIP(0)) && IP.isNumber(me.getPortIP(1)))
            me.finish(new Object[]{((Number)me.getPortData(0)).doubleValue() * ((Number)me.getPortData(1)).doubleValue()});
    }

    @Override
    public String getInfo() {
        return "Multiplies the multiplier and multiplicand and outputs their product \\n--------------------------\\nclear: Resets the values stored for the multiplier and multiplicand\\nmultiplier * multiplicant -> product";
    }
}
