package net.askarian.spaceai.FlowAiServer.components.impl.math;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;

/**
 * Created by alex on 11/14/15.
 */
public class AddComponent extends FlowComponent {
    public AddComponent(String name) {
        super(name, 3, 1, new CompJSONHelper("Add").groups("arithmetic").newIP("addend1").all().newIP("addend2").all().newIP("clear").allowsPort().newOP("sum").build());
    }

    @Override
    public void onInput(ProgramNode me, int port) {
        if (port == 2)
            me.clearPorts();
        else if (IP.isNumber(me.getPortIP(0)) && IP.isNumber(me.getPortIP(1)))
            me.finish(new Object[]{((Number) me.getPortData(0)).doubleValue() + ((Number) me.getPortData(1)).doubleValue()});
    }

    @Override
    public String getInfo() {
        return "Adds two addents and outputs their sum \\n---------------------\\nclear: Resets the values stored for the addents\\naddent1 + addent2 -> sum";
    }
}
