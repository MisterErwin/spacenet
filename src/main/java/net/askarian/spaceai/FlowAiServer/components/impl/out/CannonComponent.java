package net.askarian.spaceai.FlowAiServer.components.impl.out;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;
import net.askarian.spaceai.server.components.output.Cannon;

/**
 * Created by alex on 11/15/15.
 */
public class CannonComponent extends FlowComponent{
    public CannonComponent(String name) {
        super(name,1, 0, new CompJSONHelper("cannon").newIP("dir").required().allowsPort().trigger().build());
    }

    @Override
    public void onInput(ProgramNode me, final int port) {
        if (!IP.isNumber(me.getPortIP(0)))return;
        Cannon cannon = me.getProgram().getComponent(Cannon.class);
        if (cannon!= null)
            cannon.dir = ((Number)me.getPortData(0)).intValue();
    }
}
