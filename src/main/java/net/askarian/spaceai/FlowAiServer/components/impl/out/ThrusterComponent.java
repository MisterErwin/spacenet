package net.askarian.spaceai.FlowAiServer.components.impl.out;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;
import net.askarian.spaceai.server.components.output.Thrusters;

/**
 * Created by alex on 11/15/15.
 */
public class ThrusterComponent extends FlowComponent {
    public ThrusterComponent(String name) {
        super(name,2, 0, new CompJSONHelper("thrusters").newIP("thrust").all().newIP("direction").all().build());
    }

    @Override
    public void onInput(ProgramNode me, final int port) {
        if (port == 0 && IP.isNumber(me.getPortIP(port))) { //Strength
            Thrusters thrusters = me.getProgram().getComponent(Thrusters.class);
            if (thrusters != null) {
                thrusters.strength = ((Number) me.getPortData(port)).doubleValue();
                if (me.getPortIP(1) instanceof IP.ConstantIP && IP.isNumber(me.getPortIP(1)))
                    thrusters.dir = ((Number) me.getPortData(1)).intValue();


            }
        } else if (port == 1 && IP.isNumber(me.getPortIP(port))) { //DIR
            Thrusters thrusters = me.getProgram().getComponent(Thrusters.class);
            if (thrusters != null) {
                thrusters.dir = ((Number) me.getPortData(port)).intValue();
                if (me.getPortIP(0) instanceof IP.ConstantIP && IP.isNumber(me.getPortIP(0)))
                    thrusters.strength = ((Number) me.getPortData(0)).doubleValue();

            }

        }
    }
}
