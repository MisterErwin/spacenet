package net.askarian.spaceai.FlowAiServer.components.impl.core;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;

/**
 * Created by alex on 11/13/15.
 */
public class CondComponent extends FlowComponent {


    public CondComponent(String name) {
        super(name,4, 2, new CompJSONHelper("IF").groups("core condition").colour("aqua")
                .newIP("value A").all()
                .newIP("condition").required().allowsConstant().values("==", "!=", ">", "<", ">=", "<=").colour("blue")
                .newIP("value B").all()
                .newIP("clear").allowsPort()
                .newOP("IF").colour("green").newOP("ELSE").colour("red").build());
    }

    //wait & trigger
    @Override
    public void onInput(final ProgramNode me, final int port) {
        if (port == 0 || port == 2) {
            if (isSet(me)) {
                if (isTrue(me))
                    me.finish(new Object[]{me.getPortData(port), null});
                else
                    me.finish(new Object[]{null, me.getPortData(port)});

            }
        } else if (port == 3) {
            me.clearPorts();
        }
    }



    private static final boolean isSet(ProgramNode me) {
        return (me.getPortIP(0) != null && me.getPortIP(2) != null);
    }

    private static final boolean isTrue(ProgramNode me) {
        switch ((String) me.getPortIP(1).get()) {
            case "==":
                return me.getPortData(0).equals(me.getPortData(2));
            case "!=":
                return !me.getPortData(0).equals(me.getPortData(2));
            case ">":
                return IP.isNumber(me.getPortIP(0)) && IP.isNumber(me.getPortIP(2)) && ((Number) me.getPortData(0)).doubleValue() > ((Number) me.getPortData(2)).doubleValue();
            case "<":
                return IP.isNumber(me.getPortIP(0)) && IP.isNumber(me.getPortIP(2)) && ((Number) me.getPortData(0)).doubleValue() < ((Number) me.getPortData(2)).doubleValue();
            case ">=":
                return IP.isNumber(me.getPortIP(0)) && IP.isNumber(me.getPortIP(2)) && ((Number) me.getPortData(0)).doubleValue() >= ((Number) me.getPortData(2)).doubleValue();
            case "<=":
                return IP.isNumber(me.getPortIP(0)) && IP.isNumber(me.getPortIP(2)) && ((Number) me.getPortData(0)).doubleValue() <= ((Number) me.getPortData(2)).doubleValue();
            default:
                return false;
        }
    }

    @Override
    public String getInfo() {
        return "Describe me :D";
    }

}
