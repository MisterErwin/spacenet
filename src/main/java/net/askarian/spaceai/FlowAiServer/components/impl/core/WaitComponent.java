package net.askarian.spaceai.FlowAiServer.components.impl.core;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;

/**
 * Created by alex on 11/13/15.
 */
public class WaitComponent extends FlowComponent {

    public WaitComponent(String name) {
        super(name, 2, 1, new CompJSONHelper("wait").groups("core").colour("orange").newIP("time\n(And trigger)").all().newIP("trigger").trigger().newOP("onTrigger").build());
    }

    //wait & trigger
    @Override
    public void onInput(final ProgramNode me, final int port) {
        if (IP.isInt(me.getPortIP(0)) && (me.flags & ProgramNode.FLAG_RUNNING)!=ProgramNode.FLAG_RUNNING) { //set delay
//            me.flags = 1;
            new Thread(() -> {
                try {
                    Thread.sleep((Integer) me.getPortData(0));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                me.finish(new Object[]{me.getPortData(port)});
                me.flags = 0;
            }).start();
        }
    }


    @Override
    public String getInfo() {
        return "Starts a timer of n ms (set by \'time\').\\nAny input starts the timer\\n data on the input node -> onTrigger ";
    }
}
