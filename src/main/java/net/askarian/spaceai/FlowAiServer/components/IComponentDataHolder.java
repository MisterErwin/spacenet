package net.askarian.spaceai.FlowAiServer.components;

import org.json.simple.JSONObject;

/**
 * Created by alex on 11/15/15.
 */
public interface IComponentDataHolder {
    JSONObject getComponentData();

    class ComponentGroup implements IComponentDataHolder {
        private JSONObject jsonObject;
        ////            title: 'Math functions', group: true, children: {

        public ComponentGroup(String title) {
            jsonObject = new JSONObject();
            jsonObject.put("title", title);
            jsonObject.put("group", true);
            jsonObject.put("children", new JSONObject());
        }

        @Override
        public JSONObject getComponentData() {
            return jsonObject;
        }
    }

}
