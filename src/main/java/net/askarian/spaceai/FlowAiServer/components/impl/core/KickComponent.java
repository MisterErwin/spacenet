package net.askarian.spaceai.FlowAiServer.components.impl.core;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;

/**
 * Created by alex on 11/13/15.
 */
public class KickComponent extends FlowComponent {

    public KickComponent(String name) {
        super(name,0, 1, new CompJSONHelper("kick").groups("core").colour("red").newOP("impulse").build());
    }

    //wait & trigger
    @Override
    public void onInput(final ProgramNode me, final int port) {
        me.finish(new Object[]{1});
    }


    @Override
    public String getInfo() {
        return "Used to kick-start the program \\n Outputs a 1 when the program starts \\n \\n1 -> impulse";
    }
}
