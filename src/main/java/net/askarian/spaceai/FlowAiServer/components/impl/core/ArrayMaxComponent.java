package net.askarian.spaceai.FlowAiServer.components.impl.core;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;
import net.askarian.spaceai.Utils;

/**
 * Created by alex on 11/14/15.
 */
public class ArrayMaxComponent extends FlowComponent {
    public ArrayMaxComponent(String name) {
        super(name,1, 2, new CompJSONHelper("arrayMax").groups("array").newIP("array").allowsPort().trigger().required().newOP("maxIndex").newOP("maxValue").build());
    }

    @Override
    public void onInput(ProgramNode me, int port) {
        Object o = me.getPortData(0);
        if (!(o instanceof int[])) return;
        int i = Utils.maxIndex((int[]) o);

        me.finish(new Object[]{i, i != -1 ? ((int[]) o)[i] : -1});
    }

//    @Override
//    public String getInfo() {
//        return "Adds two addents and outputs their sum \\n---------------------\\nclear: Resets the values stored for the addents\\naddent1 + addent2 -> sum";
//    }
}
