package net.askarian.spaceai.FlowAiServer.components.impl.nav;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;
import net.askarian.spaceai.server.components.input.Waypoint;
import net.askarian.spaceai.server.components.physics.Position;

/**
 * Created by alex on 11/15/15.
 */
public class WaypointSetComponent extends FlowComponent {
    public WaypointSetComponent(String name) {
        super(name,1, 0, new CompJSONHelper("setwaypoint").groups("nav waypoint").colour("blue")
                .newIP("number").trigger().required().build());
    }


    @Override
    public void onInput(ProgramNode me, int port) {
        if (!IP.isNumber(me.getPortIP(0))) return;

        Waypoint waypoint = me.getProgram().getArtemisEntity().getComponent(Waypoint.class);
        if (waypoint == null) {
            me.getProgram().setAsEnded();
            return;
        }
        int nr = ((Number) me.getPortIP(0)).intValue();
        if (nr >= waypoint.amount) return;
        Position position = me.getProgram().getArtemisEntity().getComponent(Position.class);
        waypoint.waypoints[nr] = position.pos;
    }


}
