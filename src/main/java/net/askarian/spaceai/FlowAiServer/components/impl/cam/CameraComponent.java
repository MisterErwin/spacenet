package net.askarian.spaceai.FlowAiServer.components.impl.cam;

import net.askarian.spaceai.FlowAiServer.components.CompJSONHelper;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.programs.Program;
import net.askarian.spaceai.FlowAiServer.programs.ProgramNode;

/**
 * Created by alex on 11/15/15.
 */
public class CameraComponent extends FlowComponent {
    public CameraComponent(String name) {
        super(name,2, 1, new CompJSONHelper("camera").groups("input view").colour("blue")
                .newIP("data").allowsConstant()
                .newIP("trigger").trigger().required().allowsPort().newOP("rawdata\\nType: Array of ints").build());
    }


    @Override
    public void onInput(ProgramNode me, int port) {
        if (port == 1)
            me.finish(new Object[]{me.getPortData(0)});
    }


    public static void notifyChange(Program program, int[] data){
        program.onNodeType(CameraComponent.class, pn -> pn.setPortData(0, data));
    }
}
