package net.askarian.spaceai.FlowAiServer.programs;

import com.artemis.Component;
import com.artemis.Entity;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.components.impl.core.KickComponent;
import org.java_websocket.WebSocket;
import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by alex on 11/3/15.
 */
public class Program {
     ProgramNode[] nodes;
     int nodeUpdates = 0;
     final int maxUpdates = 10;
     boolean running = true;
     public boolean selfTriggering = true;
    transient Entity entity;

    transient List<WebSocket> watchers = new LinkedList<>();

    private final static int wait = 1000;

    public Program() {
    }

    public void initAfterJSON(){
        for (ProgramNode pn : nodes){
            pn.program = this;
            pn.flowComponent = ProgramBuilder.getComponentByName(pn.flowComponentName);
        }
    }
    public Program(ProgramNode[] nodes) {
        boolean sT = true;
        this.nodes = nodes;
        for (ProgramNode pn : this.nodes) {
            if (pn.flowComponent instanceof KickComponent)
                sT = false;
            pn.program = this;
        }
        this.selfTriggering = sT;
    }


    void queueUpdate(int firedNode, int node, int port, Object value, long delay) {
        if (!running) return;
        if (++nodeUpdates > maxUpdates) {
            setAsEnded();
            throw new TooManyUpdatesException();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(wait + delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                doUpdate(firedNode, node, port, value);
            }
        }).start();
    }

    private void doUpdate(int firedNode, int node, int port, Object value) {
        if (!running) return;
        nodes[firedNode].flags &= ~ProgramNode.FLAG_RUNNING;
        ProgramNode programNode = nodes[node];
        if (programNode.inputPorts.length > port)
            programNode.inputPorts[port] = new IP.IPImpl(value);
        programNode.flowComponent.onInput(programNode, port);
        nodeUpdates--;

        //JSON
        JSONObject ret = new JSONObject();
        ret.put("ID", node);
        ret.put("type", "setP");
        ret.put("port", port);
        ret.put("val", value == null ? null : JSONObject.escape(value.toString()));
        this.sendUpdate(ret);

    }

    public ProgramNode getFirstNodeOfType(Class<? extends FlowComponent> classType) {
        for (ProgramNode pn : nodes)
            if (pn != null && pn.flowComponent.getClass() == classType)
                return pn;
        return null;
    }

    public <T extends FlowComponent> boolean onNodeType(Class<T> classType, Consumer<ProgramNode<T>> consumer) {
        boolean ret = false;
        for (ProgramNode pn : nodes)
            if (pn != null && pn.flowComponent.getClass() == classType) {
                consumer.accept(pn);
                ret = true;
            }
        return ret;
    }

    public void sendUpdate(JSONAware ret) {
        watchers.removeIf(w -> !w.isOpen());
        if (!watchers.isEmpty())
            watchers.forEach(w -> w.send(ret.toJSONString()));
    }

    public void watch(WebSocket watcher) {
        JSONArray a = new JSONArray();
        for (ProgramNode pn : nodes)
            a.add(pn.getJSONData());
        sendUpdate(a);
        watchers.add(watcher);
    }

    public boolean isWatching(WebSocket watcher) {
        return watchers.contains(watcher);
    }

    public void unwatchAll() {
        watchers.clear();
        setAsEnded();
    }

    public void unwatch(WebSocket watcher) {
        watchers.remove(watcher);
        if (watchers.isEmpty())
            setAsEnded();
    }

    public void checkIfEnded() {
        if (!selfTriggering && this.nodeUpdates == 0)
            setAsEnded();
    }

    public void setAsEnded() {
        running = false;
        JSONObject ret = new JSONObject();
        ret.put("type", "error");
        ret.put("msg", "Program run end!");
        sendUpdate(ret);
        watchers.clear();

        ProgramServer.instance.notifyDead(this);
    }


    public Entity getArtemisEntity() {
        return entity;
    }

    public <T extends Component> T getComponent(Class<T> comp) {
        return entity == null ? null : entity.getComponent(comp);
    }


    public void setArtemisEntity(Entity entity) {
        this.entity = entity;
    }

    public void notifyAboutQueuedUpdate(int id1, int port1, int id2, int port2, Object value, long pDdelay) {
        if (watchers.isEmpty())
            return;
        JSONObject ret = new JSONObject();
        ret.put("type", "qupdate");
        ret.put("id1", id1);
        ret.put("port1", port1);
        ret.put("id2", id2);
        ret.put("port2", port2);
        ret.put("val", value == null ? null : JSONObject.escape(value.toString()));
        ret.put("del", wait + pDdelay);

        sendUpdate(ret);

    }

    public int getWait() {
        return wait;
    }

    public JSONArray toJSON() {
        JSONArray ret = new JSONArray();
        for (int id = 0; id < nodes.length; ++id) {
            ProgramNode pn = nodes[id];
            JSONObject o = new JSONObject();
            o.put("type", pn.flowComponent.getName());
            o.put("id", id);
            o.put("x", pn.x);
            o.put("y", pn.y);
            JSONObject p = new JSONObject();
            for (int i = 0; i < pn.inputPorts.length; ++i) {
                JSONArray a = new JSONArray();
                IP ip = pn.inputPorts[i];
                if (ip instanceof IP.ConstantIP)
                    a.add(ip.get());
                p.put(i, a);
            }
            o.put("iP", p);
            p = new JSONObject();
            for (int i = 0; i < pn.outputPortPointers.length; ++i) {
                ProgramNode.PortRef[] portRefs = pn.outputPortPointers[i];
                JSONArray a = new JSONArray();
                for (ProgramNode.PortRef portRef : portRefs) {
                    JSONArray prA = new JSONArray();
                    prA.add(portRef.nextNode);
                    prA.add(portRef.portNr);
                    a.add(prA);
                }
                p.put(i, a);
            }
            o.put("oP", p);
            ret.add(o);
        }

        return ret;
    }

    public boolean isRunning() {
        return running;
    }
}
