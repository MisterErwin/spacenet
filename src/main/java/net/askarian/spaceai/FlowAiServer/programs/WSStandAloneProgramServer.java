package net.askarian.spaceai.FlowAiServer.programs;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.askarian.spaceai.FlowAiServer.components.impl.core.KickComponent;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by alex on 10/27/15.
 */
public class WSStandAloneProgramServer extends WebSocketServer {

    final Gson gson;
    final ProgramBuilder programBuilder;

    boolean isRunning = false;

    List<Program> currentlyRunningPrograms;

    static WSStandAloneProgramServer instance;

    public WSStandAloneProgramServer(int port) {
        super(new InetSocketAddress(port));
        gson = new Gson();
        programBuilder = new ProgramBuilder();
        currentlyRunningPrograms = new ArrayList<>();
        instance = this;
    }

    @Override
    public void start() {
        super.start();
        isRunning = true;
    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {

    }

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        unwatchAll(webSocket);
    }

    public void unwatchAll(WebSocket webSocket){
        for (int i=0;i< currentlyRunningPrograms.size();++i)
            currentlyRunningPrograms.get(i).unwatch(webSocket);

    }

    @Override
    public void onMessage(WebSocket webSocket, String s) {
//        System.out.println("WS<<" + s);
        Type type = new TypeToken<Map<String, Object>>() { /*Empty intended*/
        }.getType();
        Map<String, Object> map = gson.fromJson(s, type);
        if (map.containsKey("type"))
            switch (map.get("type").toString().toLowerCase()) {
                case "reload":
                    broadcast("{\"type\":\"reload\"}");
                    break;
                case "getcompinfo":
                    webSocket.send("{\"type\":\"compinfo\", \"info\":\"" + programBuilder.getInfo((String) map.get("com")) + "\"}");
                    break;
                case "getcomponents":
                    JSONObject a = new JSONObject();

                    programBuilder.forEachColumn(null, (name, iComponentDataHolder) -> a.put(name, iComponentDataHolder.getComponentData()));
                    JSONObject ret = new JSONObject();
                    ret.put("type", "listcomponents");
                    ret.put("c", a);
                    webSocket.send(ret.toJSONString());
                    break;
                case "start":
                    unwatchAll(webSocket);
                    Program program;
                    try {
                        program = programBuilder.buildProgram((JSONArray) JSONValue.parse((String) map.get("program")));
                    }catch (Exception e){
                        e.printStackTrace();
                        webSocket.send("{\"type\":\"error\", \"msg\":\"Could not load program\"}");
                        return;
                    }
                    if (!program.onNodeType(KickComponent.class, pn -> pn.flowComponent.onInput(pn, -1))){
                        webSocket.send("{\"type\":\"error\", \"msg\":\"Could not kick-start program\"}");
                        program.setAsEnded();
                    }else{
                        currentlyRunningPrograms.add(program);
                        program.watch(webSocket);
                        webSocket.send("{\"type\":\"status\", \"msg\":\"Program created\"}");
                    }
                    break;
                default:
                    System.out.println("Unknown key");
                    break;
            }
    }

    public void broadcast(String message) {
        this.connections().forEach(c -> c.send(message));
    }


    @Override
    public void onError(WebSocket webSocket, Exception e) {
        e.printStackTrace();
        if (webSocket == null)
            isRunning = false;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void notifyDead(Program program) {
        currentlyRunningPrograms.remove(program);
    }
}