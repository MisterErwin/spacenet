package net.askarian.spaceai.FlowAiServer.programs;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Table;
import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IComponentDataHolder;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.components.impl.cam.CameraComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.cam.CameraTriggerComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.core.*;
import net.askarian.spaceai.FlowAiServer.components.impl.math.AddComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.math.DivComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.math.MultComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.math.SubComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.nav.GetVelocityComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.nav.WaypointGetComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.nav.WaypointSetComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.out.CannonComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.out.ColorComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.out.ThrusterComponent;
import org.json.simple.JSONObject;

import java.util.Map;
import java.util.function.BiConsumer;

/**
 * Created by alex on 11/14/15.
 */
public class ProgramBuilder {

//    private static Map<String, FlowComponent> componentMap = new HashMap<>();

    //Name | Group-Parent or root | self
    private static Table<String, String, IComponentDataHolder> componentTable = HashBasedTable.create();
    private static String root = "root";

    static {
        componentTable.put("core", root, new IComponentDataHolder.ComponentGroup("Core functions"));
        componentTable.put("math", root, new IComponentDataHolder.ComponentGroup("Math functions"));
        componentTable.put("array", root, new IComponentDataHolder.ComponentGroup("Collection functions"));
        componentTable.put("nav", root, new IComponentDataHolder.ComponentGroup("Nav functions"));

        add(root, new Struct("struct"));

        add("core", new KickComponent("kick"));
        add("core", new CondComponent("condition"));
        add("core", new StoreComponent("store"));
        add("core", new WaitComponent("wait"));
        add("math", new AddComponent("add"));
        add("math", new SubComponent("sub"));
        add("math", new DivComponent("div"));
        add("math", new MultComponent("mult"));

        add("array", new ArrayMaxComponent("arraymax"));

        add("nav", new ThrusterComponent("thrusters"));
        add("nav", new GetVelocityComponent("getvelocity"));
        add("nav", new WaypointSetComponent("setwaypoint"));
        add("nav", new WaypointGetComponent("getwaypoint"));


        add(root, new CameraComponent("camera"));
        add(root, new CameraTriggerComponent("camera_selftrigger"));

        add(root, new ColorComponent("color"));
        add(root, new CannonComponent("cannon"));


        add("core", new DebugComponent("debug"));

    }

    private static void add(String parent, FlowComponent flowComponent) {
        componentTable.put(flowComponent.getName(), parent, flowComponent);
    }

    public ProgramBuilder() {
        setupChildren(root, null);
    }

    private void setupChildren(String group, JSONObject putinto) {
        forEachColumn(group, (name, holder) -> {
            if (putinto != null)
                putinto.put(name, holder.getComponentData());

            if (holder instanceof IComponentDataHolder.ComponentGroup) {
                setupChildren(name, (JSONObject) holder.getComponentData().get("children"));
            }
        });
    }

    public void forEachCell(BiConsumer<String, ? super FlowComponent> action) {
        for (Table.Cell<String, String, IComponentDataHolder> c : componentTable.cellSet())
            if (c.getValue() instanceof FlowComponent)
                action.accept(c.getRowKey(), (FlowComponent) c.getValue());
    }

    public void forEachColumn(String column, BiConsumer<String, ? super IComponentDataHolder> action) {
        componentTable.column(column == null ? root : column).forEach(action);
    }

    public Program buildProgram(Iterable components) {
        ProgramNode[] allNodes = new ProgramNode[Iterables.size(components)];

        components.forEach(e -> buildNode((Map) e, allNodes));

        Program program = new Program(allNodes);
        for (ProgramNode pn : allNodes)
            pn.program = program;

        return program;
    }

    public static FlowComponent getComponentByName(String name) {
        Map.Entry<String, IComponentDataHolder> e = Iterables.getFirst(componentTable.row(name).entrySet(), null);
        return e == null ? null : e.getValue() instanceof FlowComponent ? (FlowComponent) e.getValue() : null;
    }

    private boolean buildNode(Map jsonObject, ProgramNode[] ret) {
        int ID = ((Number) jsonObject.get("id")).intValue();
        FlowComponent flowComponent = getComponentByName((String) jsonObject.get("type"));
        if (flowComponent == null)
            System.out.println(jsonObject.get("type"));
        IP[] ips = new IP[flowComponent.getInputPortSize()];

        Map portJO = (Map) jsonObject.get("iP");
        if (portJO != null)
            for (Object port : portJO.keySet()) {
                if (port instanceof String && "class".equals(port))
                    continue;
                Object a = portJO.get(port);
                Object i = Iterables.get((Iterable<?>) a, 0);
                Object o = (i instanceof Iterable) ? Iterables.get((Iterable<?>) i, 1) : null;
                ips[Integer.parseInt((String) port)] = new IP.ConstantIPImpl(o/*((List) ((List) portJO.get(port)).get(0)).get(1)*/);
            }

        ProgramNode.PortRef[][] outports = new ProgramNode.PortRef[flowComponent.getOutputPortSize()][];

        portJO = (Map) jsonObject.get("oP");
        if (portJO != null)
            for (int port = 0; port < flowComponent.getOutputPortSize(); ++port) {
                Iterable ars = (Iterable) portJO.get(Long.toString(port));
                if (ars != null && !Iterables.isEmpty(ars)) {
                    int s = Iterables.size(ars);
                    ProgramNode.PortRef[] thisPort = new ProgramNode.PortRef[s];

                    for (int i = 0; i < s; ++i)
                        thisPort[i] = new ProgramNode.PortRef(((Number) Iterables.getFirst((Iterable) Iterables.get(ars, i), null)).intValue(),
                                ((Number) Iterables.get((Iterable) Iterables.get(ars, i), 1)).intValue());

                    outports[port] = thisPort;
                }
            }

        ProgramNode pn = new ProgramNode(ID, ips, outports, flowComponent);

        pn.x = ((Number) jsonObject.getOrDefault("x", 0)).intValue();
        pn.y = ((Number) jsonObject.getOrDefault("y", 0)).intValue();

        ret[ID] = pn;
        return true;
    }


    public String getInfo(String name) {
        FlowComponent fc = getComponentByName(name);
        if (fc == null)
            return "Not found";
        else return fc.getInfo();
    }
}
