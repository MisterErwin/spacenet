package net.askarian.spaceai.FlowAiServer.programs;

import net.askarian.spaceai.FlowAiServer.components.FlowComponent;
import net.askarian.spaceai.FlowAiServer.components.IP;
import net.askarian.spaceai.FlowAiServer.components.impl.core.CondComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.core.KickComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.core.StoreComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.core.WaitComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.math.AddComponent;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by alex on 11/14/15.
 */
public class TestProgram {

    static Map<String, FlowComponent> flowComponentMap = new HashMap<>();

    static {
        flowComponentMap.put("kick", new KickComponent("kick"));
        flowComponentMap.put("cond", new CondComponent("cond"));
        flowComponentMap.put("store", new StoreComponent("store"));
        flowComponentMap.put("wait", new WaitComponent("wait"));
        flowComponentMap.put("add", new AddComponent("add"));

    }


    public static void main(String[] args) {
        final int a = 2, b=4, c=8, d=16;
        int i = a|b;
        System.out.println( ((i & a) ==a) + "|" +  ((i & b) ==b) + "|" + ( (i & c) ==c));
        i |= c;
        System.out.println( ((i & a) ==a) + "|" +  ((i & b) ==b) + "|" + ( (i & c) ==c));
        i |= c;
        System.out.println( ((i & a) ==a) + "|" +  ((i & b) ==b) + "|" + ( (i & c) ==c));

//        Object ausdi;
//        System.out.println((ausdi=new String("d")));
//        test();

//        List<ProgramNode> nodes = new ArrayList<>();
//        nodes.add(newPN(0, "kick", new ProgramNode.PortRef[][]{new ProgramNode.PortRef[]{new ProgramNode.PortRef(1, 1)}}));
//        nodes.add(new ProgramNode(1, new IP[]{new IP.ConstantIPImpl(2000), null},
//                new ProgramNode.PortRef[][]{new ProgramNode.PortRef[]{new ProgramNode.PortRef(2, 1)}},
//                flowComponentMap.get("wait")));
//        nodes.add(new ProgramNode(2, new IP[]{new IP.ConstantIPImpl(50), null, null},
//                new ProgramNode.PortRef[0][],
//                flowComponentMap.get("add")));
//
//        Program program = new Program(nodes.toArray(new ProgramNode[nodes.size()]));
//        program.queueUpdate(0, 0, null, 0);

    }

    private static ProgramNode newPN(int ID, String name, ProgramNode.PortRef[][] next) {
        return new ProgramNode(ID, new IP[flowComponentMap.get(name).getInputPortSize()], next, flowComponentMap.get(name));
    }

    private static void test(){
        String json = "[{\"id\":0,\"type\":\"kick\",\"oP\":{\"0\":[[1,0]]}},{\"id\":1,\"type\":\"add\",\"oP\":{\"0\":[[2,0]]},\"iP\":{\"1\":[[-1,\"41\"]]}},{\"id\":2,\"type\":\"condition\",\"oP\":{\"0\":[[3,0]],\"1\":[[4,0]]},\"iP\":{\"1\":[[-1,\"==\"]],\"2\":[[-1,\"42\"]]}},{\"id\":3,\"type\":\"sub\",\"oP\":{\"0\":[]}},{\"id\":4,\"type\":\"mult\",\"oP\":{\"0\":[]}}]";
        ProgramBuilder programBuilder = new ProgramBuilder();
        Program program = programBuilder.buildProgram((JSONArray) JSONValue.parse(json));
        System.out.println(Objects.toString(program));
        ProgramNode kick = program.getFirstNodeOfType(KickComponent.class);
        kick.flowComponent.onInput(kick, -1);
    }
}
