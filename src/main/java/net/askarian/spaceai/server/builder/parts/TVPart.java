package net.askarian.spaceai.server.builder.parts;

import com.artemis.Component;
import lombok.Getter;
import net.askarian.spaceai.server.builder.Part;
import net.askarian.spaceai.server.builder.PartType;
import net.askarian.spaceai.server.components.rendering.RenderNearby;
import net.askarian.spaceai.server.util.config.Configuration;
import org.json.simple.JSONObject;

/**
 * Created by MisterErwin on 24.12.2015.
 * In case you need it, ask me ;)
 */
@Getter
public class TVPart extends Part.UniquePart {

    private final int range;

    public TVPart(Configuration configuration) {
        super(PartType.TV_STATION, configuration);
        this.range = configuration.getInt("range", 0);
    }


    @Override
    public Component buildComponent() {
        return new RenderNearby(range);
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject o= super.toJSONObject();
        o.put("range", range);
        return o;
    }

    @Override
    protected String makeDesc(String desc) {
        return super.makeDesc(desc)
                .replace("$range", aS(range));
    }
}
