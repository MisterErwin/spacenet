package net.askarian.spaceai.server.builder;

import com.artemis.Component;
import com.artemis.utils.EntityBuilder;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Data;
import net.askarian.spaceai.server.builder.parts.CasePart;
import net.askarian.spaceai.server.components.Health;
import net.askarian.spaceai.server.components.NodeAI;
import net.askarian.spaceai.server.components.physics.Mass;
import net.askarian.spaceai.server.util.config.Configuration;
import net.askarian.spaceai.server.util.config.ConfigurationProvider;
import net.askarian.spaceai.server.util.config.YamlConfiguration;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * Created by alex on 11/1/15.
 */
public class PartRegistry {

    private final Map<String, Part> partMap = Maps.newHashMap();
    //    public final Table<PartType, String, Part> partTable = HashBasedTable.create();
    private final List<PartType> requiredparts = Arrays.asList(PartType.CASE);

    public JSONObject jsonPartList;

    public void loadParts() {
        File partFolder = new File("parts");
        if (!partFolder.exists())
            partFolder.mkdirs();
        loadFolder(partFolder, ConfigurationProvider.getProvider(YamlConfiguration.class));

        jsonPartList = new JSONObject();
        for (PartType pt : PartType.values())
            jsonPartList.put(pt.name(), new JSONObject());

        for (Part part : partMap.values()) {
            ((JSONObject) jsonPartList.get(part.getType().name())).put(part.getName(), part.toJSONObject());
        }
    }

    public static List<String> makePartList(Map<?, Object> object) {
        if (object == null || object.size() == 0) return null;
        List<String> ret = Lists.newArrayList();

        for (Object array : object.values())
            for (Object s : (Iterable) array)
                ret.add((String) s);
        return ret;
    }

    public SchemataResult evaluateSchemataAndGetCost(List<String> parts) {
        if (parts == null) return null;
        List<PartType> unique = new LinkedList<>();
        List<PartType> required = new ArrayList<>(requiredparts);
        int mass = 0, cost = 0, size = 0, maxSize = -1, time=1;

        for (String partName : parts) {
            Part part = partMap.get(partName);

            if (part == null) return null;
            if (part.getType().isUnique()) {
                if (unique.contains(part.getType())) return null;
                unique.add(part.getType());
                if (part instanceof CasePart)
                    maxSize = ((CasePart) part).getMaxSize();

            }
            cost += part.getCost();
            mass += part.getMass();
            size += part.getSize();
            time += part.getTime();
            required.remove(part.getType());
        }
        if (!required.isEmpty()) return null;
        return size <= maxSize ? new SchemataResult(cost,time) : null;
    }

    public NodeAI appendSchemata(EntityBuilder entityBuilder, List<String> parts) {
        if (parts == null)return null;
        NodeAI nodeAI = null;
        int mass = 1,health=0;
        for (String partName : parts) {
            Part part = partMap.get(partName);
            if (part == null) continue;
            mass += part.getMass();
            if (part instanceof Part.UniquePart) {
                Component c = ((Part.UniquePart) part).buildComponent();
                if (c != null) {
                    entityBuilder.with(c);
                    if (c instanceof NodeAI)
                        nodeAI = (NodeAI) c;
                }
            }

            if (part instanceof CasePart)
                health += ((CasePart)part).getHealth();
        }
        entityBuilder.with(new Mass(mass));
        entityBuilder.with(new Health(health));

        return nodeAI;
    }


    private void loadFolder(File folder, ConfigurationProvider yamlConfiguration) {
        for (File g : folder.listFiles())
            if (g.isDirectory())
                loadFolder(g, yamlConfiguration);
            else
                loadPartFile(g, yamlConfiguration);
    }

    private void loadPartFile(File f, ConfigurationProvider yamlConfiguration) {
        System.out.println("Loading part file " + f.getName());
        try {
            Configuration configuration = yamlConfiguration.load(f);
            PartType pr = PartType.getByName(configuration.getString("type", "key-not-found"));
            if (pr == null) {
                System.out.println("Unknown part-type " + configuration.getString("type"));
                return;
            }
            try {

                Constructor constructor = pr.getPartClass().getConstructor(Configuration.class);
                Part p = (Part) constructor.newInstance(configuration);
                p.makeDesc();
                partMap.put(configuration.getString("name"), p);
                System.out.println("Loaded " + configuration.getString("name"));
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException | IllegalArgumentException | NullPointerException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public
    @Data
    static class SchemataResult {
        final int cost;
        final int time;
    }
}
