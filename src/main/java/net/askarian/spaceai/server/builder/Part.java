package net.askarian.spaceai.server.builder;

import com.artemis.Component;
import lombok.Getter;
import net.askarian.spaceai.server.util.config.Configuration;
import org.json.simple.JSONObject;

/**
 * Created by alex on 11/1/15.
 */
@Getter
public abstract class Part {
    private final PartType type;
    private final String name, title;
    private String description;
    private final int mass, size, cost, time;

    public Part(PartType type, Configuration configuration) {
        this.type = type;
        this.name = configuration.getString("name");
        if (name == null)
            throw new NullPointerException("Name MUST NOT be null");
        this.title = configuration.getString("title", name);
        this.description = configuration.getString("description", "{empty:true}");
        this.mass = configuration.getInt("mass", 0);
        this.size = configuration.getInt("size", 0);
        this.cost = configuration.getInt("cost", 0);
        this.time = configuration.getInt("time", 1);


    }

    public String makeDesc(){
        return this.description = makeDesc(this.description);
    }

    protected String makeDesc(String desc){
       return desc.replace("$title", title)
                .replace("$mass", aS(mass))
                .replace("$size", aS(size))
                .replace("$cost", aS(cost))
                .replace("$time", aS(time));
    }

    protected static  String aS(Number n){
        if (n.intValue() == 0)return "-";
        return n.toString();
    }

    public JSONObject toJSONObject() {
        JSONObject o = new JSONObject();
        o.put("title", title);
        o.put("description", getDescription());
        o.put("mass", mass);
        o.put("size", size);
        o.put("cost", cost);
        o.put("time", time);
        return o;
    }

    public static abstract class UniquePart extends Part {
        public abstract Component buildComponent();

        public UniquePart(PartType type, Configuration configuration) {
            super(type, configuration);
            if (!type.isUnique())
                throw new IllegalArgumentException("type must be unique, got " + type);
        }
    }

    public static abstract class NotUniquePart extends Part {
        public abstract void modifyComponent(Component component);

        public NotUniquePart(PartType type, Configuration configuration) {
            super(type, configuration);
            if (type.isUnique())
                throw new IllegalArgumentException("type must be unique, got " + type);

        }
    }


}
