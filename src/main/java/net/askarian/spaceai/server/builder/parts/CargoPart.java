package net.askarian.spaceai.server.builder.parts;

import com.artemis.Component;
import lombok.Getter;
import net.askarian.spaceai.server.builder.Part;
import net.askarian.spaceai.server.builder.PartType;
import net.askarian.spaceai.server.util.config.Configuration;

/**
 * Created by MisterErwin on 24.12.2015.
 * In case you need it, ask me ;)
 */
@Getter
public class CargoPart extends Part.NotUniquePart {
    private final int cargoHold;

    public CargoPart(Configuration configuration) {
        super(PartType.CARGO, configuration);
        this.cargoHold = configuration.getInt("cargoHold", 100);
    }

    @Override
    public void modifyComponent(Component component) {

    }
}
