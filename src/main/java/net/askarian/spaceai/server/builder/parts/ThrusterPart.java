package net.askarian.spaceai.server.builder.parts;

import com.artemis.Component;
import lombok.Getter;
import net.askarian.spaceai.server.builder.Part;
import net.askarian.spaceai.server.builder.PartType;
import net.askarian.spaceai.server.components.output.Thrusters;
import net.askarian.spaceai.server.util.config.Configuration;

/**
 * Created by MisterErwin on 24.12.2015.
 * In case you need it, ask me ;)
 */
@Getter
public class ThrusterPart extends Part.UniquePart {
    private final double maxForce;

    public ThrusterPart(Configuration configuration) {
        super(PartType.THRUSTER, configuration);
        this.maxForce = configuration.getDouble("maxForce", 1);
    }

    @Override
    public Component buildComponent() {
        return new Thrusters(maxForce);
    }

    @Override
    protected String makeDesc(String desc) {
        return super.makeDesc(desc).replace("$thrust", aS(maxForce));
    }
}
