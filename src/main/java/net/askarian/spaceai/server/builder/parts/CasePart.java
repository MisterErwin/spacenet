package net.askarian.spaceai.server.builder.parts;

import com.artemis.Component;
import lombok.Getter;
import net.askarian.spaceai.server.builder.Part;
import net.askarian.spaceai.server.builder.PartType;
import net.askarian.spaceai.server.components.Case;
import net.askarian.spaceai.server.util.config.Configuration;
import org.json.simple.JSONObject;

/**
 * Created by MisterErwin on 24.12.2015.
 * In case you need it, ask me ;)
 */
@Getter
public class CasePart extends Part.UniquePart {

    private final int maxSize, health;

    public CasePart(Configuration configuration) {
        super(PartType.CASE, configuration);
        this.maxSize = configuration.getInt("maxSize", 0);
        this.health = configuration.getInt("health", 0);
    }


    @Override
    public Component buildComponent() {
        return new Case(maxSize, health);
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject o= super.toJSONObject();
        o.put("newSize", maxSize);
        o.put("newHealth", health);
        return o;
    }

    @Override
    protected String makeDesc(String desc) {
        return super.makeDesc(desc)
                .replace("$maxSize", aS(maxSize))
                .replace("$health", aS(health));
    }
}
