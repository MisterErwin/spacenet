package net.askarian.spaceai.server.builder.parts;

import com.artemis.Component;
import lombok.Getter;
import net.askarian.spaceai.server.builder.Part;
import net.askarian.spaceai.server.builder.PartType;
import net.askarian.spaceai.server.components.output.Cannon;
import net.askarian.spaceai.server.util.config.Configuration;

/**
 * Created by MisterErwin on 24.12.2015.
 * In case you need it, ask me ;)
 */
@Getter
public class WeaponPart extends Part.UniquePart {
    private final int projectilesPerShot, projectileLiveTime, damage, delay;
    private final double noisePerProjectile, projectileSpeed;


    public WeaponPart(Configuration configuration) {
        super(PartType.WEAPON, configuration);
        this.projectilesPerShot = configuration.getInt("projectilesPerShot", 1);
        this.projectileLiveTime = configuration.getInt("projectileLiveTime", 12);
        this.damage = configuration.getInt("damage", 100);
        this.delay = configuration.getInt("delay", 10);
        this.noisePerProjectile = configuration.getDouble("noisePerProjectile", 0);
        this.projectileSpeed = configuration.getDouble("projectileSpeed", 2);
    }

    @Override
    public Component buildComponent() {
        return new Cannon(projectileSpeed, delay, projectilesPerShot, projectileLiveTime, damage);
    }
}
