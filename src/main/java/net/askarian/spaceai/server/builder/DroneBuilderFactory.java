package net.askarian.spaceai.server.builder;

import com.artemis.utils.EntityBuilder;
import net.askarian.spaceai.FlowAiServer.programs.Program;
import net.askarian.spaceai.server.components.Basis;
import net.askarian.spaceai.server.components.Health;
import net.askarian.spaceai.server.components.Inventory;
import net.askarian.spaceai.server.components.NodeAI;
import net.askarian.spaceai.server.components.input.Waypoint;
import net.askarian.spaceai.server.components.output.Cannon;
import net.askarian.spaceai.server.components.output.Thrusters;

/**
 * Created by MisterErwin on 24.12.2015.
 * In case you need it, ask me ;)
 */
public class DroneBuilderFactory {

    public static void build(PartRegistry partRegistry, EntityBuilder drone, Basis basis, Program program) {
        if (basis == null) {
            drone.with(new Thrusters(0.2), new Inventory(1000, 50), new Waypoint(2), new NodeAI(100, 100, 2, program),
                    new Health(100), new Cannon(2, 30, 1, 50, 10));
        } else {
            NodeAI nodeAI = partRegistry.appendSchemata(drone, PartRegistry.makePartList(basis.droneConfiguration));
            if (nodeAI == null)
                program.setAsEnded();
            else
                nodeAI.program = program;

        }
    }

}
