package net.askarian.spaceai.server.builder;

import lombok.Getter;
import net.askarian.spaceai.server.builder.parts.*;

/**
 * Created by alex on 11/1/15.
 */
public enum PartType {
    CASE(true, CasePart.class),
    THRUSTER(true, ThrusterPart.class),
    WEAPON(true, WeaponPart.class),
    CAMERA(true, CameraPart.class),
    CARGO(false, null),
    BATTERY(false, null),
    GENERATOR(false, null),
    WAYPOINT(true, null),
    TV_STATION(true, TVPart.class),
    BRAIN(true, BrainPart.class),
    SHIELD(false, null);

    @Getter
    private final boolean isUnique;
    @Getter
    private final Class<? extends Part> partClass;

    PartType(boolean isUnique, Class<? extends Part> c) {
        this.isUnique = isUnique;
        this.partClass = c;
    }

    public static PartType getByName(String name){
        for (PartType pr : values())
            if (pr.name().equalsIgnoreCase(name))
                return pr;
        return null;
    }

}
