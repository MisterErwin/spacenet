package net.askarian.spaceai.server.builder.parts;

import com.artemis.Component;
import lombok.Getter;
import net.askarian.spaceai.server.builder.Part;
import net.askarian.spaceai.server.builder.PartType;
import net.askarian.spaceai.server.components.NodeAI;
import net.askarian.spaceai.server.components.PlayerControlled;
import net.askarian.spaceai.server.util.config.Configuration;

/**
 * Created by MisterErwin on 24.12.2015.
 * In case you need it, ask me ;)
 */
@Getter
public class BrainPart extends Part.UniquePart {
    private final int delay, nodeAmount, maxRunningNodes;
    private final boolean manual;

    public BrainPart(Configuration configuration) {
        super(PartType.BRAIN, configuration);
        this.delay = configuration.getInt("delay");
        this.nodeAmount = configuration.getInt("nodeAmount");
        this.maxRunningNodes = configuration.getInt("maxRunningNodes");
        this.manual = configuration.getBoolean("manual",false);
    }

    @Override
    public Component buildComponent() {
        if (!manual)
        return new NodeAI(delay, nodeAmount, maxRunningNodes);
        else
            return new PlayerControlled();
    }

    @Override
    protected String makeDesc(String desc) {
        return super.makeDesc(desc)
                .replace("$delay", aS(delay))
                .replace("$nodeAmount", aS(nodeAmount))
                .replace("$maxRunningNodes", aS(maxRunningNodes));
    }
}
