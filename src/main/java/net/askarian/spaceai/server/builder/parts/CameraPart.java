package net.askarian.spaceai.server.builder.parts;

import com.artemis.Component;
import lombok.Getter;
import net.askarian.spaceai.server.builder.Part;
import net.askarian.spaceai.server.builder.PartType;
import net.askarian.spaceai.server.components.input.Camera;
import net.askarian.spaceai.server.util.config.Configuration;

/**
 * Created by MisterErwin on 24.12.2015.
 * In case you need it, ask me ;)
 */
@Getter
public class CameraPart extends Part.UniquePart {
    private final int maxDistance, size /*amount of sector*/;


    public CameraPart(Configuration configuration) {
        super(PartType.CAMERA, configuration);
        this.maxDistance = configuration.getInt("maxDistance", 100);
        this.size = configuration.getInt("size", 36);
    }

    @Override
    public Component buildComponent() {
        return new Camera(size, maxDistance);
    }
}
