package net.askarian.spaceai.server;

import com.artemis.Component;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.managers.PlayerManager;
import com.artemis.managers.UuidEntityManager;
import com.artemis.utils.Bag;
import com.artemis.utils.ImmutableBag;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.askarian.spaceai.FlowAiServer.programs.Program;
import net.askarian.spaceai.FlowAiServer.programs.ProgramServer;
import net.askarian.spaceai.math.Vector2d;
import net.askarian.spaceai.server.builder.PartRegistry;
import net.askarian.spaceai.server.components.*;
import net.askarian.spaceai.server.components.output.Cannon;
import net.askarian.spaceai.server.components.output.Color;
import net.askarian.spaceai.server.components.output.Thrusters;
import net.askarian.spaceai.server.components.physics.ForceAccumulator;
import net.askarian.spaceai.server.components.physics.Position;
import net.askarian.spaceai.server.components.rendering.RenderNearby;
import net.askarian.spaceai.server.components.rendering.Rendered;
import net.askarian.spaceai.server.components.rendering.Watchable;
import net.askarian.spaceai.server.systems.BasisSystem;
import net.askarian.spaceai.server.systems.ChunkSystem;
import net.askarian.spaceai.server.util.LoginServer;
import org.apache.commons.lang.math.NumberUtils;
import org.java_websocket.WebSocket;
import org.java_websocket.exceptions.WebsocketNotConnectedException;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by alex on 10/27/15.
 */
public class WSServer extends WebSocketServer {

    Map<InetSocketAddress, WebSocketViewer> inetSocketAddressWebSocketViewerMap = Maps.newHashMap();
    final Region region;
    final Gson gson;
    ICommandManager commandManager;


    boolean isRunning = false;
    ProgramServer programServer;
    LoginServer loginServer;


    public WSServer(Region region, int port, ICommandManager commandManager) {
        super(new InetSocketAddress(port));
        gson = new Gson();
        this.region = region;
        this.commandManager = commandManager;
        programServer = new ProgramServer();
        loginServer = new LoginServer(region);
    }

    @Override
    public void start() {
        super.start();
        isRunning = true;
    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        if (clientHandshake.getResourceDescriptor().equals("/login")) {
            loginServer.onOpen(webSocket);
            return;
        }
        if (!loginServer.isLoggedIn(webSocket)) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "error");
            jsonObject.put("authentification", "missing");

            webSocket.send(jsonObject.toJSONString());
            webSocket.close();
            return;
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", "authentification");
        jsonObject.put("name", loginServer.getPlayerName(webSocket));
        webSocket.send(jsonObject.toJSONString());

        if (clientHandshake.getResourceDescriptor().equals("/program")) {
            programServer.listComponents(webSocket);
        } else {
            Entity e = null;
            ImmutableBag<Entity> bag =
                    region.getArtemisWorld().getSystem(PlayerManager.class).getEntitiesOfPlayer(loginServer.getPlayerName(webSocket));
            if (bag.isEmpty()) {
                e = region.addBase(loginServer.getPlayerName(webSocket));
            } else {
                GroupManager groupManager = region.getArtemisWorld().getSystem(GroupManager.class);
                for (Entity entity : bag) {
                    if (groupManager.isInGroup(entity, "base")) {
                        e = entity;
                        break;
                    }
                }
            }
            if (e == null) {
                jsonObject = new JSONObject();
                jsonObject.put("type", "error");
                jsonObject.put("msg", "Failed to spawn a base\n Reload to retry");
                jsonObject.put("close", true);
                webSocket.send(jsonObject.toJSONString());
                webSocket.close();
            } else {
                WebSocketViewer iv = new WebSocketViewer(webSocket);
                inetSocketAddressWebSocketViewerMap.put(webSocket.getRemoteSocketAddress(), iv);
//                RenderNearby renderNearby = e.getComponent(RenderNearby.class);
//                iv.view(e.getId(), renderNearby);

                startViewing(e, region.getArtemisWorld().getSystem(UuidEntityManager.class).getUuid(e), webSocket);
                sendPartList(webSocket);
                region.getExec().schedule(new Runnable() {
                    @Override
                    public void run() {
                        sendSchematic(webSocket);
                    }
                }, 2, TimeUnit.SECONDS);
                sendBaseProgram(webSocket);
            }
        }
    }

    private void sendPartList(WebSocket webSocket) {
        JSONObject o = new JSONObject();
        o.put("type", "partlist");
        o.put("parts", this.region.partRegistry.jsonPartList);
        webSocket.send(o.toJSONString());
    }

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        WebSocketViewer webSocketViewer = inetSocketAddressWebSocketViewerMap.remove(webSocket.getRemoteSocketAddress());
        if (webSocketViewer != null)
            webSocketViewer.setExplicitWatching(null);

    }


    @Override
    public void onMessage(WebSocket webSocket, String s) {
        Type type = new TypeToken<Map<String, Object>>() { /*Empty intended*/
        }.getType();
        Map<String, Object> map = gson.fromJson(s, type);
        if (!loginServer.onMessage(webSocket, map, s))
            if (map.containsKey("type")) {
                switch (map.get("type").toString().toLowerCase()) {
                    case "program":
                        programServer.onMessage(webSocket, map, s);

                        break;
                    case "getschematic":
                        sendSchematic(webSocket);

                        break;
                    case "setschematic":
                        Map<?, Object> map1 = (Map) map.get("schematic");
                        PartRegistry.SchemataResult cost = region.partRegistry.evaluateSchemataAndGetCost(PartRegistry.makePartList(map1));
                        if (cost == null) {
                            JSONObject o = new JSONObject();
                            o.put("type", "error");
                            o.put("msg", "Invalid schematic");
                            webSocket.send(o.toJSONString());
                            break;
                        }
                        Basis b = getBasis(webSocket);
                        b.droneConfiguration = new JSONObject();
                        for (Map.Entry e : map1.entrySet()) {
                            JSONArray array = new JSONArray();
                            array.addAll((Collection) e.getValue());
                            b.droneConfiguration.put(e.getKey(), array);
                        }
                        region.getArtemisWorld().getSystem(BasisSystem.class).update(b);
                        break;
                    case "setbaseprogram":
                        ArrayList jsonArray = (ArrayList) map.get("program");
                        b = getBasis(webSocket);
                        b.droneProgram = new JSONArray();
                        b.droneProgram.addAll(jsonArray);
                        b.droneProgramName = (String) map.get("pname");
                        break;
                    case "setbaseautobuild":
                        b = getBasis(webSocket);
                        b.autoBuild = (boolean) map.getOrDefault("autobuild", false);
                        break;
                    case "produceone":
                        b = getBasis(webSocket);
                        if (!b.autoBuild)
                            b.currentBuildTime = b.schematicBuildTime;
                        break;
                    case "getbaseprogram":
                        sendBaseProgram(webSocket);
                        break;
                    case "askforprogram":
                        UUID uuid = UUID.fromString((String) map.get("uuid"));
                        JSONObject ret = new JSONObject();
                        if (uuid != null) {
                            Entity entity = region.getArtemisWorld().getSystem(UuidEntityManager.class).getEntity(uuid);
                            if (entity != null) {
                                NodeAI nodeAI = entity.getComponent(NodeAI.class);
                                if (nodeAI != null)
                                    if (nodeAI.program != null) {
                                        ret.put("type", "setprogram");
                                        ret.put("program", nodeAI.program.toJSON());
                                        webSocket.send(ret.toJSONString());
                                        break;
                                    }

                            }
                        }
                        ret.put("type", "error");
                        ret.put("msg", "No program found");

                        webSocket.send(ret.toJSONString());
                        break;
                    case "objectinfo":
                        uuid = UUID.fromString((String) map.get("uuid"));
                        if (uuid != null) {
                            Entity entity = region.getArtemisWorld().getSystem(UuidEntityManager.class).getEntity(uuid);
                            if (entity != null) {
                                String owner = region.getArtemisWorld().getSystem(PlayerManager.class).getPlayer(entity);
                                String playername = loginServer.getPlayerName(webSocket);
                                String renderType = entity.getComponent(Rendered.class).name;
                                if (entity.getComponent(RenderNearby.class) != null && map.containsKey("shift")) {
                                    startViewing(entity, uuid, webSocket);
                                    break;
                                }
                                if (playername.equals(owner) && entity.getComponent(PlayerControlled.class) != null && map.containsKey("rightButton")) {
                                    startPlayerControl(entity, uuid, webSocket);
                                    break;
                                }
                                if (renderType.equals("base") && owner.equals(playername)) {
                                    openBaseControlModal(entity, uuid, webSocket);
                                    break;
                                }

                                StringBuilder msg = new StringBuilder();
                                msg.append("<h3>Type:").append(renderType).append("</h3><br>");

                                msg.append("Owner: ").append(owner);

                                if (entity.getComponent(NodeAI.class) != null)
                                    msg.append("<br>").append("<a href='#openDebug' onclick='return openDebugByUUID(\"" + entity.getWorld().getSystem(UuidEntityManager.class).getUuid(entity).toString() + "\")'>DEBUG</a>");

                                Bag<Component> components = new Bag<>();
                                entity.getComponents(components);
                                for (Component c : components) {
                                    if (c instanceof IObjectInfoComponent)
                                        ((IObjectInfoComponent) c).appendInfo(msg);
                                }


                                JSONObject o = new JSONObject();
                                o.put("type", "error");
                                o.put("msg", msg.toString());
                                webSocket.send(o.toJSONString());

                                break;
                            }
                        }
                        JSONObject o = new JSONObject();
                        o.put("type", "error");
                        o.put("msg", "Could not retrieve data");
                        webSocket.send(o.toJSONString());
                        break;
                    case "killdrone":
                        for (Program p : programServer.getByWatcher(webSocket)) {
                            p.unwatch(webSocket);
                            p.getArtemisEntity().deleteFromWorld();
                        }

                        break;
                    case "updateprogram":
                        Program program = programServer.buildAndStartProgram((String) map.get("program"));
                        boolean ok = false;
                        if (program != null) {
                            for (Program p : programServer.getByWatcher(webSocket)) {
                                program.setArtemisEntity(p.getArtemisEntity());
                                p.getComponent(NodeAI.class).program = program;
                                p.unwatchAll();
                                program.watch(webSocket);
                                System.out.println("Started program " + program);
                                return;
                            }
                            program.setAsEnded();
                            System.out.println("Failed to spawn node node");
                        } else {
                            System.out.println("error - program is null");
                        }
                        break;
                    case "askfordebug":
                        try {
                            uuid = UUID.fromString((String) map.get("uuid"));
                            if (uuid != null) {
                                region.getArtemisWorld().getSystem(UuidEntityManager.class).getEntity(uuid).
                                        getComponent(NodeAI.class).program.watch(webSocket);
                            }
                            break;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        JSONObject answer = new JSONObject();
                        answer.put("type", "error");
                        answer.put("msg", "Could not find drone\nAborting");
                        answer.put("close", true);
                        webSocket.send(answer.toJSONString());

                        break;

                    case "getspawninfo":
                        uuid = UUID.fromString((String) map.get("uuid"));
                        if (uuid != null) {
                            Entity e = region.getArtemisWorld().getSystem(UuidEntityManager.class).
                                    getEntity(uuid);
                            if (e != null)
                                Rendered.addUpdateType(
                                        e.getComponent(Rendered.class),
                                        inetSocketAddressWebSocketViewerMap.get(webSocket.getRemoteSocketAddress()),
                                        Rendered.UpdateType_ALL);
                        }
                        break;

                    case "spawndrone":
                        programServer.unwatchAll(webSocket);
                        program = programServer.buildAndStartProgram((String) map.get("program"));
                        o = new JSONObject();
                        o.put("type", "spawnreply");
                        o.put("pname", map.get("pname"));
                        boolean doWatch = map.containsKey("watch");
                        if (program != null) {
                            int a = region.addAIDrone(program,
                                    loginServer.getPlayerName(webSocket),
                                    Vector2d.random(40).addSelf(getBaseLocation(webSocket)), getBasis(webSocket));


                            System.out.println("Spawned AI on request: " + a);
                            if (a >= 0) {
                                if (doWatch)
                                    program.watch(webSocket);
                                o.put("status", "ok");
                                o.put("eid", a);
                                o.put("uuid",
                                        region.getArtemisWorld().getSystem(UuidEntityManager.class).getUuid(region.getArtemisWorld().getEntity(a)).toString());
                            } else {
                                program.setAsEnded();
                                System.out.println("Failed to spawn node node" + a);
                                o.put("status", "failed");

                            }
                        } else {
                            System.out.println("error");
                            o.put("status", "parseError");

                        }
                        webSocket.send(o.toJSONString());
                        break;
                    case "watch":
                        WebSocketViewer webSocketViewer = inetSocketAddressWebSocketViewerMap.get(webSocket.getRemoteSocketAddress());
                        Entity entity = region.getArtemisWorld().getSystem(UuidEntityManager.class).getEntity(UUID.fromString((String) map.get("uuid")));
                        webSocketViewer.setExplicitWatching(entity);
                        break;
                    case "view":
                        uuid = UUID.fromString((String) map.get("uuid"));
                        startViewing(
                                region.getArtemisWorld().getSystem(UuidEntityManager.class).getEntity(uuid), uuid,
                                webSocket
                        );
                        break;
                    case "shot":
                        entity = region.getArtemisWorld().getSystem(UuidEntityManager.class).getEntity(UUID.fromString((String) map.get("uuid")));

                        Cannon can = entity.getComponent(Cannon.class);
                        if (can != null) {
                            can.dir = map.containsKey("dir") ? NumberUtils.toInt((String) map.get("dir"), 1) : 1;
                            break;
                        }

                        break;
                    case "force":
                    case "forcea":
                    case "forceb":
                        entity = region.getArtemisWorld().getSystem(UuidEntityManager.class).getEntity(UUID.fromString((String) map.get("uuid")));

                        ForceAccumulator forceAccumulator = entity.getComponent(ForceAccumulator.class);
                        if (forceAccumulator != null) {
                            forceAccumulator.add(Vector2d.random(0.2));
                            break;
                        }

                        break;
                    case "color":
                        entity = region.getArtemisWorld().getSystem(UuidEntityManager.class).getEntity(UUID.fromString((String) map.get("uuid")));
                        Color color = entity.getComponent(Color.class);
                        if (color == null) break;
                        color.color++;
                        if (color.color > 11)
                            color.color = 0;

                        Rendered rendered = entity.getComponent(Rendered.class);
                        if (rendered != null)
                            Rendered.addUpdateType(rendered, (IViewer) null, Rendered.UpdateType_COLOR);

                        break;
                    case "keycontrol":
                        uuid = UUID.fromString((String) map.get("uuid"));
                        if (uuid != null) {
                            Entity e = region.getArtemisWorld().getSystem(UuidEntityManager.class).getEntity(uuid);
                            if (e != null) {
                                PlayerControlled playerControlled = e.getComponent(PlayerControlled.class);
                                if (playerControlled != null) {
                                    if (loginServer.getPlayerName(webSocket).equals(playerControlled.controller)) {
                                        Thrusters thrusters = e.getComponent(Thrusters.class);
                                        int n = 0, dir = 0;
                                        if ((Boolean) map.getOrDefault("up", false)) {
                                            ++n;
                                            dir += 3;
                                        }
                                        if ((Boolean) map.getOrDefault("down", false)) {
                                            ++n;
                                            dir += 1;
                                        }

                                        if ((Boolean) map.getOrDefault("left", false)) {
                                            ++n;
                                            dir += 2;
                                        }
                                        if ((Boolean) map.getOrDefault("right", false)) {
                                            ++n;
                                        }
                                        if (n > 0) {
                                            thrusters.strength = thrusters.maxStrength;
                                            thrusters.dir = dir / n * 90;
                                        } else
                                            thrusters.strength = 0;
                                    }
                                }
                            }
                        }
                        break;
                    case "chat":
                        if (((String) map.get("text")).startsWith(":")) {
                            if (commandManager != null) {
                                switch (((String) map.get("text")).substring(1)) {
                                    case ":s3":
                                        Vector2d p = getBaseLocation(webSocket);
                                        String on = loginServer.getPlayerName(webSocket);
                                        for (int n = 0; n < 20; ++n) {
                                            Vector2d d = Vector2d.random(2.0);
                                            int i = region.addDrone(p.clone().addScaledSelf(d, 7), on);
                                            Entity e = region.getArtemisWorld().getEntity(i);
                                            forceAccumulator = e.getComponent(ForceAccumulator.class);
                                            forceAccumulator.add(d);
                                        }
                                        break;
                                    default:
                                        commandManager.onCommand(((String) map.get("text")).substring(1));
                                }
                            }
                        } else {
                            this.connections().forEach(c -> c.send(s));
                            System.out.println(map.get("user") + ":" + map.get("text"));
                        }
                        break;
                    default:
                        System.out.println("Unknown key" + Objects.toString(map.get("type")));
                        break;
                }
            }
    }

    private void startPlayerControl(Entity entity, UUID uuid, WebSocket webSocket) {
        entity.getComponent(PlayerControlled.class).controller = loginServer.getPlayerName(webSocket);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", "startplayercontrol");
        jsonObject.put("uuid", (uuid != null) ? uuid.toString() : null);
        webSocket.send(jsonObject.toJSONString());
    }

    private void sendBaseProgram(WebSocket webSocket) {
        Basis b = getBasis(webSocket);
        if (b.droneProgramName == null || b.droneProgram == null) return;
        JSONObject o = new JSONObject();
        o.put("type", "setbaseprogram");
        o.put("program", b.droneProgram.toJSONString());
        o.put("pname", b.droneProgramName);
        o.put("autobuild", b.autoBuild);
        webSocket.send(o.toJSONString());
    }

    private void sendSchematic(WebSocket webSocket) {
        Basis b = getBasis(webSocket);
        if (b.droneConfiguration == null) return;
        JSONObject o = new JSONObject();
        o.put("type", "setschematic");
        o.put("schematic", b.droneConfiguration.toJSONString());
        webSocket.send(o.toJSONString());
    }

    private Vector2d getBaseLocation(WebSocket webSocket) {
        ImmutableBag<Entity> bag =
                region.getArtemisWorld().getSystem(PlayerManager.class).getEntitiesOfPlayer(loginServer.getPlayerName(webSocket));
        if (!bag.isEmpty()) {
            GroupManager groupManager = region.getArtemisWorld().getSystem(GroupManager.class);
            for (Entity entity : bag) {
                if (groupManager.isInGroup(entity, "base")) {
                    return entity.getComponent(Position.class).pos;
                }
            }
        }
        return new Vector2d();
    }

    private Basis getBasis(WebSocket webSocket) {
        ImmutableBag<Entity> bag =
                region.getArtemisWorld().getSystem(PlayerManager.class).getEntitiesOfPlayer(loginServer.getPlayerName(webSocket));
        if (!bag.isEmpty()) {
            GroupManager groupManager = region.getArtemisWorld().getSystem(GroupManager.class);
            for (Entity entity : bag) {
                if (groupManager.isInGroup(entity, "base")) {
                    return entity.getComponent(Basis.class);
                }
            }
        }
        return null;
    }

    private void openBaseControlModal(Entity entity, UUID uuid, WebSocket webSocket) {
        JSONObject o = new JSONObject();
        o.put("type", "openbasecontrolmodal");
        o.put("uuid", uuid.toString());
        Basis basis = entity.getComponent(Basis.class);
        String owner = entity.getComponent(Owned.class) != null ? entity.getComponent(Owned.class).owner : "unknown";
        o.put("spawnedInBase", (basis != null) ? basis.spawnedInBase : -1);
        int i = 0;
        for (Entity e : entity.getWorld().getSystem(PlayerManager.class).getEntitiesOfPlayer(owner
        ))
            if (e.getComponent(Rendered.class).name.equals("drone"))
                ++i;
        o.put("spawnedInWorld", i);
        Inventory inventory = entity.getComponent(Inventory.class);
        o.put("resources", (inventory != null) ? inventory.currentCapacity : -1);
        o.put("resourcesMax", (inventory != null) ? inventory.maxCapacity : -1);
        ChunkReference chunkReference = entity.getComponent(ChunkReference.class);
        o.put("chunkX", chunkReference.x);
        o.put("chunkY", chunkReference.y);
        webSocket.send(o.toJSONString());
    }

    private void startViewing(Entity entity, UUID uuid, WebSocket webSocket) {
        WebSocketViewer webSocketViewer = inetSocketAddressWebSocketViewerMap.get(webSocket.getRemoteSocketAddress());
        webSocketViewer.unViewAll();
        RenderNearby renderNearby = entity.getComponent(RenderNearby.class);
        webSocketViewer.view(entity.getId(), renderNearby);

        region.getExec().schedule(new Runnable() {
            @Override
            public void run() {
                JSONObject ret = new JSONObject();
                ret.put("type", "startviewing");
                ret.put("uuid", uuid.toString());
                webSocket.send(ret.toJSONString());
            }
        }, 1, TimeUnit.SECONDS);
    }

    public void broadcast(String message) {
        for (int i = 0; i < connections().size(); ++i)
            this.connections().forEach(c -> {
                try {
                    c.send(message);
                } catch (WebsocketNotConnectedException e) {
                }

            });
    }

    public void chatMessage(String message) {
        broadcast("{\"type\":\"chat\",\"user\":\"<u>server</u>\",\"text\":\"" + message + "\"}");
    }


    @Override
    public void onError(WebSocket webSocket, Exception e) {
        e.printStackTrace();
        if (webSocket == null)
            isRunning = false;
    }

    public boolean isRunning() {
        return isRunning;
    }

    class WebSocketViewer implements IViewer {
        private WebSocket webSocket;
        Entity currentlyWatching = null;
        List<Integer> viewingList = new LinkedList<>();

        public WebSocketViewer(WebSocket webSocket) {
            this.webSocket = webSocket;
        }

        @Override
        public void sendMessage(String message) {
            if (webSocket.isOpen())
                webSocket.send(message);
        }

        @Override
        public Entity getExplicitWatching() {
            return currentlyWatching;
        }

        @Override
        public void setExplicitWatching(Entity explicitWatching) {
            Entity e = getExplicitWatching();
            if (e != null)
                e.getComponent(Watchable.class).
                        unwatch(this);

            if (explicitWatching != null) {
                Watchable w = explicitWatching.getComponent(Watchable.class);
                if (w == null) {
                    System.out.println("Entity is not watchable!" + explicitWatching.getId());
                    return;
                }
                w.watch(this);
            }
            currentlyWatching = explicitWatching;
        }


        @Override
        public Collection<Integer> getViewing() {
            return viewingList;
        }


        @Override
        public void view(Integer viewing, RenderNearby renderNearbyComponent) {
            if (viewingList.add(viewing)) {
                Entity e = region.getArtemisWorld().getEntity(viewing);
                //Get component if null
                if (renderNearbyComponent == null)
                    renderNearbyComponent = e.getComponent(RenderNearby.class);
                //Add to component
                renderNearbyComponent.viewerList.add(this);
                //Queue as update
                ComponentMapper<Rendered> renderedComponentMapper = region.getArtemisWorld().getMapper(Rendered.class);
                for (Integer viewedEntity : e.getWorld().getSystem(ChunkSystem.class).getNearbyEntities(e.getComponent(Position.class).pos, renderNearbyComponent.size + 5)) {
                    Rendered rendered = renderedComponentMapper.getSafe(viewedEntity);
                    if (rendered != null)
                        Rendered.addUpdateType(rendered, this, Rendered.UpdateType_ALL);
                }

            }
        }

        @Override
        public void unView(Integer viewing, RenderNearby renderNearbyComponent) {
            if (viewingList.remove(viewing)) {
                Entity e = region.getArtemisWorld().getEntity(viewing);
                //Get component if null
                if (renderNearbyComponent == null)
                    renderNearbyComponent = e.getComponent(RenderNearby.class);
                //Del from component

                if (renderNearbyComponent == null) {
                    renderNearbyComponent = region.getArtemisWorld().getMapper(RenderNearby.class).get(viewing);
                }
                if (renderNearbyComponent == null) {
                    System.out.print("renderNearbyComponent==null");
                    viewingList.clear();
                    return;
                }
                renderNearbyComponent.viewerList.remove(this);
                //Remove the web entities
                if (viewingList.isEmpty()) {
                    //Remove all entities
                    ComponentMapper<Rendered> renderedComponentMapper = region.getArtemisWorld().getMapper(Rendered.class);
                    for (Integer viewedEntity : e.getWorld().getSystem(ChunkSystem.class).getNearbyEntities(e.getComponent(Position.class).pos, renderNearbyComponent.size + 5)) {
                        Rendered rendered = renderedComponentMapper.getSafe(viewedEntity);
                        if (rendered != null)
                            Rendered.addUpdateType(rendered, this, Rendered.UpdateType_REMOVE);
                    }
                } else {
                    Map<Integer, Rendered> toRem = new HashMap<>();
                    ComponentMapper<Rendered> renderedComponentMapper = region.getArtemisWorld().getMapper(Rendered.class);
                    for (Integer viewedEntity : e.getWorld().getSystem(ChunkSystem.class).getNearbyEntities(e.getComponent(Position.class).pos, renderNearbyComponent.size + 5)) {
                        Rendered rendered = renderedComponentMapper.getSafe(viewedEntity);
                        if (rendered != null)
                            toRem.put(viewedEntity, rendered);
                    }
                    for (Integer otherView : viewingList) {
                        Entity otherViewE = region.getArtemisWorld().getEntity(otherView);
                        for (Integer viewedEntity : e.getWorld().getSystem(ChunkSystem.class)
                                .getNearbyEntities(otherViewE.getComponent(Position.class).pos, otherViewE.getComponent(RenderNearby.class).size + 5)) {
                            toRem.remove((Object) viewedEntity);
                        }
                    }
                    //Queue updated for removing from renderer
                    for (Map.Entry<Integer, Rendered> entry : toRem.entrySet())
                        Rendered.addUpdateType(entry.getValue(), this, Rendered.UpdateType_REMOVE);
                }

            }
        }
    }

    public ProgramServer getProgramServer() {
        return programServer;
    }

    public LoginServer getLoginServer() {
        return loginServer;
    }
}