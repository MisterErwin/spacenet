package net.askarian.spaceai.server;

import com.artemis.*;
import com.artemis.io.JsonArtemisSerializer;
import com.artemis.io.SaveFileFormat;
import com.artemis.managers.GroupManager;
import com.artemis.managers.PlayerManager;
import com.artemis.managers.UuidEntityManager;
import com.artemis.managers.WorldSerializationManager;
import com.artemis.utils.EntityBuilder;
import com.artemis.utils.IntBag;
import com.google.common.eventbus.EventBus;
import net.askarian.spaceai.FlowAiServer.programs.Program;
import net.askarian.spaceai.math.Vector2d;
import net.askarian.spaceai.server.rendering.HTMRenderer.HTMLRendererSystem;
import net.askarian.spaceai.server.builder.DroneBuilderFactory;
import net.askarian.spaceai.server.builder.PartRegistry;
import net.askarian.spaceai.server.components.*;
import net.askarian.spaceai.server.components.input.Camera;
import net.askarian.spaceai.server.components.output.Cannon;
import net.askarian.spaceai.server.components.output.Color;
import net.askarian.spaceai.server.components.physics.*;
import net.askarian.spaceai.server.components.rendering.RenderNearby;
import net.askarian.spaceai.server.components.rendering.Rendered;
import net.askarian.spaceai.server.components.rendering.Watchable;
import net.askarian.spaceai.server.systems.*;
import net.askarian.spaceai.server.systems.onload.ReInvokeProgramSystem;
import net.askarian.spaceai.server.systems.onload.SetOwnerSystem;
import net.askarian.spaceai.server.util.SpawnUtils;
import org.apache.commons.lang.math.RandomUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by alex on 9/27/15.
 */
public class Region {
    //Constants
    public static final double spaceFriction = .9;

    private World artemisWorld;
    final List<IControlServer> controlServerList;
    private EventBus eventBus = new EventBus();

    public WSServer server;

    private final File saveFile;
    public PartRegistry partRegistry;
    final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);


    public Region(File saveFile) {
        this.saveFile = saveFile;
        controlServerList = new ArrayList<>();
        setupWorld();

    }

    public void init(WSServer server) {
        if (this.server != null) throw new IllegalAccessError("Wrong timing");
        this.server = server;
        if (saveFile.exists())
            load();
    }

    private void load() {
        try {
            FileInputStream fileInputStream = new FileInputStream(this.saveFile);
            SaveFileFormat load = artemisWorld.getSystem(WorldSerializationManager.class).load(fileInputStream, SaveFileFormat.class);
            artemisWorld.process();
            System.out.println("Loaded " + artemisWorld.getAspectSubscriptionManager().get(Aspect.all()).getEntities().size() + " entities");
            PlayerManager playerManager = artemisWorld.getSystem(PlayerManager.class);
            IntBag bag = artemisWorld.getAspectSubscriptionManager().get(Aspect.all()).getEntities();
            for (int i = 0; i < bag.size(); ++i) {
                Entity e = artemisWorld.getEntity(bag.get(i));
                artemisWorld.getSystems().forEach(baseSystem -> {
                    if (baseSystem instanceof INotifyAboutSerAddSystem)
                        ((INotifyAboutSerAddSystem) baseSystem).reAddAfterLoad(e, this);
                });
            }
        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println("Could not load savegame");
        }
    }

    public void save() {
        artemisWorld.getSystems().forEach(baseSystem -> {
            if (baseSystem instanceof INotifyOnSave) ((INotifyOnSave) baseSystem).onBeforeSave();
        });
        PrintWriter printWriter = null;
        System.out.println("Saving #" + artemisWorld.getAspectSubscriptionManager().get(Aspect.all()).getEntities().size());

        try {
            printWriter = new PrintWriter(saveFile, "UTF-8");
            artemisWorld.getSystem(WorldSerializationManager.class).save(printWriter,
                    new SaveFileFormat(artemisWorld.getAspectSubscriptionManager().get(Aspect.all())));
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            if (printWriter != null)
                printWriter.close();
        }
        System.out.println("--save end");
    }


    private void setupWorld() {
        WorldConfiguration configuration = new WorldConfiguration();

        configuration.setSystem(WorldSerializationManager.class);

        configuration.setSystem(new RegionReference(this));

        configuration.setSystem(GroupManager.class);
        configuration.setSystem(UuidEntityManager.class);
        configuration.setSystem(PlayerManager.class);


        configuration.setSystem(new ChunkSystem(100));

        configuration.setSystem(ReInvokeProgramSystem.class).setSystem(SetOwnerSystem.class);


        //
        configuration.setSystem(ResetRenderNearbySystem.class);
        configuration.setSystem(RenderNearbySystem.class);

        configuration.setSystem(PhysicsIntegrationSystem.class);

        configuration.setSystem(ThrusterSystem.class).setSystem(MassSystem.class);
        configuration.setSystem(BasisSystem.class);

        configuration.setSystem(ControlSystem.class);
        configuration.setSystem(new LinearAIControlSystem());


        configuration.setSystem(new CameraSystem());
        configuration.setSystem(new WaypointSystem());

        configuration.setSystem(new PickupNearbySystem());
        configuration.setSystem(new TransferNearbySystem());

        configuration.setSystem(new DespawnTimeSystem());
        configuration.setSystem(new WeaponSystem());
        configuration.setSystem(new DamageNearbySystem()).setSystem(new ShotAtNearbySystem()).setSystem(new HealthSystem());


        configuration.setSystem(new HTMLRendererSystem());
        configuration.setSystem(new ResetEnergyEmissionSystem());


        //LAST


        artemisWorld = new World(configuration);

        JsonArtemisSerializer backend = new JsonArtemisSerializer(artemisWorld);
        backend.prettyPrint(true);


        artemisWorld.getSystem(WorldSerializationManager.class).setSerializer(backend);

        for (BaseSystem baseSystem : artemisWorld.getSystems())
            eventBus.register(baseSystem);
    }

    public int addDrone() {
        return addDrone(new Vector2d(), "console");
    }

    public int addDrone(Vector2d pos, String owner) {
        EntityBuilder entityBuilder = new EntityBuilder(artemisWorld);
        entityBuilder.with(ChunkReference.class, Velocity.class, Acceleration.class, ForceAccumulator.class, Mass.class, EnergyEmission.class, ShootAtNearby.class);

        entityBuilder.with(new Rendered("drone", Rendered.SHAPE_CIRCLE, 15), new Watchable(), new Position(pos), new SpawnWreck(), new Owned(owner));

//        entityBuilder.with(SpinItAround.class);
        entityBuilder.with(new Color()).with(new Camera()).with(new Health(100), new Cannon(2)).player(owner);

        entityBuilder.group("drone");

        Entity e = entityBuilder.build();

        return e.getId();
    }

    public void addWreck(Entity oldEntity) {
        EntityBuilder entityBuilder = new EntityBuilder(artemisWorld);

        entityBuilder.with(ChunkReference.class, ForceAccumulator.class);


        Velocity velocity = artemisWorld.getMapper(Velocity.class).getSafe(oldEntity);
        entityBuilder.with(new Velocity(velocity == null ? new Vector2d() : velocity.vector.clone().multSelf(0.8)));

        Acceleration acceleration = artemisWorld.getMapper(Acceleration.class).getSafe(oldEntity);
        entityBuilder.with(new Acceleration(acceleration == null ? new Vector2d() : acceleration.acc.clone().multSelf(0.5)));
        MassSystem massSystem = artemisWorld.getSystem(MassSystem.class);

        Mass mass = new Mass(1);
        Pickupable pickupable = new Pickupable();
        pickupable.pickupAbles = new HashMap<>();

        if (oldEntity.getComponent(Inventory.class) != null)
            for (Map.Entry<String, Integer> e : oldEntity.getComponent(Inventory.class).inv.entrySet()) {
                int i = (int) Math.round(e.getValue() * RandomUtils.nextDouble());
                massSystem.addMass(mass, e.getKey(), i);
                pickupable.pickupAbles.put(e.getKey(), i);
            }

        entityBuilder.with(mass).with(pickupable);

        entityBuilder.with(Color.newFromIndex(13));


        entityBuilder.with(new Rendered("wreck", Rendered.SHAPE_RECT, 5), new Position(oldEntity.getComponent(Position.class).pos), new Health(10));
    }


    public int addAIDrone(Program program, String who, Vector2d pos, Basis basis) {
        if (who == null)return -1;
        who = who.replaceAll("[^a-zA-Z0-9]+", "");
        EntityBuilder entityBuilder = new EntityBuilder(artemisWorld);
        entityBuilder.with(ChunkReference.class, Velocity.class, Acceleration.class, ForceAccumulator.class, EnergyEmission.class);

        entityBuilder.with(new Rendered("drone", Rendered.SHAPE_CIRCLE, 15), new Watchable(),
                new Position(pos),
                new DespawnTime(20000), new SpawnWreck());

        entityBuilder.groups("drone", "ai");
        entityBuilder.player(who);
        entityBuilder.with(new Owned(who));

        DroneBuilderFactory.build(partRegistry, entityBuilder, basis, program);


        entityBuilder.with(new Color()); //.with(new Camera()).with(new Health(100), new Cannon(2), nodeAI);

        Entity e = entityBuilder.build();

        program.setArtemisEntity(e);

        return e.getId();
    }


//    public int addTV(String owner, IViewer... iViewers) {
//        EntityBuilder entityBuilder = new EntityBuilder(artemisWorld);
//        entityBuilder.with(ChunkReference.class, Position.class, Velocity.class, Acceleration.class, ForceAccumulator.class, Mass.class, Owned.class);
//
//        entityBuilder.with(new Rendered("tv"), new RenderNearby(800, iViewers));
//
//        entityBuilder.group("TV");
//        entityBuilder.player(owner);
//
//        Entity e = entityBuilder.build();
//
//        return e.getId();
//    }

    public Entity addBase(String owner) {
        Vector2d vector2d = SpawnUtils.getFreePosition(artemisWorld.getSystem(ChunkSystem.class), 100, 1000, 10, null, artemisWorld.getMapper(Basis.class));
        if (vector2d != null)
            return addBase(owner, vector2d);
        else
            return null;
    }

    public Entity addBase(String owner, Vector2d pos) {

        System.out.println("Creating base for " + owner);

        EntityBuilder entityBuilder = new EntityBuilder(artemisWorld);
        entityBuilder.with(ChunkReference.class, Velocity.class, Acceleration.class, ForceAccumulator.class, Mass.class, Owned.class/*, ShootAtNearby.class*/);

        entityBuilder.with(new Position(pos), new Rendered("base",Rendered.SHAPE_RECT,10 ), new RenderNearby(800), new Basis(), new Cannon(2, 50, 1, 200, 100));

        entityBuilder.player(owner);
        entityBuilder.with(new Owned(owner));
        entityBuilder.groups("base", "TV");

        return entityBuilder.build();

    }

    public Entity addStar( Vector2d pos, int innerColour, int outerColour, int radius) {

        EntityBuilder entityBuilder = new EntityBuilder(artemisWorld);
        entityBuilder.with(ChunkReference.class, Velocity.class, Acceleration.class, ForceAccumulator.class, Mass.class);

        entityBuilder.with(new Position(pos), new Rendered("star", Rendered.SHAPE_CIRCLE, radius*2), new Color(outerColour), new Star(innerColour, outerColour, radius), new DamageNearby(5, radius+50, "star", false));

        entityBuilder.groups("star", "environment");

        return entityBuilder.build();

    }

    public void process(float delta) {
        artemisWorld.setDelta(delta);
        artemisWorld.process();
    }

    public void stop() {
        artemisWorld.dispose();
    }

    public World getArtemisWorld() {
        return artemisWorld;
    }

    public static class RegionReference extends BaseSystem {
        Region r;

        public RegionReference(Region r) {
            this.r = r;
        }

        public Region getRegion() {
            return r;
        }

        @Override
        protected boolean checkProcessing() {
            return false;
        }

        @Override
        protected void processSystem() {

        }
    }

    public File getSaveFile() {
        return saveFile;
    }

    public ScheduledExecutorService getExec() {
        return exec;
    }

    public static interface INotifyOnSave {
        void onBeforeSave();
    }

    public void postEvent(Object event){
        eventBus.post(event);
    }
}
