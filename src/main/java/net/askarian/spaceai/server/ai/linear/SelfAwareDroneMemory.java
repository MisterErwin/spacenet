package net.askarian.spaceai.server.ai.linear;

import com.artemis.Entity;
import com.google.common.collect.Maps;
import org.apache.commons.lang.math.NumberUtils;

import java.util.Map;

/**
 * Created by alex on 10/30/15.
 */
public class SelfAwareDroneMemory implements IDroneMemory, ISelfAware {

    Map<String, AIData> dataKeyMap;
    Entity drone;

    public SelfAwareDroneMemory(Entity drone) {
        dataKeyMap = Maps.newHashMap();
        this.drone = drone;
    }

    @Override
    public AIData getData(String name) {
        int n = NumberUtils.toInt(name, Integer.MIN_VALUE);
        if (n != Integer.MIN_VALUE)
            return new AIData(n);
        AIData data = dataKeyMap.get(name);
        if (data != null) return data;
        return create(name, new AIData(0));
    }

    @Override
    public AIData create(String name, AIData data) {
        dataKeyMap.put(name, data);
        return data;
    }

    @Override
    public Entity getEntity() {
        return this.drone;
    }
}
