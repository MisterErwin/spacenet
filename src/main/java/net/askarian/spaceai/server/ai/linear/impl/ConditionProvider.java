package net.askarian.spaceai.server.ai.linear.impl;

import com.google.common.collect.Maps;
import net.askarian.spaceai.Utils;
import net.askarian.spaceai.server.ai.linear.IDroneMemory;

import java.util.List;
import java.util.Map;

/**
 * Created by alex on 10/30/15.
 */
public class ConditionProvider {
    static Map<String, ICondition> conditionMap = Maps.newHashMap();

    static {
        conditionMap.put("equals", new ICondition() {
            @Override
            public boolean evaluate(List<IDroneMemory.AIData> datas) {
                return (datas.size() > 1 && datas.get(0).isInt() && datas.get(1).isInt() && datas.get(0).get() == datas.get(1).get());
            }
        });
        conditionMap.put("greater", new ICondition() {
            @Override
            public boolean evaluate(List<IDroneMemory.AIData> datas) {
                return (datas.size() > 1 && datas.get(0).isInt() && datas.get(1).isInt() && datas.get(0).getInt() > datas.get(1).getInt());
            }
        });
        conditionMap.put("lesser", new ICondition() {
            @Override
            public boolean evaluate(List<IDroneMemory.AIData> datas) {
                return (datas.size() > 1 && datas.get(0).isInt() && datas.get(1).isInt() && (Integer) datas.get(0).get() < (Integer) datas.get(1).get());
            }
        });
        conditionMap.put("contains", new ICondition() {
            @Override
            public boolean evaluate(List<IDroneMemory.AIData> datas) {
                return (datas.size() > 1 &&
                        ((datas.get(0).isInt() && datas.get(1).isArray() && Utils.containsPrimitive((int[]) datas.get(1).get(), (Integer) datas.get(0).get())) ||
                                (datas.get(1).isInt() && datas.get(0).isArray() && Utils.containsPrimitive((int[]) datas.get(0).get(), (Integer) datas.get(1).get()))));
            }
        });

    }

    public static ICondition get(String name){
        return conditionMap.get(name);
    }


    public interface ICondition {
        boolean evaluate(List<IDroneMemory.AIData> datas);
    }
}
