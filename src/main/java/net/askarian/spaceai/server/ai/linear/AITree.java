package net.askarian.spaceai.server.ai.linear;

import net.askarian.spaceai.server.ai.linear.impl.ConditionProvider;
import net.askarian.spaceai.server.ai.linear.impl.FunctionProvider;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alex on 10/30/15.
 */
public class AITree {

    AITreeNode rootNode;

    public AITree(AITreeNode rootNode) {
        this.rootNode = rootNode;
    }

    public void tick(IDroneMemory memory) {
        if (rootNode != null)
            rootNode.tick(memory);
    }

    @Override
    public String toString() {
        return "AITree{" +
                "rootNode=" + rootNode +
                '}';
    }

    public static class AITreeNode {
        List<AITreeNode> children;
        String[] params;

        public AITreeNode(List<AITreeNode> children, String[] params) {
            this.children = children;
            this.params = params;
        }


        void tick(IDroneMemory memory) {
            if (params != null && params.length > 0) {
                if (params[0].equals("if")) { //Condition
                    if (params.length == 1) return;
                    ConditionProvider.ICondition condition = ConditionProvider.get(params[1]);
                    if (condition != null && !children.isEmpty()) {
                        List<IDroneMemory.AIData> datas = new LinkedList<>();
                        for (int i = 2; i < params.length; ++i)
                            datas.add(memory.getData(params[i]));
                        //If true, continue to child
                        if (condition.evaluate(datas))
                            for (AITreeNode child : children)
                                child.tick(memory);
                    }
                    return;
                }else if (params[0].equals("ifnot")) { //Condition
                    if (params.length == 1) return;
                    ConditionProvider.ICondition condition = ConditionProvider.get(params[1]);
                    if (condition != null && !children.isEmpty()) {
                        List<IDroneMemory.AIData> datas = new LinkedList<>();
                        for (int i = 2; i < params.length; ++i)
                            datas.add(memory.getData(params[i]));
                        //If false, continue to child
                        if (!condition.evaluate(datas))
                            for (AITreeNode child : children)
                                child.tick(memory);
                    }
                    return;
                } else {
                    //Function
                    FunctionProvider.IFunction function = FunctionProvider.get(params[0]);
                    if (function != null) {
                        List<IDroneMemory.AIData> datas = new LinkedList<>();
                        for (int i = 1; i < params.length; ++i)
                            datas.add(memory.getData(params[i]));
                        function.work(datas, memory);
                    }
                }
            }
            //Last (and least)
            if (children != null)
                for (AITreeNode child : children)
                    child.tick(memory);
        }

        @Override
        public String toString() {
            return "AITreeNode{" +
                    "children=" + children +
                    ", params=" + Arrays.toString(params) +
                    '}';
        }
    }
}
