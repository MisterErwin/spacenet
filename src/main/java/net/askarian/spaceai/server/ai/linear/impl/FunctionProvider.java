package net.askarian.spaceai.server.ai.linear.impl;

import com.google.common.collect.Maps;
import net.askarian.spaceai.Utils;
import net.askarian.spaceai.server.ai.linear.IDroneMemory;
import net.askarian.spaceai.server.ai.linear.ISelfAware;
import net.askarian.spaceai.server.components.Health;
import net.askarian.spaceai.server.components.Inventory;
import net.askarian.spaceai.server.components.input.Camera;
import net.askarian.spaceai.server.components.input.Waypoint;
import net.askarian.spaceai.server.components.output.Cannon;
import net.askarian.spaceai.server.components.output.Color;
import net.askarian.spaceai.server.components.output.Thrusters;
import net.askarian.spaceai.server.components.physics.Position;
import net.askarian.spaceai.server.components.physics.Velocity;
import net.askarian.spaceai.server.components.rendering.Rendered;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by alex on 10/30/15.
 */
public class FunctionProvider {
    static Map<String, IFunction> functionMap = Maps.newHashMap();

    static {
        functionMap.put("set", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                if (datas.size() > 1 && datas.get(1).isInt())
                    datas.get(0).set(datas.get(1).getInt());
            }
        });
        functionMap.put("addself", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                if (datas.size() > 1 && datas.get(0).isInt() && datas.get(1).isInt())
                    datas.get(0).set(datas.get(0).getInt() + datas.get(1).getInt());
            }
        });
        functionMap.put("subself", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                if (datas.size() > 1 && datas.get(0).isInt() && datas.get(1).isInt())
                    datas.get(0).set(datas.get(0).getInt() - datas.get(1).getInt());
            }
        });
        functionMap.put("multself", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                if (datas.size() > 1 && datas.get(0).isInt() && datas.get(1).isInt())
                    datas.get(0).set(datas.get(0).getInt() * datas.get(1).getInt());
            }
        });
        functionMap.put("divself", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                if (datas.size() > 1 && datas.get(0).isInt() && datas.get(1).isInt() && datas.get(1).getInt() != 0)
                    datas.get(0).set((Integer) (int) (datas.get(0).getInt() / (double) datas.get(1).getInt()));
            }
        });

        functionMap.put("add", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                if (datas.size() > 2 && datas.get(1).isInt() && datas.get(2).isInt())
                    datas.get(0).set(datas.get(1).getInt() + datas.get(2).getInt());
            }
        });
        functionMap.put("sub", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                if (datas.size() > 2 && datas.get(1).isInt() && datas.get(2).isInt())
                    datas.get(0).set(datas.get(1).getInt() - datas.get(2).getInt());
            }
        });
        functionMap.put("mult", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                if (datas.size() > 2 && datas.get(1).isInt() && datas.get(2).isInt())
                    datas.get(0).set(datas.get(1).getInt() * datas.get(2).getInt());
            }
        });
        functionMap.put("div", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                if (datas.size() > 2 && datas.get(1).isInt() && datas.get(2).isInt() && datas.get(2).getInt() != 0)
                    datas.get(0).set((Integer) (int) (datas.get(1).getInt() / (double) datas.get(2).getInt()));
            }
        });

        functionMap.put("print", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                for (IDroneMemory.AIData data : datas)
                    if (data.isArray())
                        System.out.println(Arrays.toString(data.getIntArray()));
                    else
                        System.out.println(data.get());
            }
        });


        functionMap.put("greatest", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                Integer max = 0;
                for (int i = 1; i < datas.size(); ++i)
                    if (datas.get(i).isInt() && datas.get(i).getInt() > max)
                        max = datas.get(i).getInt();
                if (!datas.isEmpty())
                    datas.get(0).set(max);
            }
        });


        functionMap.put("max", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                if (datas.size() > 1 && datas.get(0).isInt() && datas.get(1).isArray())
                    datas.get(0).set(Utils.maxValue(datas.get(1).getIntArray()));
            }
        });
        functionMap.put("maxindex", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                if (datas.size() > 1 && datas.get(0).isInt() && datas.get(1).isArray())
                    datas.get(0).set(Utils.maxIndex(datas.get(1).getIntArray()));
            }
        });
        functionMap.put("get", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas) {
                if (datas.size() > 2 && datas.get(2).isInt() && datas.get(1).isArray())
                    datas.get(0).set(datas.get(1).getIntArray()[datas.get(2).getInt()]);
            }
        });


        functionMap.put("in_color", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas, IDroneMemory memory) {
                if (datas.size() > 0 && memory instanceof ISelfAware) {
                    Camera cameraC = ((ISelfAware) memory).getComponent(Camera.class);
                    if (cameraC != null)
                        datas.get(0).set(cameraC.colorData);
                }
            }
        });

        functionMap.put("in_waypoint", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas, IDroneMemory memory) {
                if (datas.size() > 0 && memory instanceof ISelfAware) {
                    Waypoint waypoint = ((ISelfAware) memory).getComponent(Waypoint.class);
                    if (waypoint != null) {
                        datas.get(0).set(waypoint.angle);
                        if (datas.size() > 1)
                            datas.get(1).set(waypoint.dist);
                    }
                }
            }
        });

        functionMap.put("in_velocity", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas, IDroneMemory memory) {
                if (!(memory instanceof ISelfAware)) return;
                Velocity velocity = ((ISelfAware) memory).getComponent(Velocity.class);
                if (velocity == null) return;
                if (datas.size() > 0) {
                    datas.get(0).set(Integer.valueOf((int) Math.toDegrees(Math.atan2(velocity.vector.getY(), velocity.vector.getX()))));
                    if (datas.size() > 1)
                        datas.get(1).set((int) (velocity.vector.length() * 100));
                }
            }
        });

        functionMap.put("in_dist", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas, IDroneMemory memory) {
                if (datas.size() > 0 && memory instanceof ISelfAware) {
                    Camera cameraC = ((ISelfAware) memory).getComponent(Camera.class);
                    if (cameraC != null)
                        datas.get(0).set(cameraC.data);
                }
            }
        });

        functionMap.put("in_capacity", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas, IDroneMemory memory) {
                if (memory instanceof ISelfAware && !datas.isEmpty()) {
                    Inventory inventory = ((ISelfAware) memory).getComponent(Inventory.class);
                    if (inventory != null)
                        datas.get(0).set((int)inventory.currentCapacity/inventory.maxCapacity*100);
                }
            }
        });

        functionMap.put("in_health", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas, IDroneMemory memory) {
                if (datas.size() > 0 && memory instanceof ISelfAware) {
                    Health health = ((ISelfAware) memory).getComponent(Health.class);
                    if (health != null)
                        datas.get(0).set(health.health);
                }
            }
        });

        functionMap.put("out_color", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas, IDroneMemory memory) {
                if (datas.size() > 0 && memory instanceof ISelfAware && datas.get(0).isInt()) {
                    Color color = ((ISelfAware) memory).getComponent(Color.class);
                    if (color != null)
                        color.setFromIndex(((ISelfAware) memory).getComponent(Rendered.class), Math.max(0, Math.min(11, datas.get(0).getInt())));
                }
            }
        });

        functionMap.put("out_cannon", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas, IDroneMemory memory) {
                if (datas.size() > 0 && memory instanceof ISelfAware && datas.get(0).isInt()) {
                    Cannon cannon = ((ISelfAware) memory).getComponent(Cannon.class);
                    if (cannon != null)
                        cannon.dir = datas.get(0).getInt();
                }
            }
        });

        functionMap.put("out_thrusters", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas, IDroneMemory memory) {
                if (datas.size() > 1 && memory instanceof ISelfAware && datas.get(0).isInt() && datas.get(1).isInt()) {
                    Thrusters thrusters = ((ISelfAware) memory).getComponent(Thrusters.class);
                    if (thrusters != null) {
                        thrusters.dir = datas.get(0).getInt();
                        thrusters.strength = datas.get(1).getInt() / 100d;
                    }
                }
            }
        });

        functionMap.put("out_waypoint", new IFunction() {
            @Override
            public void work(List<IDroneMemory.AIData> datas, IDroneMemory memory) {
                if (memory instanceof ISelfAware && datas.size() > 0 && datas.get(0).isInt()) {
                    Waypoint waypoint = ((ISelfAware) memory).getComponent(Waypoint.class);
                    Position pos = ((ISelfAware) memory).getComponent(Position.class);
                    if (waypoint != null && pos != null && datas.get(0).getInt()<waypoint.amount)
                        waypoint.waypoints[datas.get(0).getInt()] = pos.pos.clone();
                }
            }
        });

    }

    public static IFunction get(String name) {
        return functionMap.get(name);
    }

    public interface IFunction {
        default void work(List<IDroneMemory.AIData> datas) {
            throw new UnsupportedOperationException("Nothing implemented");
        }

        default void work(List<IDroneMemory.AIData> datas, IDroneMemory memory) {
            this.work(datas);
        }
    }

}
