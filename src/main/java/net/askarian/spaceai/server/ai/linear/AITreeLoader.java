package net.askarian.spaceai.server.ai.linear;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * Created by alex on 10/30/15.
 */
public class AITreeLoader {

    public final static Gson gson;

    public static final Type type = new TypeToken<AITree>() { /*Empty intended*/
    }.getType();

    public static AITree load(String json){
        System.out.println(json);
        return gson.fromJson(json, type);
    }

    static {
        GsonBuilder gsonBuilder = new GsonBuilder();

//        gsonBuilder.registerTypeAdapter(AITree.AITreeNode.class, new TypeAdapter<AITree.AITreeNode>() {
//            @Override
//            public void write(JsonWriter jsonWriter, AITree.AITreeNode treeNode) throws IOException {
//                if (treeNode == null) {
//                    jsonWriter.nullValue();
//                    return;
//                }
//                jsonWriter.beginObject();
//                jsonWriter.name("children");
//                jsonWriter.beginArray();
//                if (treeNode.children != null)
//                    for (AITree.AITreeNode node : treeNode.children)
//                        write(jsonWriter, node);
//                jsonWriter.endArray();
//
//
//                jsonWriter.name("params");
//                jsonWriter.beginArray();
//                if (treeNode.params != null)
//                    for (String p : treeNode.params)
//                        jsonWriter.value(p);
//                jsonWriter.endArray();
//
//                jsonWriter.endObject();
//            }
//
//
//            @Override
//            public AITree.AITreeNode read(JsonReader jsonReader) throws IOException {
//                if (jsonReader.peek() == JsonToken.NULL) {
//                    jsonReader.nextNull();
//                    return null;
//                }
//                jsonReader.beginObject();
//                jsonReader.beginArray();
//                List<AITree.AITreeNode> children = new ArrayList<AITree.AITreeNode>();
//                while (jsonReader.hasNext())
//                    children.add(read(jsonReader));
//                jsonReader.endArray();
//
//                List<String> params = new ArrayList<String>();
//                jsonReader.beginArray();
//                while(jsonReader.hasNext())
//                    params.add(jsonReader.nextString());
//                jsonReader.endArray();
//
//                return new AITree.AITreeNode(children.isEmpty()?null:children, params.isEmpty()?null:params.toArray(new String[params.size()]));
//            }
//        });

        gson = gsonBuilder.create();

    }

}
