package net.askarian.spaceai.server.ai.linear;

import com.google.common.collect.Maps;
import org.apache.commons.lang.math.NumberUtils;

import java.util.Map;

/**
 * Created by alex on 10/30/15.
 */
public class SimpleDroneMemory implements IDroneMemory {

    Map<String, AIData> dataKeyMap;

    public SimpleDroneMemory() {
        dataKeyMap = Maps.newHashMap();
    }

    @Override
    public AIData getData(String name) {
        int n = NumberUtils.toInt(name);
        if (n != 0)
            return new AIData(n);
        AIData data = dataKeyMap.get(name);
        if (data != null) return data;
        return create(name, new AIData(0));
    }

    @Override
    public AIData create(String name, AIData data) {
        dataKeyMap.put(name, data);
        return data;
    }

}
