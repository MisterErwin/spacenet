package net.askarian.spaceai.server.ai.linear;

import com.artemis.Component;
import com.artemis.Entity;

/**
 * Created by alex on 10/30/15.
 */
public interface ISelfAware {
    Entity getEntity();

    default <T extends Component> T getComponent(Class<T> type) {
        return getEntity().getComponent(type);
    }
}
