package net.askarian.spaceai.server.ai.linear;

/**
 * Created by alex on 10/30/15.
 */
public interface IDroneMemory {
    AIData getData(String name);

    AIData create(String name, AIData data);


     class AIData{
        Object data;

        public AIData(Object d) {
            this.data = d;
        }

        public boolean isArray(){
            return data instanceof Object[] || data instanceof int[];
        }

        public boolean isInt(){
            return data instanceof Integer;
        }

        public Object get() {
            return this.data;
        }

        public Integer getInt(){
            return (Integer) this.data;
        }

        public int[] getIntArray(){
            return (int[]) this.data;
        }

        public void set(Object d) {
            if (d.getClass().isPrimitive())
                throw new UnsupportedOperationException("No primitives supported");
            this.data = d;
        }
    }

}
