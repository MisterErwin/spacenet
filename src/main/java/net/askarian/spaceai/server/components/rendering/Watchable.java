package net.askarian.spaceai.server.components.rendering;

import com.artemis.Component;
import net.askarian.spaceai.server.IViewer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 9/28/15.
 */
public class Watchable extends Component {
    public transient List<IViewer> watchers = new ArrayList<>();
    public transient List<String> updateTypes = new ArrayList<>();

    public Watchable() {
    }

    public void update(String type) {
        if (!updateTypes.contains(type))
            updateTypes.add(type);
    }

    public static final String WatchUT_CAMERA = "CAMERA";


    public void unwatch(IViewer viewer) {
        watchers.remove(viewer);
    }

    public void watch(IViewer watcher) {
        watchers.add(watcher);
    }

}
