package net.askarian.spaceai.server.components;

import com.artemis.Component;

/**
 * Created by alex on 11/1/15.
 */
public class Case extends Component{
    public int size, health;

    public Case(int size, int health) {
        this.size = size;
        this.health = health;
    }

    public Case() {
    }
}
