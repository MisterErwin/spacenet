package net.askarian.spaceai.server.components.output;

import com.artemis.Component;
import net.askarian.spaceai.server.IViewer;
import net.askarian.spaceai.server.components.rendering.Rendered;
import org.apache.commons.lang.math.RandomUtils;

/**
 * Created by alex on 9/28/15.
 */
public class Color extends Component {
    public int color = 0xFFFFFF;
    public int radius;

    public static final int[] colors = new int[]{0xFFFFFF, 0xFF0000, 0x800000, 0xFFFF00, 0x808000, 0x00FF00, 0x008000, 0x00FFFF, 0x008080, 0x0000FF, 0x000080, 0xFF00FF, 0x800080, 0x0c0c0c};


    public Color(int color) {
        this.color = color;
    }

    public Color() {
        fromIndex(RandomUtils.nextInt(11));
    }

    public void fromIndex(int index){
        if (index > 0 && index < colors.length)
            this.color = colors[index];
        else
            this.color = 0x808080;
    }

    public void set(Rendered component, int color) {
        if (component==null)return;

        this.color=color;
        Rendered.addUpdateType(component, (IViewer)null, Rendered.UpdateType_COLOR);
    }

    public void setFromIndex(Rendered component, int index) {
        if (component==null)return;

        fromIndex(index);
        Rendered.addUpdateType(component, (IViewer)null, Rendered.UpdateType_COLOR);
    }

    public static Color newFromIndex(int index){
        Color c = new Color();
        c.fromIndex(index);
        return c;
    }
}
