package net.askarian.spaceai.server.components;

import com.artemis.Component;

/**
 * Created by MisterErwin on 26.12.2015.
 * In case you need it, ask me ;)
 */
public class ShootAtNearby extends Component {
    public  int radius = 100;
    public boolean friendlyFire = false;

    @Override
    public String toString() {
        return "ShootAtNearby{" +
                "radius=" + radius +
                ", friendlyFire=" + friendlyFire +
                '}';
    }
}
