package net.askarian.spaceai.server.components.physics;

import com.artemis.Component;
import net.askarian.spaceai.math.Vector2d;

/**
 * Created by alex on 9/27/15.
 */
public class Velocity extends Component {
    public Vector2d vector;

    public Velocity(Vector2d vector) {
        this.vector = vector;
    }

    public Velocity() {
        vector=new Vector2d();
    }
}
