package net.askarian.spaceai.server.components;

import com.artemis.Component;

/**
 * Created by alex on 9/27/15.
 */
public class DespawnTime extends Component {
    public int time;

    public DespawnTime() {
    }

    public DespawnTime(int time) {
        this.time = time;
    }
}
