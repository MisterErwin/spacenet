package net.askarian.spaceai.server.components;

import com.artemis.Component;

import java.util.Objects;

/**
 * Created by alex on 9/27/15.
 */
public class Health extends Component implements IObjectInfoComponent {
    public double maxHealth=100, health=maxHealth;
    transient public String lastDamageReason;
    transient public Integer lastDamagedBy;

    public Health(double maxHealth) {
        this.maxHealth = maxHealth;
        this.health = maxHealth;
    }

    public Health() {
    }

    public void damage(int me, double damageDealt, String reason, Integer damager) {
        lastDamageReason = reason;
        lastDamagedBy = damager;
        health -= damageDealt;
        System.out.println(me + " was damaged by " + Objects.toString(damager) + " for " + Objects.toString(reason) + " dealing " + damageDealt);
    }

    public void damage(int me, double damageDealt, Integer damager){
        this.damage(me, damageDealt, "unknown", damager);
    }

    public void damage(int me, double damageDealt){
        this.damage(me, damageDealt, "unknown", null);
    }

    @Override
    public String toString() {
        return "Health{" +
                "maxHealth=" + maxHealth +
                ", health=" + health +
                ", lastDamageReason='" + lastDamageReason + '\'' +
                ", lastDamagedBy=" + lastDamagedBy +
                '}';
    }

    @Override
    public void appendInfo(StringBuilder stringBuilder) {
        int n = 30, a =(int)Math.round(n * (health/maxHealth));
        stringBuilder.append("<span color='red'>");
        for (int i=0;i<a;++i)
            stringBuilder.append("=");

        stringBuilder.append("</span>").append("<span color='gray'>");
        for (int i=a;i<n;++i)
            stringBuilder.append("=");
        stringBuilder.append("</span><br/>");
    }
}
