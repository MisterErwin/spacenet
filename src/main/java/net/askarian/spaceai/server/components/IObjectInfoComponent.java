package net.askarian.spaceai.server.components;

/**
 * Created by MisterErwin on 31.12.2015.
 * In case you need it, ask me ;)
 */
public interface IObjectInfoComponent {

    void appendInfo(StringBuilder stringBuilder);

}
