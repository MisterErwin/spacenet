package net.askarian.spaceai.server.components;

import com.artemis.Component;

/**
 * Created by alex on 11/15/15.
 */
public class PlayerControlled extends Component implements IObjectInfoComponent {
    transient public String controller;

    @Override
    public void appendInfo(StringBuilder stringBuilder) {
        stringBuilder.append("<small><b>Right click</b> to take over the thrusters</small><br/>");
    }
}
