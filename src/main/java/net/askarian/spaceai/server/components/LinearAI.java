package net.askarian.spaceai.server.components;

import com.artemis.Component;
import net.askarian.spaceai.server.ai.linear.AITree;
import net.askarian.spaceai.server.ai.linear.SelfAwareDroneMemory;

/**
 * Created by alex on 10/31/15.
 */
public class LinearAI extends Component implements IObjectInfoComponent{
    public SelfAwareDroneMemory memory = null;
    public AITree tree;
    public int delay;

    public LinearAI() {
    }

    public LinearAI(AITree tree) {
        this.tree = tree;
    }

    @Override
    public void appendInfo(StringBuilder stringBuilder) {
        stringBuilder.append("LinearAI<br/> ");
    }
}
