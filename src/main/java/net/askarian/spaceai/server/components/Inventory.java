package net.askarian.spaceai.server.components;

import com.artemis.Component;

import java.util.HashMap;
import java.util.Map;


public class Inventory extends Component {
    public int maxCapacity, currentCapacity = 0;
    public Map<String, Integer> inv;
    public int pickupRange;
    public boolean activePickup = true;

    public Inventory() {
    }

    public Inventory(int maxCapacity, int pickupRange) {
        this.maxCapacity = maxCapacity;
        this.pickupRange = pickupRange;
        inv = new HashMap<>();
    }

    public Inventory(int maxCapacity, int pickupRange, boolean activePickup) {
        this.maxCapacity = maxCapacity;
        this.pickupRange = pickupRange;
        this.activePickup = activePickup;
        inv = new HashMap<>();
    }


    public void add(String name, int amount) {
        Integer i = inv.get(name);
        inv.put(name, i == null ? amount : i + amount);
        currentCapacity += amount;
    }

    public int remove(String name, int amount) {
        Integer i = inv.get(name);
        if (i != null) {
            if (amount > i)
                amount = i;
            if (amount == i)
                inv.remove(name);
            else
                inv.put(name, i - amount);
            currentCapacity -= amount;

            return amount;
        }
        return 0;
    }

    public int removeAll(String name) {
        Integer i = inv.get(name);
        if (i != null) {
            inv.remove(name);
            currentCapacity -= i;
            return i;
        }
        return 0;
    }

}
