package net.askarian.spaceai.server.components.rendering;

import com.artemis.Component;
import net.askarian.spaceai.server.IViewer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 9/28/15.
 */
public class RenderNearby extends Component {
    public int size;
    public int sizeSQ;
    public boolean active = true;
    public transient List<IViewer> viewerList = new ArrayList<>();


    public RenderNearby(int size) {
        this.size = size;
        this.sizeSQ = size * size;
    }

    public RenderNearby(int size, IViewer... iViewers) {
        this.size = size;
        this.sizeSQ = size * size;
        for (IViewer iv : iViewers)
            viewerList.add(iv);
    }

    public RenderNearby() {

    }
}
