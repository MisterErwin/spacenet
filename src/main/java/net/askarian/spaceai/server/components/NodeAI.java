package net.askarian.spaceai.server.components;

import com.artemis.Component;
import net.askarian.spaceai.FlowAiServer.programs.Program;

/**
 * Created by alex on 11/15/15.
 */
public class NodeAI extends Component implements IObjectInfoComponent{
    public Program program;
    public int delay, nodeAmount, maxRunningNodes;


    public NodeAI() {
    }

    public NodeAI(int delay, int nodeAmount, int maxRunningNodes) {
        this.delay = delay;
        this.nodeAmount = nodeAmount;
        this.maxRunningNodes = maxRunningNodes;
    }

    public NodeAI(int delay, int nodeAmount, int maxRunningNodes, Program program) {
        this.delay = delay;
        this.nodeAmount = nodeAmount;
        this.maxRunningNodes = maxRunningNodes;
        this.program = program;
    }

    @Override
    public String toString() {
        return "NodeAI{" +
                "delay=" + delay +
                ", nodeAmount=" + nodeAmount +
                ", maxRunningNodes=" + maxRunningNodes +
                '}';
    }

    @Override
    public void appendInfo(StringBuilder stringBuilder) {
        stringBuilder.append("Program: ").append(program==null?"none": (program.isRunning()?"running":"stopped")).append("<br/>");
    }
}
