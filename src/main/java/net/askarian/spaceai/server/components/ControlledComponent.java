package net.askarian.spaceai.server.components;

import com.artemis.Component;
import net.askarian.spaceai.server.IControlServer;

import java.util.Map;

/**
 * Created by alex on 9/28/15.
 */
public class ControlledComponent extends Component {
    public IControlServer controlServer;
    public Map<String, Integer> res, input;


}