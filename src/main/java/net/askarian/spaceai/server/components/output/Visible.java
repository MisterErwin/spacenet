package net.askarian.spaceai.server.components.output;

import com.artemis.Component;

/**
 * Created by alex on 9/28/15.
 */
public class Visible extends Component {
    public int antiVisiblePercentage=0;
    public int color = 0;
}
