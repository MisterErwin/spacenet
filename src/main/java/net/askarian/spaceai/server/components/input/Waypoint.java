package net.askarian.spaceai.server.components.input;

import com.artemis.Component;
import net.askarian.spaceai.math.Vector2d;

import java.util.Arrays;

/**
 * Created by alex on 9/28/15.
 */
public class Waypoint extends Component {
    public transient int[] angle;
    public transient int[] dist;
    public int amount;
    public Vector2d[] waypoints;

    public Waypoint() {
    }

    public Waypoint(int amount) {
        this.amount = amount;
        this.angle = new int[amount];
        this.dist = new int[amount];
        Arrays.fill(angle,0);
        Arrays.fill(dist,0);
        waypoints = new Vector2d[amount];
    }
}
