package net.askarian.spaceai.server.components.physics;

import com.artemis.Component;

/**
 * Created by alex on 9/27/15.
 */
public class Mass extends Component {
    /**
     * Holds the inverse of the mass of the particle. It
     * is more useful to hold the inverse mass because
     * integration is simpler, and because in real time
     * simulation it is more useful to have objects with
     * infinite mass (immovable) than zero mass
     * (completely unstable in numerical simulation).
     */
    public double inverseMass;

    public Mass(double mass) {
        this.inverseMass = 1 / mass;
    }

    public Mass() {
        this(1);
    }

    public static Mass newMassFromInverse(double inverseMass) {
        Mass m = new Mass();
        m.inverseMass = inverseMass;
        return m;
    }
}
