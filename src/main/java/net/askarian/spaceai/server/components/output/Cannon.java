package net.askarian.spaceai.server.components.output;

import com.artemis.Component;
import net.askarian.spaceai.math.Vector2d;
import org.apache.commons.lang.math.RandomUtils;

/**
 * Created by alex on 9/28/15.
 */
public class Cannon extends Component {
    public int dir = 0;
    public int delay = 0;
    public double strength;
    public int mindelay, projectilesPerShot=1, projectileLiveTime=20, damage;

    public Cannon() {
    }

    public Cannon(double strength, int mindelay, int projectilesPerShot, int projectileLiveTime, int damage) {
        this.strength = strength;
        this.mindelay = mindelay;
        this.projectilesPerShot = projectilesPerShot;
        this.projectileLiveTime = projectileLiveTime;
        this.damage = damage;
    }

    @Deprecated
    public Cannon(double strength, int delay) {
        this.strength = strength;
        this.mindelay = delay;
    }

    @Deprecated
    public Cannon(double strength) {
        this.strength = strength;
        this.mindelay = RandomUtils.nextInt(30);
    }

        public static int getDir(Vector2d self, Vector2d target){
            double angle = Math.toDegrees(Math.atan2(target.getX()-self.getY(), target.getX()-self.getX()));
            if (angle < 0)
                angle += 360;
            return (int) Math.round(angle);
        }
}
