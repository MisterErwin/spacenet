package net.askarian.spaceai.server.components.physics;

import com.artemis.Component;
import net.askarian.spaceai.math.Vector2d;

/**
 * Created by alex on 9/27/15.
 */
public class ForceAccumulator extends Component {
    public Vector2d res = new Vector2d();

    public ForceAccumulator add(Vector2d vector2d) {
        res.addSelf(vector2d);
        return this;
    }

}
