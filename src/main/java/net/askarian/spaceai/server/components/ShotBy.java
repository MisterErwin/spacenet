package net.askarian.spaceai.server.components;

import com.artemis.Component;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by alex on 11/1/15.
 */
public @AllArgsConstructor(access = AccessLevel.PUBLIC) @NoArgsConstructor @ToString(callSuper = false)
class ShotBy extends Component{
    public int shooter;



}
