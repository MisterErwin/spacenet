package net.askarian.spaceai.server.components;

import com.artemis.Component;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Created by alex on 9/28/15.
 */
public class Basis extends Component {
    public int spawnedInBase = 0;
    public JSONObject droneConfiguration;
    public JSONArray droneProgram;
    public String droneProgramName;

    public boolean autoBuild = false;

    public transient int schematicBuildTime;
    public transient int currentBuildTime = 100;

    public Basis() {
    }

}
