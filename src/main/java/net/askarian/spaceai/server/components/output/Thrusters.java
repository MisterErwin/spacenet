package net.askarian.spaceai.server.components.output;

import com.artemis.Component;
import net.askarian.spaceai.math.Vector2d;

/**
 * Created by alex on 9/28/15.
 */
public class Thrusters extends Component {
    public int dir = 0;
    public double maxStrength, strength = 0;

    public Thrusters() {
    }

    public Thrusters(double maxStrength) {
        this.maxStrength = maxStrength;
    }

    public Vector2d getForce() {
        if (strength <= 0)
            return null;
        double rad = Math.toRadians(dir);
        return new Vector2d(Math.cos(rad) * -1, Math.sin(rad) * -1).trimSelf(maxStrength);
    }
}
