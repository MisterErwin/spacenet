package net.askarian.spaceai.server.components;

import com.artemis.Component;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Created by alex on 10/31/15.
 */
@AllArgsConstructor
@NoArgsConstructor
public class Star extends Component {
    public int RGBColorA, RGBColorB;
    public int radius;
}
