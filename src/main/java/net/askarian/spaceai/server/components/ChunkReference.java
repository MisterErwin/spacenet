package net.askarian.spaceai.server.components;

import com.artemis.Component;

/**
 * Created by alex on 10/27/15.
 */
public class ChunkReference extends Component implements IObjectInfoComponent{
    public transient int x, y;

    public ChunkReference(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public ChunkReference() {
        x=y=Integer.MIN_VALUE;
    }

    @Override
    public void appendInfo(StringBuilder stringBuilder) {
        stringBuilder.append("Chunk: ").append(x).append("/").append(y).append("<br/>");
    }
}
