package net.askarian.spaceai.server.components;

import com.artemis.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by alex on 10/31/15.
 */
public class Pickupable extends Component{
    public Map<String,Integer> pickupAbles;

    public Pickupable() {
    }

    public Pickupable(Inventory inventory){
        pickupAbles = new HashMap<>(inventory.inv);
    }

    public int remove(String name, int amount) {
        Integer i = pickupAbles.get(name);
        if (i != null) {
            if (amount > i)
                amount = i;
            if (amount == i)
                pickupAbles.remove(name);
            else
                pickupAbles.put(name, i - amount);

            return amount;
        }
        return 0;
    }

    public Set<String> contains(){
        return pickupAbles.keySet();
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Pickupable{");
        sb.append("pickupAbles=[");
        for (Map.Entry<String,Integer> e : pickupAbles.entrySet())
            sb.append(e.getKey()).append("=>").append(e.getValue()).append(',');
        sb.append("]}");
        return sb.toString();
    }
}
