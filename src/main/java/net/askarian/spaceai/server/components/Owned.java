package net.askarian.spaceai.server.components;

import com.artemis.Component;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Created by MisterErwin on 26.12.2015.
 * In case you need it, ask me ;)
 */
@AllArgsConstructor(access = AccessLevel.PUBLIC)@NoArgsConstructor
public class Owned extends Component implements IObjectInfoComponent{
    public String owner;


    @Override
    public String toString() {
        return "Owned{" +
                "owner='" + owner + '\'' +
                '}';
    }

    @Override
    public void appendInfo(StringBuilder stringBuilder) {
        stringBuilder.append("Owner: ").append(owner).append("<br/>");
    }
}
