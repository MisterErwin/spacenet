package net.askarian.spaceai.server.components.input;

import com.artemis.Component;

/**
 * Created by alex on 9/28/15.
 */
public class Camera extends Component {
    /**
     * @link {net.askarian.spaceai.server.systems.CameraSystem}
     */
    public final int CAMERA_SIZE, MAX_DISTANCE, MAX_DISTANCE_SQ;
    public final double CAMERA_ONE_RANGE;
    public int[] data = new int[0], colorData = new int[0];

    public Camera() {
        this(36,100);
    }

    public Camera(int CAMERA_SIZE, int MAX_DISTANCE) {
        this.CAMERA_SIZE = CAMERA_SIZE;
        this.MAX_DISTANCE = MAX_DISTANCE;
        this.MAX_DISTANCE_SQ = MAX_DISTANCE * MAX_DISTANCE;
        this.CAMERA_ONE_RANGE = 2 * Math.PI / CAMERA_SIZE;
    }

    //    public int amount = 0;

}
