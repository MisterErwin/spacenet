package net.askarian.spaceai.server.components.rendering;

import com.artemis.Component;
import com.google.common.collect.Maps;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import net.askarian.spaceai.Utils;
import net.askarian.spaceai.server.IViewer;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

/**
 * Created by alex on 9/28/15.
 */
@NoArgsConstructor
public class Rendered extends Component {
    public String name;
    public int shape;
    public int size;
    public String texture;

    public Rendered(String name, int shape, int size) {
        this.name = name;
        this.shape = shape;
        this.size = size;
    }

    public Rendered(String name, String texture, int size) {
        this.name = name;
        this.texture = texture;
        this.shape = SHAPE_TEXTURE;
        this.size = size;
    }

//    transient public List<IRenderUpdate> renderUpdateList = Lists.newArrayList();


    transient public Map<Integer, Integer> visibleVia = Maps.newHashMap();
    transient public Map<IViewer, String[]> updateTypes = Maps.newHashMap(); //IViewer or null for all | UpdateType


    public static int SHAPE_CIRCLE = 0, SHAPE_RECT = 1, SHAPE_TEXTURE = 2;

    public static final String UpdateType_ALL = "ALL", UpdateType_POS = "POS", UpdateType_REMOVE = "REMOVE",
            UpdateType_COLOR = "COLOR";

    public static void addUpdateType(@NonNull Rendered rendered, IViewer viewer, String updateType) {
        String[] ex = rendered.updateTypes.get(viewer);
        if (ex == null)
            rendered.updateTypes.put(viewer, new String[]{updateType});
        else {
            if (Utils.contains(ex, updateType))
                return;
            ex = Arrays.copyOf(ex, ex.length + 1);
            ex[ex.length - 1] = updateType;
            rendered.updateTypes.put(viewer, ex);
        }
    }

    public static void addUpdateType(Rendered rendered, Collection<IViewer> viewerCollection, String updateType) {
        for (IViewer iViewer : viewerCollection)
            addUpdateType(rendered, iViewer, updateType);
    }


}
