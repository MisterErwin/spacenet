package net.askarian.spaceai.server.components.physics;

import com.artemis.Component;
import net.askarian.spaceai.math.Vector2d;

/**
 * Created by alex on 9/27/15.
 */
public class Position extends Component {
    public Vector2d pos;

    public Position() {
        pos = new Vector2d();
    }

    public Position(Vector2d pos) {
        this.pos = pos;
    }
}
