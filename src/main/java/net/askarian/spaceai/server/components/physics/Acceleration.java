package net.askarian.spaceai.server.components.physics;

import com.artemis.Component;
import net.askarian.spaceai.math.Vector2d;

/**
 * Created by alex on 9/27/15.
 */
public class Acceleration extends Component {
    public Vector2d acc;

    public Acceleration(Vector2d acc) {
        this.acc = acc;
    }

    public Acceleration() {
        acc = new Vector2d();
    }
}
