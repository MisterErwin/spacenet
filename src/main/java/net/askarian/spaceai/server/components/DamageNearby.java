package net.askarian.spaceai.server.components;

import com.artemis.Component;

/**
 * Created by alex on 9/28/15.
 */
public class DamageNearby extends Component {
    public int damageDealt;
    public int radius;
    public String damageReason;
    public int[] ignoring;
    public boolean removeAfter = false;

    public DamageNearby(int damageDealt, int radius, String damageReason) {
        this.damageDealt = damageDealt;
        this.radius = radius;
        this.damageReason = damageReason;
    }

    public DamageNearby(int damageDealt, int radius, String damageReason, int... ignoring) {
        this.damageDealt = damageDealt;
        this.radius = radius;
        this.damageReason = damageReason;
        this.ignoring = ignoring;
    }

    public DamageNearby(int damageDealt, int radius, String damageReason, boolean removeAfter, int... ignoring) {
        this.damageDealt = damageDealt;
        this.radius = radius;
        this.damageReason = damageReason;
        this.removeAfter = removeAfter;
        this.ignoring = ignoring;
    }


    public DamageNearby(int damageDealt, int radius) {
        this.damageDealt = damageDealt;
        this.radius = radius;
    }

    public DamageNearby() {
    }


}
