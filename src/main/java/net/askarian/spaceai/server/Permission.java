package net.askarian.spaceai.server;

/**
 * Created by MisterErwin on 30.12.2015.
 * In case you need it, ask me ;)
 */
public enum Permission {
    CHANGE_BUILD_PLAN_SCHEMATIC,
    CHANGE_BUILD_PLAN_PROGRAM,
    CHANGE_DRONE_PROGRAM,
    CHANGE_ALLIES
}
