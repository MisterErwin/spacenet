package net.askarian.spaceai.server.util;

import com.artemis.ComponentMapper;
import net.askarian.spaceai.math.Vector2d;
import net.askarian.spaceai.server.systems.ChunkSystem;

/**
 * Created by MisterErwin on 26.12.2015.
 * In case you need it, ask me ;)
 */
public class SpawnUtils {

    public static Vector2d getFreePosition(ChunkSystem chunkSystem, int freeRange, int range, int tries, Vector2d origin, ComponentMapper... notWanted){
        Vector2d vector2d = null;
        while (tries-- > 0 && vector2d == null)
            vector2d = getFreePosition(chunkSystem, freeRange, range, origin, notWanted);
        return vector2d;
    }


        public static Vector2d getFreePosition(ChunkSystem chunkSystem, int freeRange, int range, Vector2d origin, ComponentMapper... notWanted){
        Vector2d vector2d = Vector2d.random(range);
        if (origin != null)
            vector2d.addScaledSelf(origin,1);

        return chunkSystem.getNearbyEntities(vector2d, freeRange, notWanted).isEmpty()?vector2d:null;
    }
}
