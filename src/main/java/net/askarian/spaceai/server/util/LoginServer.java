package net.askarian.spaceai.server.util;

import net.askarian.spaceai.server.Region;
import net.askarian.spaceai.server.util.config.Configuration;
import net.askarian.spaceai.server.util.config.ConfigurationProvider;
import net.askarian.spaceai.server.util.config.YamlConfiguration;
import org.java_websocket.WebSocket;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alex on 10/27/15.
 */
public class LoginServer {

    final Map<InetAddress, LoginData> tokens = new HashMap<InetAddress, LoginData>();

    static LoginServer instance;
    final File playerDir;
    final Region region;
    public LoginServer(Region region) {
        instance = this;
        this.region = region;
        playerDir = new File("players");
        if (!playerDir.isDirectory())
            playerDir.mkdirs();
    }

    public void onOpen(WebSocket webSocket) {
        tokens.put(webSocket.getRemoteSocketAddress().getAddress(), new LoginData());
//        System.out.println("Login>Hanshake: " + webSocket.getRemoteSocketAddress().getAddress().toString());
    }

    public boolean onMessage(WebSocket webSocket, Map<String, Object> map, String s) {
        LoginData loginData = tokens.get(webSocket.getRemoteSocketAddress().getAddress());
        if (loginData == null || loginData.ok) return false;

        if (map.containsKey("type"))
            switch (map.get("type").toString().toLowerCase()) {
                case "login":
                    loginData.username = (String) map.get("username");
                    if (!loadUserData(loginData)) {
                        JSONObject ret = new JSONObject();
                        ret.put("type", "login.error");
                        ret.put("msg", "notfound.username");
                        webSocket.send(ret.toJSONString());
                        return true;
                    }
                    if (!checkPWD(loginData.pwd, (String) map.get("pwd"))) {
                        JSONObject ret = new JSONObject();
                        ret.put("type", "login.error");
                        ret.put("msg", "wrong.pwd");
                        webSocket.send(ret.toJSONString());
                        return true;
                    }
//                    System.out.println("Login>Success: " + webSocket.getRemoteSocketAddress().getAddress().toString());

                    loginData.ok=true;
                    JSONObject ret = new JSONObject();
                    ret.put("type", "login.success");
                    ret.put("msg", "login.success");
                    webSocket.send(ret.toJSONString());
                    break;
                case "create":
                    loginData.username = (String) map.get("username");
                    if (existsUserData(loginData)){
                        ret = new JSONObject();
                        ret.put("type", "create.error");
                        ret.put("msg", "already.username");
                        webSocket.send(ret.toJSONString());
                        return true;
                    }
                    loginData.pwd = hash((String) map.get("pwd"));
                    createUser(loginData);
                    region.addBase(loginData.username); //Try it at least once. If this fails, we can retry later
                    ret = new JSONObject();
                    ret.put("type", "create.success");
                    ret.put("msg", "create.success");
                    webSocket.send(ret.toJSONString());
                    break;
                default:
                    System.out.println("Unknown login key");
                    break;
            }
        return true;
    }

    private static boolean checkPWD(byte[] hashed, String orig) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA");
            return Arrays.equals(md.digest(orig.getBytes()), hashed);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static byte[] hash(String orig) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA");
            return md.digest(orig.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean existsUserData(LoginData loginData) {
        File f = new File(playerDir, loginData.username + ".yml");
        return f.exists();
    }

    private boolean createUser(LoginData loginData) {
        File f = new File(playerDir, loginData.username + ".yml");
        if (!f.exists())
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        try {
            Configuration yamlConfiguration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(f);
            yamlConfiguration.set("pwd", loginData.pwd);
            yamlConfiguration.set("user", loginData.username);
            yamlConfiguration.set("lastlogin", System.currentTimeMillis());
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(yamlConfiguration, f);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }


    private boolean loadUserData(LoginData loginData) {
        File f = new File(playerDir, loginData.username + ".yml");
        if (!f.exists())
            return false;
        try {
            Configuration yamlConfiguration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(f);
            loginData.pwd = yamlConfiguration.get("pwd", new byte[0]);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isLoggedIn(WebSocket webSocket) {
        //DEBUG:
//        return true;
        LoginData loginData = tokens.get(webSocket.getRemoteSocketAddress().getAddress());
        return loginData!=null && loginData.ok;
    }

    public String getPlayerName(WebSocket webSocket) {
//        return "alex";
        LoginData loginData = tokens.get(webSocket.getRemoteSocketAddress().getAddress());
        return loginData!=null? loginData.username : null;
    }

    private class LoginData {
        String username;
        byte[] pwd;
        boolean ok=false;
    }

}