package net.askarian.spaceai.server;

import com.artemis.Entity;
import com.artemis.managers.UuidEntityManager;
import net.askarian.spaceai.server.components.rendering.RenderNearby;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by alex on 10/27/15.
 */
public interface IViewer {
    void sendMessage(String message);

    default UUID getExplicitWatchingUUID() {
        Entity e = getExplicitWatching();
        return e == null ? null : e.getWorld().getSystem(UuidEntityManager.class).getUuid(e);
    }

    Entity getExplicitWatching();

    void setExplicitWatching(Entity explicitWatching);

    Collection<Integer> getViewing();

    void view(Integer viewing, RenderNearby renderNearbyComponent);

    void unView(Integer viewing, RenderNearby renderNearbyComponent);

    default void unViewAll() {
        getViewing().forEach(i->unView(i,null));
    }

}
