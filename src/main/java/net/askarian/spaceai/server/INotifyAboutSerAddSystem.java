package net.askarian.spaceai.server;

import com.artemis.Entity;

/**
 * Created by MisterErwin on 26.12.2015.
 * In case you need it, ask me ;)
 */
public interface INotifyAboutSerAddSystem {
    default void reAddAfterLoad(Entity e, Region region){
        reAddAfterLoad(e);
    }
    default void reAddAfterLoad(Entity e){};
}
