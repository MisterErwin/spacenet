package net.askarian.spaceai.server.rendering.HTMRenderer;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.PlayerManager;
import com.artemis.managers.UuidEntityManager;
import com.artemis.systems.IteratingSystem;
import com.google.common.collect.Maps;
import com.google.common.eventbus.Subscribe;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import net.askarian.spaceai.math.Vector2d;
import net.askarian.spaceai.server.IViewer;
import net.askarian.spaceai.server.components.Star;
import net.askarian.spaceai.server.components.input.Camera;
import net.askarian.spaceai.server.components.output.Color;
import net.askarian.spaceai.server.components.physics.Mass;
import net.askarian.spaceai.server.components.physics.Position;
import net.askarian.spaceai.server.components.rendering.RenderNearby;
import net.askarian.spaceai.server.components.rendering.Rendered;
import net.askarian.spaceai.server.components.rendering.Watchable;
import net.askarian.spaceai.server.systems.ChunkSystem;
import net.askarian.spaceai.server.systems.PhysicsIntegrationSystem;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by alex on 9/28/15.
 */
@Wire
public class HTMLRendererSystem extends IteratingSystem {
    ChunkSystem chunkSystem;
    UuidEntityManager uuidEntityManager;
    PlayerManager playerManager;
    ComponentMapper<Rendered> renderedComponentMapper;
    ComponentMapper<RenderNearby> renderNearbyComponentMapper;
    ComponentMapper<Position> positionComponentMapper;
    ComponentMapper<Watchable> watchableComponentMapper;
    ComponentMapper<Camera> cameraComponentMapper;
    ComponentMapper<Color> colorComponentMapper;
    ComponentMapper<Mass> massComponentMapper;
    ComponentMapper<Star> starComponentMapper;
    Map<Integer, UUID> uuidBag = new HashMap<>();


    final Gson gson;

    public HTMLRendererSystem() {
        super(Aspect.one(Rendered.class));
        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder.registerTypeAdapter(Vector2d.class, new TypeAdapter<Vector2d>() {
            @Override
            public void write(JsonWriter jsonWriter, Vector2d vector2d) throws IOException {
                if (vector2d == null) {
                    jsonWriter.nullValue();
                    return;
                }
                String xy = vector2d.getX() + "," + vector2d.getY();
                jsonWriter.value(xy);
            }

            @Override
            public Vector2d read(JsonReader jsonReader) throws IOException {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.nextNull();
                    return null;
                }
                String xy = jsonReader.nextString();
                String[] parts = xy.split(",");
                int x = Integer.parseInt(parts[0]);
                int y = Integer.parseInt(parts[1]);
                return new Vector2d(x, y);
            }
        });

        this.gson = gsonBuilder.create();


    }


    @Override
    protected void process(int entity) {
        Rendered rendered = renderedComponentMapper.get(entity);
        try {
            for (Map.Entry<IViewer, String[]> entry : rendered.updateTypes.entrySet()) {
                String[] datas = new String[entry.getValue().length];
                for (int i = 0; i < entry.getValue().length; ++i)
                    datas[i] = getData(entry.getValue()[i], entity);
                if (entry.getKey() == null) {
                    for (Integer visibleTo : rendered.visibleVia.keySet()) {
                        RenderNearby renderNearby = renderNearbyComponentMapper.getSafe(visibleTo);
                        if (renderNearby != null) {
                            for (IViewer viewer : renderNearby.viewerList)
                                for (String data : datas)
                                    viewer.sendMessage(data);


                        }
                    }
                } else //Send data to viewer
                    for (String data : datas)
                        entry.getKey().sendMessage(data);
            }
            rendered.updateTypes.clear();

            Watchable watchable = watchableComponentMapper.getSafe(entity);
            if (watchable != null && !watchable.updateTypes.isEmpty()) {
                if (!watchable.watchers.isEmpty()) {
                    for (String type : watchable.updateTypes) {
                        String data = getData(type, entity);
                        for (IViewer iViewer : watchable.watchers)
                            iViewer.sendMessage(data);
                    }
                }
                watchable.updateTypes.clear();
            }
        }catch (Exception e){
            e.printStackTrace();
            Rendered.addUpdateType(rendered,(IViewer)null,Rendered.UpdateType_ALL);
        }
    }

    @Override
    protected void inserted(int entityId) {
        super.inserted(entityId);
        this.uuidBag.put(entityId, world.getSystem(UuidEntityManager.class).getUuid(getWorld().getEntity(entityId)));
    }

    @Override
    protected void removed(int entityId) {
        Rendered rendered = renderedComponentMapper.getSafe(entityId);
        if (rendered != null) {
            String data = getData(Rendered.UpdateType_REMOVE, entityId);
            for (Integer visibleTo : rendered.visibleVia.keySet()) {
                RenderNearby renderNearby = renderNearbyComponentMapper.getSafe(visibleTo);
                if (renderNearby != null) {
                    renderNearby.viewerList.forEach(viewer1 -> viewer1.sendMessage(data));
                }
            }
        }
        this.uuidBag.remove(entityId);
    }

    private String getData(String updateType, int updated) {
        Type type = new TypeToken<Map<String, Object>>() { /*Empty intended*/
        }.getType();
        Map<String, Object> map = Maps.newHashMap();
        switch (updateType) {
            case Rendered.UpdateType_POS:
                map.put("type", "pos");
                Entity entity = world.getEntity(updated);
                map.put("uuid", uuidEntityManager.getUuid(entity));
                map.put("pos", positionComponentMapper.get(updated).pos);
                Mass mass = massComponentMapper.getSafe(updated);
                map.put("mass", mass != null ? mass.inverseMass : 100);
                map.put("owner", playerManager.getPlayer(entity));

                if (starComponentMapper.has(updated)) {
                    Star star = starComponentMapper.get(updated);
                    map.put("starrad", star.radius);
                    map.put("starcolA", star.RGBColorA);
                    map.put("starcolB", star.RGBColorB);
                }

                return gson.toJson(map, type);
            case Rendered.UpdateType_COLOR:
                map.put("type", "color");
                map.put("uuid", uuidEntityManager.getUuid(world.getEntity(updated)));
                map.put("color", colorComponentMapper.get(updated).color);

                if (starComponentMapper.has(updated)) {
                    Star star = starComponentMapper.get(updated);
                    map.put("starrad", star.radius);
                    map.put("starcolA", star.RGBColorA);
                    map.put("starcolB", star.RGBColorB);
                }

                return gson.toJson(map, type);

            case Rendered.UpdateType_ALL:
                map.put("type", "all");
                entity = world.getEntity(updated);
                map.put("uuid", uuidEntityManager.getUuid(entity));
                map.put("pos", positionComponentMapper.get(updated).pos);
                Rendered rendered = renderedComponentMapper.get(updated);
                map.put("render", rendered.name);
                map.put("rendershape", rendered.shape);
                if (rendered.shape == Rendered.SHAPE_TEXTURE)
                    map.put("rendertexture", rendered.texture);
                map.put("rendersize", rendered.size);

                mass = massComponentMapper.getSafe(updated);
                map.put("mass", mass != null ?
                        mass.inverseMass :
                        100);
                map.put("owner", playerManager.getPlayer(entity));


                Color color = colorComponentMapper.getSafe(updated);
                if (color != null)
                    map.put("color", color.color);

                if (starComponentMapper.has(updated)) {
                    Star star = starComponentMapper.get(updated);
                    map.put("starrad", star.radius);
                    map.put("starcolA", star.RGBColorA);
                    map.put("starcolB", star.RGBColorB);
                }

                return gson.toJson(map, type);
            case Rendered.UpdateType_REMOVE:
                map.put("type", "remove");
                map.put("uuid", uuidBag.get(updated)); //uuidEntityManager.getUuid(world.getEntity(updated)));
                return gson.toJson(map, type);
            case Watchable.WatchUT_CAMERA:
                map.put("type", "watch_cam");
                map.put("uuid", uuidEntityManager.getUuid(world.getEntity(updated)));
                Camera camera = cameraComponentMapper.get(updated);
                map.put("cdata", camera.data);
                map.put("ccolorData", camera.colorData);
                return gson.toJson(map, type);
            default:
                map.put("type", updated);
                map.put("error", "unknown");
                return gson.toJson(map, type);
        }
    }

    private void sendData(String updateType, int updated, IViewer viewer) {
        viewer.sendMessage(getData(updateType, updated));
    }

    public void applyUpdate(Entity entity, String type) {
        applyUpdate(entity.getId(), type);
    }

    public void applyUpdate(int enityID, String type) {
        Rendered rendered = renderedComponentMapper.getSafe(enityID);
        if (rendered == null) {
            System.out.println("Discarding data for " + enityID);
            return;
        }
        Rendered.addUpdateType(rendered, (IViewer) null, type);
    }

    @Subscribe
    public void applyUpdate(PhysicsIntegrationSystem.UpdatePosEvent updatePosEvent) {
        applyUpdate(updatePosEvent.getEntity(), Rendered.UpdateType_POS);
    }



}
