package net.askarian.spaceai.server.systems.onload;

import com.artemis.Aspect;
import com.artemis.BaseSystem;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.managers.PlayerManager;
import com.artemis.utils.IntBag;
import net.askarian.spaceai.server.INotifyAboutSerAddSystem;
import net.askarian.spaceai.server.Region;
import net.askarian.spaceai.server.components.Owned;

/**
 * Created by MisterErwin on 26.12.2015.
 * In case you need it, ask me ;)
 */
public class SetOwnerSystem extends BaseSystem implements INotifyAboutSerAddSystem, Region.INotifyOnSave {
    @Override
    protected void processSystem() {
    }

    @Override
    protected boolean checkProcessing() {
        return false;
    }


    @Override
    public void reAddAfterLoad(Entity e) {
        if (e.getComponent(Owned.class) != null)
            e.getWorld().getSystem(PlayerManager.class).setPlayer(e, e.getComponent(Owned.class).owner);
    }

    @Override
    public void onBeforeSave() {
        IntBag bag = getWorld().getAspectSubscriptionManager().get(Aspect.all()).getEntities();
        PlayerManager playerManager = getWorld().getSystem(PlayerManager.class);
        ComponentMapper<Owned> ownedComponentMapper = getWorld().getMapper(Owned.class);
        for (int i = 0; i < bag.size(); ++i) {
            Entity e = world.getEntity(bag.get(i));
            String p = playerManager.getPlayer(e);
            if (p != null) {
                if (!ownedComponentMapper.has(e))
                    ownedComponentMapper.create(e).owner = p;
                else
                    ownedComponentMapper.get(e).owner = p;
            }
        }
    }
}
