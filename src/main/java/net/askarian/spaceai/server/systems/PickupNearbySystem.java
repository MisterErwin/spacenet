package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import net.askarian.spaceai.server.components.Inventory;
import net.askarian.spaceai.server.components.Pickupable;
import net.askarian.spaceai.server.components.physics.Mass;
import net.askarian.spaceai.server.components.physics.Position;

/**
 * Created by alex on 9/28/15.
 */
public class PickupNearbySystem extends IteratingSystem {
    public PickupNearbySystem() {
        super(Aspect.all(Position.class, Inventory.class));
    }

    ChunkSystem chunkSystem;
    ComponentMapper<Pickupable> pickupableComponentMapper;
    ComponentMapper<Inventory> inventoryComponentMapper;
    ComponentMapper<Position> positionComponentMapper;
    ComponentMapper<Mass> massComponentMapper;
    MassSystem massSystem;


    @Override
    protected void process(int entity) {
        Inventory inventory = inventoryComponentMapper.get(entity);
        if (!inventory.activePickup || inventory.currentCapacity >= inventory.maxCapacity) return;
        Position pos = positionComponentMapper.get(entity);
        chunkSystem.getNearbyEntities(pos.pos, inventory.pickupRange, pickupableComponentMapper)
                .forEach(e -> {
                    Pickupable pickupable = pickupableComponentMapper.get(e);
                    int free = inventory.maxCapacity - inventory.currentCapacity;
                    for (String type : pickupable.contains()){
                        int n = pickupable.remove(type, free);
                        if (n > 0){
                            inventory.add(type, n);
                            free = inventory.maxCapacity - inventory.currentCapacity;
                            massSystem.addMass(massComponentMapper.get(entity), type, n);
                            massSystem.remMass(massComponentMapper.get(e), type, n);
                            if (free <= 0)
                                break;
                        }
                    }

                    if (pickupable.pickupAbles.isEmpty()){
                        world.getEntity(e).deleteFromWorld();
                        return;
                    }

                });
    }
}
