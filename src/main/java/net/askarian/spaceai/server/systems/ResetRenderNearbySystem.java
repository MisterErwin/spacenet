package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.IntervalIteratingSystem;
import net.askarian.spaceai.server.components.rendering.RenderNearby;
import net.askarian.spaceai.server.components.rendering.Rendered;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by alex on 9/28/15.
 */
public class ResetRenderNearbySystem extends IntervalIteratingSystem {
    public ResetRenderNearbySystem() {
        super(Aspect.all(Rendered.class), 10);
    }

    ChunkSystem chunkSystem;
    ComponentMapper<Rendered> renderedComponentMapper;

    @Override
    protected void process(int entity) {
        Iterator<Map.Entry<Integer, Integer>> iterator = renderedComponentMapper.get(entity).visibleVia.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, Integer> e = iterator.next();

            e.setValue(e.getValue() - 1);
            if (e.getValue() < 0) {
                Entity ee = world.getEntity(e.getKey());
                if (ee != null) {
                    RenderNearby rn = ee.getComponent(RenderNearby.class);
                    if (rn != null)
                        Rendered.addUpdateType(renderedComponentMapper.get(entity), rn.viewerList, Rendered.UpdateType_REMOVE);
                }

                System.out.println("Out of sight!" + e.getKey());
                iterator.remove();
            }
        }
    }
}
