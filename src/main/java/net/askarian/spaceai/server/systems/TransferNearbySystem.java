package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import net.askarian.spaceai.server.components.Inventory;
import net.askarian.spaceai.server.components.MoveInventoryOver;
import net.askarian.spaceai.server.components.physics.Mass;
import net.askarian.spaceai.server.components.physics.Position;

/**
 * Created by alex on 9/28/15.
 */
public class TransferNearbySystem extends IteratingSystem {
    public TransferNearbySystem() {
        super(Aspect.all(Position.class, Inventory.class, MoveInventoryOver.class));
    }

    ChunkSystem chunkSystem;
    ComponentMapper<Inventory> inventoryComponentMapper;
    ComponentMapper<Position> positionComponentMapper;
    ComponentMapper<Mass> massComponentMapper;
    ComponentMapper<MoveInventoryOver> moveInventoryOverComponentMapper;
    MassSystem massSystem;

    @Override
    protected void process(int entity) {
        MoveInventoryOver moveInventoryOver = moveInventoryOverComponentMapper.get(entity);
        if (!moveInventoryOver.active) return;
        Inventory baseInventory = inventoryComponentMapper.get(entity);
        if (baseInventory.currentCapacity >= baseInventory.maxCapacity) return;
        Position pos = positionComponentMapper.get(entity);
        chunkSystem.getNearbyEntities(pos.pos, moveInventoryOver.pickupRange, inventoryComponentMapper)
                .forEach(e -> {
                    Inventory otherInv = inventoryComponentMapper.get(e);
                    int baseSpace = baseInventory.maxCapacity - baseInventory.currentCapacity;

                    for (String type : otherInv.inv.keySet()){
                        int n = otherInv.remove(type, baseSpace);
                        if (n > 0){
                            baseInventory.add(type, n);
                            baseSpace = baseInventory.maxCapacity - baseInventory.currentCapacity;
                            massSystem.addMass(massComponentMapper.get(entity), type, n);
                            massSystem.remMass(massComponentMapper.get(e), type, n);

                            if (baseSpace <= 0)
                                return;
                        }
                    }

                });
    }
}
