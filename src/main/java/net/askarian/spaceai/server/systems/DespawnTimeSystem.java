package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.artemis.systems.IntervalIteratingSystem;
import net.askarian.spaceai.server.components.DespawnTime;

/**
 * Created by alex on 9/28/15.
 */
@Wire
public class DespawnTimeSystem extends IntervalIteratingSystem {
    ComponentMapper<DespawnTime> despawnTimeComponentMapper;

    public DespawnTimeSystem() {
        super(Aspect.all(DespawnTime.class), 1);
    }

    @Override
    protected void process(int entity) {
        DespawnTime d = despawnTimeComponentMapper.get(entity);
        if (d.time-- <= 0)
            world.getEntity(entity).deleteFromWorld();
    }

}
