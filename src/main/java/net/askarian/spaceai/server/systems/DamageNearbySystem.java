package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import net.askarian.spaceai.Utils;
import net.askarian.spaceai.server.components.DamageNearby;
import net.askarian.spaceai.server.components.Health;
import net.askarian.spaceai.server.components.ShotBy;
import net.askarian.spaceai.server.components.physics.Position;

/**
 * Created by alex on 9/28/15.
 */
public class DamageNearbySystem extends IteratingSystem {
    public DamageNearbySystem() {
        super(Aspect.all(DamageNearby.class, Position.class));
    }

    ChunkSystem chunkSystem;
    ComponentMapper<DamageNearby> damageNearbyComponentMapper;
    ComponentMapper<Health> healthComponentMapper;
    ComponentMapper<Position> positionComponentMapper;
    ComponentMapper<ShotBy> shotByComponentMapper;
    boolean done;

    @Override
    protected void process(int entity) {
        DamageNearby damageNearby = damageNearbyComponentMapper.get(entity);
        Position pos = positionComponentMapper.get(entity);
        done = false;
        chunkSystem.getNearbyEntities(pos.pos, damageNearby.radius, healthComponentMapper)
                .stream().filter(e -> damageNearby.ignoring == null || !Utils.containsPrimitive(damageNearby.ignoring, e))
                .forEach(e -> {
                    done = true;
                    healthComponentMapper.get(e)
                            .damage(shotByComponentMapper.has(e)?shotByComponentMapper.get(e).shooter:e, damageNearby.damageDealt, damageNearby.damageReason == null ? "damagenearby" : damageNearby.damageReason, entity);
                });
        if (damageNearby.removeAfter && done)
            world.delete(entity);
    }
}
