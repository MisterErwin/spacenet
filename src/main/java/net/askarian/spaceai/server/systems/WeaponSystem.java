package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.IntervalIteratingSystem;
import com.artemis.utils.EntityBuilder;
import net.askarian.spaceai.math.Vector2d;
import net.askarian.spaceai.server.IViewer;
import net.askarian.spaceai.server.components.ChunkReference;
import net.askarian.spaceai.server.components.DamageNearby;
import net.askarian.spaceai.server.components.DespawnTime;
import net.askarian.spaceai.server.components.ShotBy;
import net.askarian.spaceai.server.components.output.Cannon;
import net.askarian.spaceai.server.components.physics.*;
import net.askarian.spaceai.server.components.rendering.Rendered;

/**
 * Created by alex on 9/28/15.
 */
@Wire
public class WeaponSystem extends IntervalIteratingSystem {
    ComponentMapper<Position> positionComponentMapper;
    ComponentMapper<Cannon> cannonComponentMapper;

    public WeaponSystem() {
        super(Aspect.all(Cannon.class, Position.class), 1);
    }

    @Override
    protected void process(int entity) {
        Cannon cannon = cannonComponentMapper.getSafe(entity);
        if (cannon == null) return;
        cannon.delay++;
        if (cannon.delay > 0)
            cannon.delay = 0;
        if (cannon.dir == 0 || cannon.delay < 0) return;

        Vector2d v = new Vector2d(Math.cos(Math.toRadians(cannon.dir)), Math.sin(Math.toRadians(cannon.dir)));

        EntityBuilder entityBuilder = new EntityBuilder(getWorld());
        entityBuilder.with(ChunkReference.class, Velocity.class, Acceleration.class, ForceAccumulator.class, Mass.class);

        entityBuilder.with(new Rendered("bullet", "bulletR", 2), new Position(positionComponentMapper.get(entity).pos.clone()), new DespawnTime(cannon.projectileLiveTime));
        entityBuilder.with(new ShotBy(entity));
//        entityBuilder.with(SpinItAround.class);
        entityBuilder.with(new DamageNearby(100, 8, "shot", true, entity));

        Entity bullet = entityBuilder.build();
        bullet.getComponent(ForceAccumulator.class).add(v.multSelf(cannon.strength));

        Rendered rendered = bullet.getComponent(Rendered.class);
        if (rendered != null)
            Rendered.addUpdateType(rendered, (IViewer) null, Rendered.UpdateType_ALL);


        cannon.dir = 0;
        cannon.delay = cannon.mindelay * -1; //-10;
    }

}
