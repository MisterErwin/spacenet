package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IntervalIteratingSystem;
import net.askarian.spaceai.server.rendering.HTMRenderer.HTMLRendererSystem;
import net.askarian.spaceai.server.components.physics.Position;
import net.askarian.spaceai.server.components.rendering.RenderNearby;
import net.askarian.spaceai.server.components.rendering.Rendered;

import java.util.List;

/**
 * Created by alex on 9/28/15.
 */
public class RenderNearbySystem extends IntervalIteratingSystem {
    public RenderNearbySystem() {
        super(Aspect.all(RenderNearby.class), 10);
    }

    HTMLRendererSystem htmlRendererSystem;
    ChunkSystem chunkSystem;
    ComponentMapper<Rendered> renderedComponentMapper;
    ComponentMapper<RenderNearby> renderNearbyComponentMapper;
    ComponentMapper<Position> positionComponentMapper;

    @Override
    protected void process(int entity) {
        RenderNearby renderNearby = renderNearbyComponentMapper.get(entity);
        if (!renderNearby.active) return; //Save CPU time
        Position pos = positionComponentMapper.get(entity);
        List<Integer> nearby = chunkSystem.getNearbyEntities(pos.pos, renderNearby.size, renderedComponentMapper);
        Rendered rendered;

        for (Integer i : nearby) {

            rendered = renderedComponentMapper.get(i);
            Object o = rendered.visibleVia.put(entity,5);
            if (o==null) { //If updated
                Rendered.addUpdateType(rendered, renderNearby.viewerList, Rendered.UpdateType_ALL);
//                System.out.println(i + " can be seen by " + entity);
            }
        }
    }
}
