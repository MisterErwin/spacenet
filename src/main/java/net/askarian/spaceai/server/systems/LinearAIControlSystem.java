package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.artemis.systems.IntervalIteratingSystem;
import net.askarian.spaceai.server.components.LinearAI;

/**
 * Created by alex on 9/28/15.
 */
@Wire
public class LinearAIControlSystem extends IntervalIteratingSystem {
    ComponentMapper<LinearAI> linearAIComponentMapper;
    public LinearAIControlSystem() {
        super(Aspect.all(LinearAI.class), 5);
    }

    @Override
    protected void process(int i) {
        LinearAI ai = linearAIComponentMapper.get(i);
        ai.tree.tick(ai.memory);
    }
}
