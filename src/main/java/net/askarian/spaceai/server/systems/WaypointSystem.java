package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.IntervalIteratingSystem;
import net.askarian.spaceai.server.INotifyAboutSerAddSystem;
import net.askarian.spaceai.server.components.input.Waypoint;
import net.askarian.spaceai.server.components.physics.Position;

import java.util.Objects;

/**
 * Created by alex on 9/28/15.
 */
@Wire
public class WaypointSystem extends IntervalIteratingSystem implements INotifyAboutSerAddSystem {
    ComponentMapper<Waypoint> waypointComponentMapper;
    ComponentMapper<Position> positionComponentMapper;

    ChunkSystem chunkSystem;

    public WaypointSystem() {
        super(Aspect.all(Waypoint.class, Position.class), 10);
    }


    @Override
    protected void process(int entity) {
        Waypoint waypoint = waypointComponentMapper.get(entity);
        Position position = positionComponentMapper.get(entity);
        for (int i=0;i<waypoint.amount;++i) {
            if (waypoint.waypoints[i] == null) { //Set a wp
                waypoint.waypoints[i] = position.pos.clone();
                return;
            }

            double alphaImBogenmass = Math.atan2(waypoint.waypoints[i].getY() - position.pos.getY(), waypoint.waypoints[i].getX() - position.pos.getX());
            int a = (int) Math.toDegrees(alphaImBogenmass);

            while (a < 0)
                a += 360;
            while (a > 360)
                a -= 360;

            waypoint.angle[i] = a;
            waypoint.dist[i] = (int) waypoint.waypoints[i].distance(position.pos);
        }
    }

    @Override
    public void reAddAfterLoad(Entity e) {
        Waypoint waypoint = e.getComponent(Waypoint.class);
        if (waypoint == null)return;
        System.out.println(Objects.toString(waypoint) + "||" + Objects.toString(e.getWorld().getMapper(Waypoint.class).get(e)));
        waypoint.angle = new int[waypoint.amount];
        waypoint.dist = new int[waypoint.amount];
    }
}
