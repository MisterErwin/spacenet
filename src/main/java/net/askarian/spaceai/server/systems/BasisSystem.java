package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.IntervalEntityProcessingSystem;
import net.askarian.spaceai.FlowAiServer.programs.Program;
import net.askarian.spaceai.math.Vector2d;
import net.askarian.spaceai.server.INotifyAboutSerAddSystem;
import net.askarian.spaceai.server.Region;
import net.askarian.spaceai.server.builder.PartRegistry;
import net.askarian.spaceai.server.components.Basis;
import net.askarian.spaceai.server.components.Owned;
import net.askarian.spaceai.server.components.physics.Position;

import java.util.Objects;

/**
 * Created by alex on 9/28/15.
 */
@Wire
public class BasisSystem extends IntervalEntityProcessingSystem implements INotifyAboutSerAddSystem {
    ComponentMapper<Basis> basisComponentMapper;
    ComponentMapper<Owned> ownedComponentMapper;
    ComponentMapper<Position> positionComponentMapper;

    public BasisSystem() {
        super(Aspect.all(Basis.class), 1);
    }

    @Override
    protected void process(Entity entity) {
        Basis b = basisComponentMapper.get(entity);

        if (b.droneProgram == null)
            b.currentBuildTime = -2;

        if (b.currentBuildTime == -2) {
            if (!b.autoBuild) return;
            b.currentBuildTime = b.schematicBuildTime;
        }
        if (--b.currentBuildTime <= 0) {
            b.currentBuildTime = b.autoBuild ? b.schematicBuildTime : -2;
            Region region = world.getSystem(Region.RegionReference.class).getRegion();

            Program program = region.server.getProgramServer().buildAndStartProgram(b.droneProgram);
            if (program != null)
                if (region.addAIDrone(program, ownedComponentMapper.get(entity).owner, Vector2d.random(20).addSelf(positionComponentMapper.get(entity).pos), b) > 0)
                    return;

            System.out.println("Failed to spawn drone by base " + Objects.toString(program) + " for " + Objects.toString(ownedComponentMapper.get(entity).owner));
            if (program != null)
                program.setAsEnded();
            b.autoBuild = false;
            b.currentBuildTime = -2;
        }

    }

    @Override
    public void reAddAfterLoad(Entity e, Region region) {
        Basis basis = e.getComponent(Basis.class);
        if (basis != null)
            update(basis, region.partRegistry);
    }

    public void update(Basis basis) {
        update(basis, getWorld().getSystem(Region.RegionReference.class)
                .getRegion()
                .partRegistry);
    }

    public void update(Basis basis, PartRegistry partRegistry) {
        if (basis.droneConfiguration != null) {
            PartRegistry.SchemataResult res = partRegistry
                    .evaluateSchemataAndGetCost(PartRegistry.makePartList(
                            basis.droneConfiguration));
            if (res != null)
                basis.schematicBuildTime = res.getTime();
            else
                basis.schematicBuildTime = -2;
        } else
            basis.schematicBuildTime = -2;
        if (basis.schematicBuildTime < 0 || basis.droneProgram == null || basis.droneProgramName == null)
            basis.autoBuild = false;
        basis.currentBuildTime = basis.autoBuild ? basis.schematicBuildTime : -2;

        System.out.println("Time:" + basis.schematicBuildTime);
    }
}
