package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.artemis.managers.PlayerManager;
import com.artemis.systems.IntervalIteratingSystem;
import net.askarian.spaceai.server.Region;
import net.askarian.spaceai.server.components.Health;
import net.askarian.spaceai.server.components.ShotBy;
import net.askarian.spaceai.server.components.SpawnWreck;

import java.util.Objects;

/**
 * Created by alex on 9/28/15.
 */
@Wire
public class HealthSystem extends IntervalIteratingSystem {
    ComponentMapper<Health> healthComponentMapper;
    ComponentMapper<ShotBy> shotByComponentMapper;

    public HealthSystem() {
        super(Aspect.all(Health.class), 1);
    }

    @Override
    protected void process(int entity) {
        Health h = healthComponentMapper.get(entity);
        if (h.health <= 0) {

            //Spawn wreck
            if (world.getMapper(SpawnWreck.class).has(entity)) {
                world.getSystem(Region.RegionReference.class).getRegion().addWreck(world.getEntity(entity));
//                EntityBuilder entityBuilder = new EntityBuilder(getWorld());
//
//                entityBuilder.with(ChunkReference.class, ForceAccumulator.class, Pickupable.class);
//
//                Velocity velocity = world.getMapper(Velocity.class).getSafe(entity);
//                entityBuilder.with(new Velocity(velocity == null ? new Vector2d() : velocity.vector.clone().multSelf(0.8)));
//
//                Acceleration acceleration = world.getMapper(Acceleration.class).getSafe(entity);
//                entityBuilder.with(new Acceleration(acceleration == null ? new Vector2d() : acceleration.acc.clone().multSelf(0.5)));
//
//                Mass mass = world.getMapper(Mass.class).getSafe(entity);
//                if (mass != null)
//                    entityBuilder.with(Mass.newMassFromInverse(mass.inverseMass * 2));
//                else
//                    entityBuilder.with(new Mass());
//
//                entityBuilder.with(new Color(13));
//
//
//                entityBuilder.with(new Rendered("wreck"), new Position(world.getMapper(Position.class).get(entity).pos.clone()), new Health(10));
            }

            //Remove entitiy
            getWorld().delete(entity);

            System.out.println("Entity " + entity + " was " + h.lastDamageReason + " by " + Objects.toString(h.lastDamagedBy) + (shotByComponentMapper.has(h.lastDamagedBy)?"(" + shotByComponentMapper.get(h.lastDamagedBy).shooter + ")" : ""));
            PlayerManager playerManager = getWorld().getSystem(PlayerManager.class);
            getWorld().getSystem(Region.RegionReference.class).getRegion().server.chatMessage(
                    "Entity " + entity + " was " + h.lastDamageReason + " by " + Objects.toString(h.lastDamagedBy));
        }
    }

}
