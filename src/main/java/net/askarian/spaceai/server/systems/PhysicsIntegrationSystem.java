package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.artemis.systems.IteratingSystem;
import lombok.Data;
import net.askarian.spaceai.math.Vector2d;
import net.askarian.spaceai.server.Region;
import net.askarian.spaceai.server.components.physics.*;

/**
 * Dieses System bewegt
 * Created by alex on 9/28/15.
 */
@Wire
public class PhysicsIntegrationSystem extends IteratingSystem {

    /**
     * Holds the amount of damping applied to linear
     * motion. Damping is required to remove energy added
     * through numerical instability in the integrator.
     */
    double damping = 0.99d;

    ComponentMapper<Acceleration> accelerationComponentMapper;
    ComponentMapper<Velocity> velocityComponentMapper;
    ComponentMapper<Position> positionComponentMapper;
    ComponentMapper<Mass> massComponentMapper;
    ComponentMapper<ForceAccumulator> forceAccumulatorComponentMapper;

    Region.RegionReference regionReference;

    int duration = 1;

    public PhysicsIntegrationSystem() {
        super(Aspect.all(Acceleration.class, Velocity.class, Position.class, ForceAccumulator.class));
    }

    @Override
    protected void process(int entity) {
        Acceleration acceleration = accelerationComponentMapper.get(entity);
        Velocity velocity = velocityComponentMapper.get(entity);
        Position position = positionComponentMapper.get(entity);
        Mass mass = massComponentMapper.getSafe(entity);
        ForceAccumulator forceAccumulator = forceAccumulatorComponentMapper.get(entity);
        //Unendlich gro�e Masse => return
        if (mass == null || mass.inverseMass <= 0.0d)
            return;
        //lineare Position updaten
        position.pos.addScaledSelf(velocity.vector, duration);

        //�ber update informieren
        if (!velocity.vector.isZero()) {
            regionReference.getRegion().postEvent(new UpdatePosEvent(entity));
        }

        //Beschleunigung(Velocity) von Kraft(Force)
        Vector2d resAcc = acceleration.acc.clone();
        resAcc.addScaledSelf(forceAccumulator.res, mass.inverseMass);

        Vector2d v = velocity.vector.clone();
        //Beschleunigung(Velocity) updaten
        velocity.vector.addScaledSelf(resAcc, duration);

        //Dragging
        velocity.vector.multSelf(Math.pow(damping, duration));

        //Kraft zur�cksetzten
        forceAccumulator.res.clear();
    }

    /**
     * Event used to notify about position changes
     */
    public @Data static class UpdatePosEvent{
        final private int entity;
    }
}
