package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.artemis.systems.IteratingSystem;
import net.askarian.spaceai.server.components.EnergyEmission;

/**
 * Created by alex on 10/31/15.
 */
@Wire
public class ResetEnergyEmissionSystem extends IteratingSystem {
    ComponentMapper<EnergyEmission> energyEmissionComponentMapper;

    public ResetEnergyEmissionSystem() {
        super(Aspect.all(EnergyEmission.class));
    }

    @Override
    protected void process(int i) {
        energyEmissionComponentMapper.get(i).emitted = 0;
    }
}
