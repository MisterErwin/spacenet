package net.askarian.spaceai.server.systems;

import com.artemis.BaseSystem;
import lombok.NonNull;
import net.askarian.spaceai.server.components.physics.Mass;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MisterErwin on 29.12.2015.
 * In case you need it, ask me ;)
 */
public class MassSystem extends BaseSystem {
    private Map<String, Double> wheights = new HashMap<>();


    public Double getWeight(String name) {
        return wheights.get(name);
    }

    public double getWeight(String name, double def) {
        return wheights.getOrDefault(name, def);
    }

    public void addMass(@NonNull Mass massComponent, @NonNull String name, int amount) {
        massComponent.inverseMass = 1 / (1 / massComponent.inverseMass + getWeight(name, 0) * amount);
    }

    public void remMass(@NonNull Mass massComponent, @NonNull String name, int amount) {
        massComponent.inverseMass = 1 / (1 / massComponent.inverseMass - getWeight(name, 0) * amount);
    }

    @Override
    protected void processSystem() {

    }

    @Override
    protected boolean checkProcessing() {
        return false;
    }

}
