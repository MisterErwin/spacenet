package net.askarian.spaceai.server.systems.onload;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.annotations.Wire;
import net.askarian.spaceai.server.INotifyAboutSerAddSystem;
import net.askarian.spaceai.server.components.NodeAI;

/**
 * Created by MisterErwin on 26.12.2015.
 * In case you need it, ask me ;)
 */
@Wire
public class ReInvokeProgramSystem extends EntitySystem implements INotifyAboutSerAddSystem {
    ComponentMapper<NodeAI> nodeAIComponentMapper;
    public ReInvokeProgramSystem(){
        super(Aspect.all(NodeAI.class));
    }

    @Override
    protected void processSystem() {  }

    @Override
    protected boolean checkProcessing() {
        return false;
    }


    @Override
    public void reAddAfterLoad(Entity e) {
        NodeAI nodeAI = e.getComponent(NodeAI.class);
        if (nodeAI != null) {
//            nodeAI.program =
//            e.getWorld().getSystem(Region.RegionReference.class)
//                    .getRegion()
//                    .server
//                    .getProgramServer()
//                    .buildAndStartProgram(nodeAI.jso);
            nodeAI.program.setArtemisEntity(e);
            nodeAI.program.initAfterJSON();
            System.out.println("Invoked program " + nodeAI.program.toString());

        }
    }

    @Override
    public void removed(Entity e) {
        nodeAIComponentMapper.get(e).program.setAsEnded();
    }

    @Override
    public void inserted(Entity e) {
    }
}
