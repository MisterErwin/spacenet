package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.artemis.systems.IteratingSystem;
import net.askarian.spaceai.math.Vector2d;
import net.askarian.spaceai.server.components.EnergyEmission;
import net.askarian.spaceai.server.components.output.Thrusters;
import net.askarian.spaceai.server.components.physics.ForceAccumulator;

/**
 * Dieses System bewegt
 * Created by alex on 9/28/15.
 */
@Wire
public class ThrusterSystem extends IteratingSystem {

    ComponentMapper<ForceAccumulator> forceAccumulatorComponentMapper;
    ComponentMapper<Thrusters> thrustersComponentMapper;
    ComponentMapper<EnergyEmission> energyEmissionComponentMapper;


    public ThrusterSystem() {
        super(Aspect.all(Thrusters.class, ForceAccumulator.class));
    }

    @Override
    protected void process(int entity) {
        Vector2d v = thrustersComponentMapper.get(entity).getForce();
        if (v != null) {
            forceAccumulatorComponentMapper.get(entity).add(v);
            EnergyEmission ee = energyEmissionComponentMapper.getSafe(entity);
            if (ee != null)
                ee.emitted += v.length();
        }
    }
}
