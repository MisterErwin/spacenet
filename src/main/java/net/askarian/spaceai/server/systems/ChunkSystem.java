package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import com.google.common.eventbus.Subscribe;
import net.askarian.spaceai.math.Vector2d;
import net.askarian.spaceai.server.components.ChunkReference;
import net.askarian.spaceai.server.components.physics.Position;

import java.util.List;

/**
 * Created by alex on 10/27/15.
 */
@Wire
public class ChunkSystem extends BaseEntitySystem {
    public final int chunkSize;
    ComponentMapper<Position> positionComponentMapper;
    ComponentMapper<ChunkReference> chunkReferenceComponentMapper;
    final Table<Integer, Integer, List<Integer>> chunkTable;

    public ChunkSystem(int chunkSize) {
        super(Aspect.all(ChunkReference.class, Position.class));
        this.chunkSize = chunkSize;
        chunkTable = HashBasedTable.create();
    }

    @Override
    protected boolean checkProcessing() {
        return false;
    }

    @Override
    protected void processSystem() {
    }

    private ChunkSystem() {
        this(100);
    }

    @Override
    protected void inserted(int entityId) {
        super.inserted(entityId);
        Position pos = positionComponentMapper.get(entityId);
        ChunkReference chunk = chunkReferenceComponentMapper.getSafe(entityId, true);
        int x = (int) pos.pos.getX() / chunkSize, y = (int) pos.pos.getY() / chunkSize;

        List<Integer> entityList
                = chunkTable.get(chunk.x, chunk.y);
        if (entityList != null) { //Aus Liste entfernen
            entityList.remove(Integer.valueOf(entityId));
            if (entityList.isEmpty()) //Da die Liste jetzt leer ist, entfernen
                chunkTable.remove(chunk.x, chunk.y);
        }
        //Chunk updaten
        chunk.x = x;
        chunk.y = y;

        //In die Liste eintragen
        entityList = chunkTable.get(x, y);
        if (entityList == null) {
            entityList = Lists.newLinkedList();
            chunkTable.put(x, y, entityList);

        }
        entityList.add(Integer.valueOf(entityId));

    }

    @Subscribe
    public void onUpdatePosition(PhysicsIntegrationSystem.UpdatePosEvent updatePosEvent) {
        int entity = updatePosEvent.getEntity();
        Position pos = positionComponentMapper.get(entity);
        ChunkReference chunk = chunkReferenceComponentMapper.getSafe(entity, true);
        int x = (int) pos.pos.getX() / chunkSize, y = (int) pos.pos.getY() / chunkSize;

        if (chunk.x != x || chunk.y != y) { //Chunk ändern
            List<Integer> entityList
                    = chunkTable.get(chunk.x, chunk.y);
            if (entityList != null) { //Aus Liste entfernen
                entityList.remove(Integer.valueOf(entity));
                if (entityList.isEmpty()) //Da die Liste jetzt leer ist, entfernen
                    chunkTable.remove(chunk.x, chunk.y);
            }
            //Chunk updaten
            chunk.x = x;
            chunk.y = y;

            //In die Liste eintragen
            entityList = chunkTable.get(x, y);
            if (entityList == null) {
                entityList = Lists.newLinkedList();
                chunkTable.put(x, y, entityList);

            }
            entityList.add(Integer.valueOf(entity));
        }
    }

    @Override
    public void removed(int entityId) {
        ChunkReference chunkReference = chunkReferenceComponentMapper.getSafe(entityId);
        if (chunkReference != null) {
            List<Integer> entityList
                    = chunkTable.get(chunkReference.x, chunkReference.y);
            if (entityList != null) { //Aus Liste entfernen
                entityList.remove(Integer.valueOf(entityId));
                if (entityList.isEmpty()) //Da die Liste jetzt leer ist, entfernen
                    chunkTable.remove(chunkReference.x, chunkReference.y);
            }
        } else {
            System.out.println(entityId + " already lost its Chunk");
        }
        super.removed(entityId);
    }

    //Hinzufügen wird vom Null-abfangen gelöst

    public List<Integer> getEntitiesInChunk(Integer x, Integer y) {
        return chunkTable.get(x, y);
    }

    public List<Integer> getEntitiesInSameChunk(Entity entity) {
        ChunkReference chunkReference = chunkReferenceComponentMapper.getSafe(entity);
        if (chunkReference == null) return null;
        return getEntitiesInChunk(chunkReference.x, chunkReference.y);
    }

    public List<Integer> getNearbyEntities(Vector2d pos, int radius) {
        return getNearbyEntities(pos, radius, null);
    }

    public List<Integer> getNearbyEntities(Vector2d pos, int radius, ComponentMapper... reqComponents) {
        return getNearbyEntities((int) Math.floor(pos.getX()), (int) Math.floor(pos.getY()), radius, reqComponents);
    }

    public List<Integer> getNearbyEntities(int posX, int posY, int radius, ComponentMapper... reqComponents) {
        List<Integer> entityList = Lists.newLinkedList(), chunkList;
        int radiusSQ = radius * radius;
        Position pos;
        int radiusChunks = (int) Math.ceil(radius / chunkSize), startChunkX = (int) Math.floor(posX / chunkSize), startChunkY = (int) Math.floor(posY / chunkSize);
        for (int cX = startChunkX - radiusChunks; cX <= startChunkX + radiusChunks; ++cX) {
            for (int cY = startChunkY - radiusChunks; cY <= startChunkY + radiusChunks; ++cY) {
                chunkList = chunkTable.get(cX, cY);

                if (chunkList == null || chunkList.isEmpty()) //No entities in that chunk
                    continue;
                entityListLabel:
                for (Integer entityID : chunkList) {
                    pos = positionComponentMapper.getSafe(entityID);
                    if (pos == null) continue;
                    //Ist Entity im Radius?
                    if (pos.pos.distanceSQR(posX, posY) < radiusSQ) {
                        if (reqComponents != null) //If there are reqComponents, check them
                            for (ComponentMapper mapper : reqComponents)
                                if (!mapper.has(entityID))
                                    continue entityListLabel;
                        //Add to return liste
                        entityList.add(entityID);
                    }
                }
            }
        }
        return entityList;
    }


}
