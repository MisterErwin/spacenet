package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.IntervalEntityProcessingSystem;
import net.askarian.spaceai.server.components.ControlledComponent;

/**
 * Created by alex on 9/28/15.
 */
@Wire
public class ControlSystem extends IntervalEntityProcessingSystem {
    ComponentMapper<ControlledComponent> componentComponentMapper;
    public ControlSystem() {
        super(Aspect.all(ControlledComponent.class), 1);
    }

    @Override
    protected void process(Entity entity) {
        ControlledComponent cp = componentComponentMapper.get(entity);
        cp.controlServer.input(cp, entity.getId(), cp.input);
        if (cp.res != null)
            handleOutput(cp,entity);
    }

    private void handleOutput(ControlledComponent controlledComponent, Entity entity) {

    }
}
