package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import net.askarian.spaceai.server.components.Health;
import net.askarian.spaceai.server.components.Owned;
import net.askarian.spaceai.server.components.Pickupable;
import net.askarian.spaceai.server.components.ShootAtNearby;
import net.askarian.spaceai.server.components.output.Cannon;
import net.askarian.spaceai.server.components.physics.Position;

/**
 * Created by alex on 9/28/15.
 */
public class ShotAtNearbySystem extends IteratingSystem {
    public ShotAtNearbySystem() {
        super(Aspect.all(ShootAtNearby.class, Cannon.class, Position.class));
    }

    ChunkSystem chunkSystem;
    ComponentMapper<ShootAtNearby> shootAtNearbyComponentMapper;
    ComponentMapper<Cannon> cannonComponentMapper;
    ComponentMapper<Position> positionComponentMapper;
    ComponentMapper<Health> healthComponentMapper;
    ComponentMapper<Owned> ownedComponentMapper;
    ComponentMapper<Pickupable> pickupableComponentMapper;
    boolean done;

    @Override
    protected void process(int entity) {
        ShootAtNearby shootAtNearby = shootAtNearbyComponentMapper.get(entity);
        Position pos = positionComponentMapper.get(entity);
        String owner = ownedComponentMapper.get(entity).owner;
        done = false;
//        System.out.println(">"+chunkSystem.getNearbyEntities(pos.pos, 11000, healthComponentMapper,positionComponentMapper).size());
        java.util.Optional<Integer> target = chunkSystem.getNearbyEntities(pos.pos, shootAtNearby.radius, healthComponentMapper, positionComponentMapper)
                .stream().filter(integer ->
                        (shootAtNearby.friendlyFire || !owner.equals(get(ownedComponentMapper.getSafe(integer, false)))) &&
                                ( !pickupableComponentMapper.has(integer))
                ).sorted((o1, o2) -> {
//                    System.out.println(o2);
                    return Double.compare(
                            pos.pos.distanceSQR(positionComponentMapper.get(o1).pos),
                            pos.pos.distanceSQR(positionComponentMapper.get(o2).pos)
                    );
                }).findFirst();
//        System.out.println(target.orElse(-1));
        if (target.isPresent())
            cannonComponentMapper.get(entity).dir = Cannon.getDir(pos.pos, positionComponentMapper.get(target.get()).pos);

    }

    private String get(Owned owned) {
        return owned == null ? null : owned.owner;
    }
}
