package net.askarian.spaceai.server.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.artemis.systems.IntervalIteratingSystem;
import net.askarian.spaceai.FlowAiServer.components.impl.cam.CameraComponent;
import net.askarian.spaceai.FlowAiServer.components.impl.cam.CameraTriggerComponent;
import net.askarian.spaceai.server.components.NodeAI;
import net.askarian.spaceai.server.components.input.Camera;
import net.askarian.spaceai.server.components.output.Color;
import net.askarian.spaceai.server.components.output.Visible;
import net.askarian.spaceai.server.components.physics.Position;
import net.askarian.spaceai.server.components.rendering.Watchable;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

/**
 * Created by alex on 9/28/15.
 */
@Wire
public class CameraSystem extends IntervalIteratingSystem {
    ComponentMapper<Camera> cameraComponentMapper;
    ComponentMapper<Position> positionComponentMapper;
    ComponentMapper<Color> colorComponentMapper;
    ComponentMapper<Watchable> watchableComponentMapper;
    ComponentMapper<NodeAI> nodeAIComponentMapper;

    ChunkSystem chunkSystem;

    public CameraSystem() {
        super(Aspect.one(Camera.class), 10);
    }

    private final NumberFormat plusMinusNF = new DecimalFormat("+#;-#");


    @Override
    protected void process(int entity) {
        Camera camera = cameraComponentMapper.get(entity);
        Position position = positionComponentMapper.get(entity);
//        int[] other = getSubscription().getEntities().getData();
        Visible visible;
        Position posO;
        double dist;
        camera.data = new int[camera.CAMERA_SIZE];
        camera.colorData = new int[camera.CAMERA_SIZE];
        List<Integer> other = chunkSystem.
                getNearbyEntities(position
                        .pos, camera.MAX_DISTANCE);
        boolean changed = false;
        for (Integer otherID : other)
            if (otherID != entity) {
                Color color = colorComponentMapper.getSafe(otherID);
                if (color == null) continue;
                posO = positionComponentMapper.get(otherID);
                if (posO == null) continue;
                dist = position.pos.distanceSQR(posO.pos);
//                if (dist > Camera.MAX_DISTANCE_SQ) continue;
//                double alphaImBogenmass = Math.atan((posO.pos.getY() - position.pos.getY()) / (posO.pos.getX() - position.pos.getX()));
                double alphaImBogenmass = Math.atan2(posO.pos.getY() - position.pos.getY(), posO.pos.getX() - position.pos.getX());
//                double alphaImBogenmass = Math.atan((posO.pos.getY() - position.pos.getY()) / (position.pos.getX() - posO.pos.getX()));

                double alpha = Math.toDegrees(alphaImBogenmass);

                //i sollte der Bereich sein, in dem sich der Winkel befindet
                int i = (int) Math.floor(alphaImBogenmass / camera.CAMERA_ONE_RANGE);


                //TODO: Wider things like stars

                while (i < 0)
                    i += camera.CAMERA_SIZE;
                while (i >= camera.CAMERA_SIZE)
                    i -= camera.CAMERA_SIZE;

//                if (entity==1) {
//                    System.out.println(plusMinusNF.format(i) + "       " + alpha);
//                }

                // Distanz in die Kamera eintragen - "Versteckspielen" berücksichtigen
                camera.data[i] = (int) (camera.MAX_DISTANCE_SQ - dist);// * ((100 - visible.antiVisiblePercentage) / 100d);
                //Jetzt die Farbe im Sektor
                camera.colorData[i] = color.color;
                changed = true;
            }
        if (changed) {
            Watchable watchable = watchableComponentMapper.getSafe(entity);
            if (watchable != null)
                watchable.update(Watchable.WatchUT_CAMERA);
            NodeAI nodeAI = nodeAIComponentMapper.getSafe(entity);
            if (nodeAI != null && nodeAI.program != null) {
                CameraTriggerComponent.notifyChange(nodeAI.program, camera.colorData);
                CameraComponent.notifyChange(nodeAI.program, camera.colorData);
            }
        }
    }
}
