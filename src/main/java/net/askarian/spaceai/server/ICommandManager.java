package net.askarian.spaceai.server;

/**
 * Created by alex on 10/30/15.
 */
public interface ICommandManager {
    boolean onCommand(String command);
}
