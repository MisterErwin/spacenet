package net.askarian.spaceai.server;

import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.google.common.collect.EvictingQueue;
import net.askarian.spaceai.math.Vector2d;
import net.askarian.spaceai.server.builder.PartRegistry;
import net.askarian.spaceai.server.components.ChunkReference;
import net.askarian.spaceai.server.components.input.Camera;
import net.askarian.spaceai.server.components.output.Cannon;
import net.askarian.spaceai.server.components.output.Color;
import net.askarian.spaceai.server.components.physics.ForceAccumulator;
import net.askarian.spaceai.server.components.physics.Position;
import net.askarian.spaceai.server.components.rendering.Rendered;
import net.askarian.spaceai.server.util.RegisterUtils;
import org.apache.commons.lang.math.NumberUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by alex on 10/27/15.
 */
public class ConsoleTestServer implements ICommandManager {

    public static void main(String... args) {
        new ConsoleTestServer();
    }

    private Region region;
    private ServerRunnable runnable;
    private WSServer wsServer;

    public static boolean DEBUG = false, REGISTER = true;

    private final int port = 8123;

    public ConsoleTestServer() {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("~ |SpaceNET| Console Test Server ~");


        PartRegistry pt = new PartRegistry();
        pt.loadParts();

        region = new Region(new File("saveGame.json"));
        region.partRegistry = pt;

        wsServer = new WSServer(region, port, this);
        region.init(wsServer);


        wsServer.start();

        if (!wsServer.isRunning()) {
            System.out.println("Failed to start the websocket server!");
            return;
        }


        //Register the server
        if (REGISTER)
            RegisterUtils.registerServerOnAskarian(port, getClass().getSimpleName());

        //

        runnable = new ServerRunnable();

        runnable.currentThread.start();

        boolean doRun = true;

        System.out.println("Server started!");
        while (doRun) {
            try {
                doRun = onCommand(bufferedReader.readLine());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("Stopping server");

        Thread t = runnable.currentThread;

        runnable.currentThread = null;

        try {
            t.interrupt();
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        region.stop();

        try {
            wsServer.stop(100);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Server stopped");
        System.exit(1);
    }

    public boolean onCommand(String c) {
        if (c == null) return false;
        String[] line = c.split(" ");
        int i;

        switch (line[0].toLowerCase()) {
            case "save":
                region.save();
                break;
            case "reset":
                region.getSaveFile().delete();
                return false;
            case "stop":
                return false;
            case "addbase":
                region.addBase("console");
                break;
//            case "spawntv":
//                i = region.addTV("console");
//                System.out.println("TV " + i + "spawned");
//                break;
            case "gc":
                System.out.println("Maximum memory" + (Runtime.getRuntime().maxMemory() / 1024 / 1024) + "MB");
                System.out.println("Allocated memory" + (Runtime.getRuntime().totalMemory() / 1024 / 1024) + "MB");
                System.out.println("Free memory" + (Runtime.getRuntime().freeMemory() / 1024 / 1024) + "MB");

                System.out.print("Last delays:");
                while (!runnable.lastDeltaQueue.isEmpty())
                    System.out.print(" " + runnable.lastDeltaQueue.poll());
                System.out.println();
                break;
            case "spawndrone":
                i = region.addDrone();
                System.out.println("Drone " + i + " spawned");
                break;
//            case "sd":
//                i = region.addAIDrone(line.length > 1 ? line[1] : null, "console");
//                System.out.println("AIDrone " + i + " spawned");
//                break;
//            case "spawnai":
            case "s":
                if (true) {
                    i = region.addDrone();
                    Entity e = region.getArtemisWorld().getEntity(i);
                    Vector2d pos = e.getComponent(Position.class).pos;
                    pos.setX(0);
                    pos.setY(20);
                    Rendered rendered = e.getComponent(Rendered.class);
                    if (rendered != null)
                        Rendered.addUpdateType(rendered, (IViewer) null, Rendered.UpdateType_ALL);

                    i = region.addDrone();
                    e = region.getArtemisWorld().getEntity(i);
                    pos = e.getComponent(Position.class).pos;
                    pos.setX(50);
                    pos.setY(0);
                    rendered = e.getComponent(Rendered.class);
                    if (rendered != null)
                        Rendered.addUpdateType(rendered, (IViewer) null, Rendered.UpdateType_ALL);
                    ForceAccumulator forceAccumulator = e.getComponent(ForceAccumulator.class);
                    forceAccumulator.add(new Vector2d(-1, 0));
                }
                break;
            case "s3":
                for (int n = 0; n < 20; ++n) {
                    i = region.addDrone();
                    Entity e = region.getArtemisWorld().getEntity(i);
                    ForceAccumulator forceAccumulator = e.getComponent(ForceAccumulator.class);
                    forceAccumulator.add(Vector2d.random(2.0));
                }
                break;
            case "s2":
                if (true) {
                    i = region.addDrone();
                    Entity e = region.getArtemisWorld().getEntity(i);
                    Vector2d pos = e.getComponent(Position.class).pos;
                    pos.setX(0);
                    pos.setY(20);
                    Rendered rendered = e.getComponent(Rendered.class);
                    if (rendered != null)
                        Rendered.addUpdateType(rendered, (IViewer) null, Rendered.UpdateType_ALL);

                    i = region.addDrone();
                    e = region.getArtemisWorld().getEntity(i);
                    pos = e.getComponent(Position.class).pos;
                    pos.setX(0);
                    pos.setY(40);
                    rendered = e.getComponent(Rendered.class);
                    if (rendered != null)
                        Rendered.addUpdateType(rendered, (IViewer) null, Rendered.UpdateType_ALL);
                }
                break;
            case "getchunk":
                Entity e = region.getArtemisWorld().getEntity(line.length > 1 ? NumberUtils.toInt(line[1], 0) : 0);
                if (e == null) System.out.println("No entity found");
                else
                    System.out.println("X:" + e.getComponent(ChunkReference.class).x + ";Y:" + e.getComponent(ChunkReference.class).y);
                break;
            case "getpos":
                e = region.getArtemisWorld().getEntity(line.length > 1 ? NumberUtils.toInt(line[1], 0) : 0);
                if (e == null) System.out.println("No entity found");
                else
                    System.out.println(e.getComponent(Position.class).pos.toString());
                break;
            case "getcam":
            case "getcamera":
                e = region.getArtemisWorld().getEntity(line.length > 1 ? NumberUtils.toInt(line[1], 0) : 0);
                if (e == null) System.out.println("No entity found");
                else {
                    Camera camera = e.getComponent(Camera.class);
                    System.out.println(Arrays.toString(camera.colorData));
                    System.out.println(Arrays.toString(camera.data));
                }
                break;
            case "remove":
            case "rem":
                e = region.getArtemisWorld().getEntity(line.length > 1 ? NumberUtils.toInt(line[1], 0) : 0);
                if (e == null) System.out.println("No entity found");
                else {
                    e.deleteFromWorld();
                }
                break;

            case "setcolor":
                e = region.getArtemisWorld().getEntity(line.length > 1 ? NumberUtils.toInt(line[1], 0) : 0);
                if (e == null) System.out.println("No entity found");
                else {
                    e.getComponent(Color.class).color = line.length > 2 ? NumberUtils.toInt(line[2], 0) : 0;
                    Rendered rendered = e.getComponent(Rendered.class);
                    if (rendered != null)
                        Rendered.addUpdateType(rendered, (IViewer) null, Rendered.UpdateType_COLOR);

                }
                break;
            case "weapon":
            case "shot":
                e = region.getArtemisWorld().getEntity(line.length > 1 ? NumberUtils.toInt(line[1], 0) : 0);
                if (e == null) System.out.println("No entity found");
                else {
                    e.getComponent(Cannon.class).dir = line.length > 2 ? NumberUtils.toInt(line[2], 0) : 0;
                }
                break;

            case "setpos":
                e = region.getArtemisWorld().getEntity(line.length > 1 ? NumberUtils.toInt(line[1], 0) : 0);
                if (e == null) System.out.println("No entity found");
                else {
                    Vector2d pos = e.getComponent(Position.class).pos;
                    pos.setX(line.length > 2 ? NumberUtils.toDouble(line[2], pos.getX()) : pos.getX());
                    pos.setY(line.length > 3 ? NumberUtils.toDouble(line[3], pos.getY()) : pos.getY());
                    Rendered rendered = e.getComponent(Rendered.class);
                    if (rendered != null) {
                        rendered.visibleVia.clear();
                        Rendered.addUpdateType(rendered, (IViewer) null, Rendered.UpdateType_ALL);
                    }
                    System.out.println("Pos set");
                }
                break;

            case "force":
                e = region.getArtemisWorld().getEntity(line.length > 1 ? NumberUtils.toInt(line[1], 0) : 0);
                if (e == null) System.out.println("No entity found");
                else {
                    Vector2d v = new Vector2d(
                            line.length > 2 ? NumberUtils.toDouble(line[2], 0) : 0,
                            line.length > 3 ? NumberUtils.toDouble(line[3], 0) : 0);
                    ForceAccumulator forceAccumulator = e.getComponent(ForceAccumulator.class);
                    forceAccumulator.add(v);
                    System.out.println("Force " + v.toString() + " applied");
                }
                break;
            case "setsleep":
            case "setdelta":
                runnable.sleep = line.length > 1 ? NumberUtils.toInt(line[1], 1000) : 1000;
                System.out.println("Set delta to " + runnable.sleep);
                break;
            case "pause":
            case "start":
                runnable.running = !runnable.running;
                System.out.println("Running set to " + runnable.running);
                break;
            case "tick":
            case "process":
                region.process(1);
                break;
            case "killdrone":
            case "killdrones":
                final int[] n = {0};
                region.getArtemisWorld().getSystem(GroupManager.class).getEntities("drone").forEach(ee -> {
                    ee.deleteFromWorld();
                    ++n[0];
                });
                System.out.println("deleted " + n[0] + " drones");
                break;
            case "killai":
                final int[] x = {0};
                region.getArtemisWorld().getSystem(GroupManager.class).getEntities("ai").forEach(ee -> {
                    ee.deleteFromWorld();
                    ++x[0];
                });
                System.out.println("deleted " + x[0] + "ais");
                break;
            default:
                System.out.println("Unknown command");
                return true;

        }
        return true;
    }

    class ServerRunnable implements Runnable {
        public boolean running = true;
        public Thread currentThread = new Thread(this);
        public int sleep = 1000;
        private long lastSleep = System.currentTimeMillis();
        public EvictingQueue<Long> lastDeltaQueue = EvictingQueue.create(10);


        public void run() {
            while (true)
                if (currentThread == Thread.currentThread()) {
                    if (running) {
                        lastSleep = System.currentTimeMillis();
                        region.process(sleep);
                        lastDeltaQueue.add(System.currentTimeMillis() - lastSleep);
                        try {
                            Thread.currentThread().sleep(sleep);
                        } catch (InterruptedException e) {
                        }
                    } else
                        try {
                            Thread.currentThread().sleep(10);
                        } catch (InterruptedException e) {
                        }
                } else
                    return;


        }
    }

    public static boolean deleteDirectory(File directory) {
        if (directory.exists()) {
            File[] files = directory.listFiles();
            if (null != files) {
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    } else {
                        files[i].delete();
                    }
                }
            }
        }
        return (directory.delete());
    }

}
