package net.askarian.spaceai.server;

import com.artemis.World;

/**
 * Created by MisterErwin on 11.01.2016.
 * In case you need it, ask me ;)
 */
public class ShowMain {

    World artemisWorld;

    void main() throws InterruptedException {
        final long optimaleTickZeit =  1000000000 / 20; //20 mal pro Sekunde
        long letzteLoopZeit = System.nanoTime();

        while (true){ //Hauptschleife des Spieles
            long jetzt = System.nanoTime();
            long updateZeit = jetzt-letzteLoopZeit;

            letzteLoopZeit = jetzt;

            float delta = updateZeit / (float)optimaleTickZeit;

            artemisWorld.setDelta(delta);
            artemisWorld.process();

            Thread.sleep( (letzteLoopZeit-System.nanoTime() + optimaleTickZeit)/1000000 );
        }
    }

    void behandleUpdate(double delta){

    }
}
