package net.askarian.spaceai.server;

import net.askarian.spaceai.server.components.ControlledComponent;

import java.util.Map;

/**
 * Created by alex on 9/28/15.
 */
public interface IControlServer {
    void input(ControlledComponent controlledComponent, int entity, Map<String,Integer> inputNodes);
}
