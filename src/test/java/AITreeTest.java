import com.google.gson.reflect.TypeToken;
import net.askarian.spaceai.server.ai.linear.AITree;
import net.askarian.spaceai.server.ai.linear.AITreeLoader;
import net.askarian.spaceai.server.ai.linear.IDroneMemory;
import net.askarian.spaceai.server.ai.linear.SimpleDroneMemory;

import java.lang.reflect.Type;
import java.util.Arrays;

/**
 * Created by alex on 10/30/15.
 */
public class AITreeTest {

    public static void main(String... args){
        AITree tree = testTree();

        IDroneMemory droneMemory = new SimpleDroneMemory();

        String s = AITreeLoader.gson.toJson(tree);
        System.out.println(s);

        Type type = new TypeToken<AITree>() { /*Empty intended*/
        }.getType();

        System.out.println(AITreeLoader.gson.fromJson(s, type).toString());

        AITree tree1 = AITreeLoader.gson.fromJson(
                "{\"rootNode\":{\"params\":[\"col\"],\"children\":[{\"params\":[\"dir\",\"col\"],\"children\":[{\"params\":[\"dir\",\"10\"],\"children\":[{\"params\":[\"dir\"],\"children\":[{\"params\":[\"dir\"],\"children\":[]}]}]}]}]}}"

                , type);

        System.out.println(tree1.toString());

    }

    public static AITree testTree(){
        AITree tree = new AITree(
                new AITree.AITreeNode(Arrays.asList(
                        new AITree.AITreeNode(null, "print a".split(" "))

                ), "addself a 10".split(" ")));


        return tree;
    }
}
