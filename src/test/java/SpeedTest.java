import lombok.Data;

/**
 * Created by alex on 11/1/15.
 */
public class SpeedTest {


    public static void main(String... s) {
        Runtime runtime = Runtime.getRuntime();
        long start = System.currentTimeMillis();
        int n = Integer.MAX_VALUE;
        BaseClass bc = new B1();
        for (int i = 0; i < n; ++i) {

            if (bc.getI() == 1) {
                bc = new B2();
            } else if (bc.getI() == 2) {
                bc = new B3();
            } else if (bc.getI() == 3) {
                bc = new B4();
            } else if (bc.getI() == 4) {
                bc = new B5();
            } else if (bc.getI() == 5) {
                bc = new B1();
            }
        }
        System.out.println(System.currentTimeMillis() - start + "||"+runtime.freeMemory());
        System.gc();
        start = System.currentTimeMillis();
        bc = new B1();
        for (int i = 0; i < n; ++i) {
            if (bc instanceof B1)
                bc = new B2();
            else if (bc instanceof B2)
                bc = new B3();
            else if (bc instanceof B3)
                bc = new B4();
            else if (bc instanceof B4)
                bc = new B5();
            else if (bc instanceof B5)
                bc = new B1();
        }
        System.out.println(System.currentTimeMillis() - start + "||"+runtime.freeMemory());
        System.gc();
        start = System.currentTimeMillis();
        bc = new B1();
        for (int i = 0; i < n; ++i) {
            switch (bc.getI()) {
                case 1:
                    bc = new B2();
                    break;
                case 2:
                    bc = new B3();
                    break;
                case 3:
                    bc = new B4();
                    break;
                case 4:
                    bc = new B5();
                    break;
                case 5:
                    bc = new B1();
                    break;
            }
        }
        System.out.println(System.currentTimeMillis() - start + "||"+runtime.freeMemory());
    }


    @Data
    static class BaseClass {
        private final int i;
    }

    static class B1 extends BaseClass {
        public B1() {
            super(1);
        }
    }

    static class B2 extends BaseClass {
        public B2() {
            super(2);
        }
    }

    static class B3 extends BaseClass {
        public B3() {
            super(3);
        }
    }

    static class B4 extends BaseClass {
        public B4() {
            super(4);
        }
    }

    static class B5 extends BaseClass {
        public B5() {
            super(5);
        }
    }
}
