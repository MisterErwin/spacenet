import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.util.Scanner;

/**
 * Created by MisterErwin on 16.02.2016.
 * In case you need it, ask me ;)
 */
public class CountLines {
    public static void main(String... args) throws Exception {
        A a = new A();

        File folder = new File("D:\\Dokumente\\java\\spacenet\\src");

        count(folder, a);

        System.out.println("Total files " + a.totalFiles);
        System.out.println("Total lines " + a.totalLines);
        System.out.println("Non empty lines " + a.nonEmptyLines);
        System.out.println("Directories " + a.totalDirs);

        System.out.println();

        System.out.println("Max: " + a.maxLines);

        System.out.println();

        System.out.println("Lines per file " + (a.totalLines / a.totalFiles));
        System.out.println("NElines per file " + (a.nonEmptyLines / a.totalFiles));
    }

    static void count(File f, A a) throws Exception {
        for (File c : f.listFiles()) {
            if (c.isDirectory()) {
                a.totalDirs++;
                count(c, a);
            } else if (c.getName().endsWith(".java")) {
                Scanner scanner = new Scanner(c);
                ++a.totalFiles;
                int lines = 0;
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    ++lines;
                    if (!StringUtils.isEmpty(line))
                        ++a.nonEmptyLines;
                }
                a.totalLines += lines;
                if (a.maxLines < lines)
                    a.maxLines = lines;
            }
        }
    }

    static class A {
        int totalLines = 0;
        int totalFiles = 0;
        int nonEmptyLines = 0;
        int totalDirs = 0;
        int maxLines = 0;
    }
}
