/**
 * Created by alex on 11/1/15.
 */
$(function () {
    $("#aiselect").val([]).chosen({
        no_results_text: 'No AI found',
        width: '100%'
    }).on("change", function () {
        if (this.value == "__reload") {
            reloadStoredAIs();
        } else {
            setBaseProgram(this.value);
        }
    });

    $("#base_autobuild").change(function () {
        var o = {type: "setbaseautobuild", autobuild: $(this).is(":checked")}
        client.ws.send(JSON.stringify(o));
        console.log(o);
    });

    /* $("#schemataselect").val([]).chosen({
     no_results_text: '',
     width: '100%'
     }).on("change", function () {
     if (this.value == "__new") {
     $("#schemataselect").val([]).trigger("chosen:updated");
     $("#drone-builder").modal();
     return;
     }
     //console.log(this.value);
     });*/

    reloadStoredAIs();
    //reloadStoredSchematics();
});


function reloadStoredAIs() {
    var select = $("#aiselect");
    select.empty();

    if (localStorage.storedAIs) {
        var all = JSON.parse(localStorage.storedAIs);

        $.each(all, function (name) {
            select.append(
                $("<option/>", {text: name, value: name})
            );
        });
    }


    select.append(
        $("<option/>", {text: '[refresh]', value: '__reload'})
    );

    select.val([]);

    select.trigger("chosen:updated")
}

function testProduce() {
    moveProgressBar($("#producingBar").data("progress-percent", 100).data("progress-finish", "createdDrone"));
}

function createdDrone() {
    $("#startDroneButton").prop("disabled", false);
}

/*

 function reloadStoredSchematics() {
 var select = $("#schemataselect");
 select.empty();

 if (localStorage.storedSchematas) {
 var all = JSON.parse(localStorage.storedSchematas);

 $.each(all, function (name) {
 select.append(
 $("<option/>", {text: name, value: name})
 );
 });
 }


 select.append(
 $("<option/>", {text: 'New', value: '__new'})
 );

 select.val([]);

 select.trigger("chosen:updated")
 }
 */


function compactData(d) {
    var v = JSON.parse(JSON.stringify(d));//data.slice();
    v.forEach(function (c) {
        if ("iP" in c) {
            for (var p in c.iP) {
                for (var i = 0; i < c.iP[p].length; ++i)
                    if (c.iP[p][i][0] != -1)
                        c.iP[p].splice(i);
                if (c.iP[p].length == 0)
                    delete c.iP[p];
            }
            if (size(c.iP) == 0)
                delete c.iP;
        }
    });
    return v;
}

function size(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function setBaseProgram(name) {
    var all = (localStorage.storedAIs) ? JSON.parse(localStorage.storedAIs) : {};
    var o = {program: compactData(all[name]), pname: name, type: "setbaseprogram"};
    client.ws.send(JSON.stringify(o));
}

function produceOne(){
    client.ws.send(JSON.stringify({type: "produceone"}));
}

function spawnAI(name) {
    if (name == undefined)
        name = $("#aiselect").val();
    if (name == "___spawn") {
        var mod = $("#mod");
        mod.html("<a href='dev2.html' target='_blank'>Open editor</a>");
        mod.modal();
        return;
    }
    //$.modal.close();
    var all = (localStorage.storedAIs) ? JSON.parse(localStorage.storedAIs) : {};
    if (name in all) {
        $("#startDroneButton").prop("disabled", true);
        var o = {program: JSON.stringify(compactData(all[name])), pname: name, type: "spawndrone"};
        client.ws.send(JSON.stringify(o));


    } else {
        var mod = $("#mod");
        if (name == null)
            mod.html("<b>Please select an AI program</b> <a rel='modal:open' href='#base-control'>Retry</a>");
        else
            mod.html("<b>" + name + "</b> is not a valid AI program (not found)");
        mod.modal();
        return;
    }


}