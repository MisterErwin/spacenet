/**
 * Created by alex on 11/10/15.
 */
var selectedElement = 0;
var currentX = 0;
var currentY = 0;
var currentMatrix = 0;

function svgMove(e) {
    updateConPreviewLine(e.pageX, e.pageY);
}

function delcon(id, lineonly) {
    console.log("delcon ", id);
    var line = document.getElementById(id);
    if (!line)return;
    if (!lineonly) {
        var sp = id.split("_");
        var s = sp[1].split("/");
        var aID = parseInt(s[0]), aP = parseInt(s[1]);
        s = sp[2].split("/");
        var bID = parseInt(s[0]), bP = parseInt(s[1]);

        var a = data[aID].oP[aP], index = findDataI(a, bID, bP);
        data[aID].oP[aP].splice(index, 1);

        a = data[bID].iP[bP], index = findDataI(a, aID, aP);
        data[bID].iP[bP].splice(index, 1);
    }
    line.parentNode.removeChild(line);
    return;
}


function onRightClick(evt) {
    if (evt.target.tagName == "svg") {
        updateConPreviewLine();
        console.log("UN62");
        port = undefined;
        showMenu(evt.clientX, evt.clientY, "X", [["plus", "newCompMenu(" + evt.clientX + "," + evt.clientY + ");", "Create a new node"],
            ["floppy-disk", "exportP();", "Save this program"], ["loop2", "runP();", "Run this program"], ["trash","killD();", "Kill spawned drone"]]);
    } else if (evt.target.tagName == "line" && evt.target.id.split("_")[0] == "con") {
        showMenu(evt.clientX, evt.clientY, "line", [["trash", "delcon('" + evt.target.id + "')", "remove connection"]]);
    } else if (evt.target.tagName == "circle" && hasClass(evt.target, "port")) {
        var id = parseInt(evt.target.parentNode.parentNode.getAttributeNS(null, "data-id")),
            p = parseInt(Array.prototype.indexOf.call(evt.target.parentNode.children, evt.target));

        if (hasClass(evt.target, "port-allowsConstant"))
            showMenu(evt.clientX, evt.clientY, "port", [[], [], [], ["cog", "setConstant(" + id + "," + p + ")", "Set constant"]]);
        else
            console.log(evt.target);
    } else if (hasClass(evt.target.parentNode, "component")) {
        var id = parseInt(evt.target.parentNode.getAttributeNS(null, "data-id"));
        showMenu(evt.clientX, evt.clientY, "X", [["trash", "delComp(" + id + ");", "Delete this component"],
            ["plus", "newCompMenu(" + evt.clientX + "," + evt.clientY + ");", "Create a new node"]
            , ["info", "showInfo('" + data[id].type + "')", "Opens the docs for more information"]]);

    }

    return false;
}

function showMenu(x, y, txt, parts) {
    var menu = document.getElementById("menu");
    var matrix = menu.getAttributeNS(null, "transform").slice(7, -1).split(' ');

    for (var i = 0; i < matrix.length; i++) {
        matrix[i] = parseFloat(matrix[i]);
    }


    if (!x) { //hide
        matrix[4] = -1000;
        menu.setAttributeNS(null, "transform", "matrix(" + matrix.join(' ') + ")");
        return;
    }

    var torem = document.getElementsByClassName("menu-remove");
    for (var i = 0; i < torem.length; ++i)
        if (torem[i].parentNode)
            torem[i].parentNode.removeChild(torem[i]);
    //menu.setAttributeNS(null, "transform", "matrix(1 0 0 1 " + x + " " + y + ")");

    matrix[4] = x - 102;
    matrix[5] = y;


    menu.setAttributeNS(null, "transform", "matrix(" + matrix.join(' ') + ")");

    if (txt) {
        var text = document.createElementNS(NSN, "text");
        text.appendChild(document.createTextNode(txt));
        text.setAttribute("text-anchor", "middle");
        text.setAttribute("fill", "#fff");
        text.setAttribute("x", 240);
        text.setAttribute("y", 280);
        text.setAttribute("font-size", "4em");
        //text.setAttribute("transform","rotate(45, 230, 280)")
        text.setAttribute("class", "menu-remove");

        document.getElementById("trigger").appendChild(text);

        if (parts) {
            for (var i = 0; i < 4; ++i)
                if (parts[i]) {
                    document.getElementById("symbolsContainer").children[i].children[1].setAttribute("xlink:href", "#" + parts[i][0]);
                    var l = document.getElementById("itemsContainer").children[i];
                    l.setAttributeNS("xlink", "href", parts[i][1]);
                    //l.setAttributeNS("xlink", "title", parts[i][2]);
                    setTitle(l, parts[i][2]);

                } else {
                    document.getElementById("symbolsContainer").children[i].children[1].setAttribute("xlink:href", "");
                    var l = document.getElementById("itemsContainer").children[i];
                    l.setAttributeNS("xlink", "href", undefined);
                    //l.setAttributeNS("xlink", "title", "");
                    setTitle(l, "");

                }

        }
    }


}

function alreadyConnected(id1, port1, id2, port2) {
    var pD = data[id1].oP[port1];
    if (!pD)
        console.log(id1, port1, id2, port2);
    for (var i = 0; i < pD.length; ++i)
        if (pD[i][0] == id2 && pD[i][1] == port2)
            return true;
    return false;
}

function selectElement(evt) {
    if (evt.button == 2)return;
    showMenu();
    if (evt.target.tagName == "path" && evt.target.parentNode.tagName == "a" && hasClass(evt.target.parentNode, "item")) {
        var x = evt.target.parentNode.getAttributeNS("xlink", "href");
        console.log(x);
        if (x)eval(x);

        return false;
    } else if (evt.target.tagName == "svg") {

        return;
    } else if (evt.target.tagName == "line") {
        var sp = evt.target.id.split("_");
        if (sp.length != 3)return;
        var s = sp[1].split("/");
        var aID = parseInt(s[0]), aP = parseInt(s[1]);
        s = sp[2].split("/");
        var bID = parseInt(s[0]), bP = parseInt(s[1]);

        var a = data[aID].oP[aP], index = findDataI(a, bID, bP);
        data[aID].oP[aP].splice(index, 1);

        a = data[bID].iP[bP], index = findDataI(a, aID, aP);
        data[bID].iP[bP].splice(index, 1);

        evt.target.parentNode.removeChild(evt.target);
        return;
    } else if (hasClass(evt.target, "port")) {
        if (!hasClass(evt.target, "port-allowsPort") && hasClass(evt.target, "port-allowsConstant") && hasClass(evt.target.parentNode, "inports")) {
            if (port == undefined) {
                var id = parseInt(evt.target.parentNode.parentNode.getAttributeNS(null, "data-id")),
                    p = parseInt(Array.prototype.indexOf.call(evt.target.parentNode.children, evt.target));
                setConstant(id, p);
            }
            return;
        }
        //Remove the preview
        updateConPreviewLine(undefined);
        if (port == undefined) {
            port = [parseInt(evt.target.parentNode.parentNode.getAttributeNS(null, "data-id")),
                Array.prototype.indexOf.call(evt.target.parentNode.children, evt.target),
                hasClass(evt.target.parentNode, "inports"),
                hasClass(evt.target, "port-struct")];
        } else {
            var isInput = hasClass(evt.target.parentNode, "inports"),
                id = parseInt(evt.target.parentNode.parentNode.getAttributeNS(null, "data-id")),
                p = parseInt(Array.prototype.indexOf.call(evt.target.parentNode.children, evt.target)),
                isStruct = hasClass(evt.target, "port-struct");


            if (port[2] == isInput && !isStruct && !port[4]) {
                alert("Please select one In- and one Out-port!")
            } else if (port[0] == id) {
                alert("Please select two different components");
            } else if (alreadyConnected(port[0], port[1], id, p)) {
                alert("Two connections won't work better")
            } else if (isStruct && port[3] && alreadyConnected(id, p, port[0], port[1])) {
                alert("Structs are not able to provide two-way connections!");
            } else {
                console.log("iI", isInput || isStruct);
                if (isInput || isStruct) {
                    if (!("iP" in data[id]))
                        data[id].iP = newMap(data[id].iPN);

                    //Remove a possible set constant
                    if (data[id].iP[p] && data[id].iP[p].length > 0 && data[id].iP[p][0] == -1) {
                        data[id].iP[p] = [];
                        var p2 = document.querySelectorAll("g[data-id='" + id + "'] > g.inports :nth-child(" + (p + 1) + ")")[0];
                        //p2.setAttribute("title", getComponent(data[id].type).inports[p].title);
                        setTitle(p2, getComponent(data[id].type).inports[p].title);

                        p2.setAttribute("data-val", null);
                    }

                    data[id].iP[p].push([parseInt(port[0]), parseInt(port[1])]);
                    data[port[0]].oP[parseInt(port[1])].push([id, p]);
                    updateConLine(port[0], port[1], id, p);


                } else {
                    if (!("iP" in data[port[0]]))
                        data[port[0]].iP = newMap(data[port[0]].iPN);

                    //Remove a possible set constant
                    if (data[port[0]].iP[port[1]] && data[port[0]].iP[port[1]].length > 0 && data[port[0]].iP[port[1]][0] == -1) {
                        data[port[0]].iP[port[1]] = [];
                        var p2 = document.querySelectorAll("g[data-id='" + port[0] + "'] > g.inports :nth-child(" + (port[1] + 1) + ")")[0];
                        //p2.setAttribute("title", getComponent(data[port[0]].type).inports[port[1]].title);
                        setTitle(p2, getComponent(data[port[0]].type).inports[port[1]].title);

                        p2.setAttribute("data-val", null);
                    }


                    data[port[0]].iP[port[1]].push([id, p]);
                    data[id].oP[p].push([port[0], port[1]]);
                    updateConLine(id, p, port[0], port[1]);


                }
            }
            port = undefined;
        }
//                alert("P");
        return;
    } else if (hasClass(evt.target.parentNode, "component")) {
        selectedElement = evt.target.parentNode;
        currentX = evt.clientX;
        currentY = evt.clientY;
        currentMatrix = selectedElement.getAttributeNS(null, "transform").slice(7, -1).split(' ');

        for (var i = 0; i < currentMatrix.length; i++) {
            currentMatrix[i] = parseFloat(currentMatrix[i]);
        }

        selectedElement.setAttributeNS(null, "onmousemove", "moveElement(evt)");
        selectedElement.setAttributeNS(null, "onmouseout", "deselectElement(evt)");
        selectedElement.setAttributeNS(null, "onmouseup", "deselectElement(evt)");
    }
}

function deselectElement(evt) {
    if (selectedElement != 0) {
        selectedElement.removeAttributeNS(null, "onmousemove");
        selectedElement.removeAttributeNS(null, "onmouseout");
        selectedElement.removeAttributeNS(null, "onmouseup");
        selectedElement = 0;
    }
}

function moveElement(evt) {
    dx = evt.clientX - currentX;
    dy = evt.clientY - currentY;
    currentMatrix[4] += dx;
    currentMatrix[5] += dy;
    newMatrix = "matrix(" + currentMatrix.join(' ') + ")";

    selectedElement.setAttributeNS(null, "transform", newMatrix);
    currentX = evt.clientX;
    currentY = evt.clientY;


    //update oP: {0: [[1, 0]], 1: []}
    var me = getDID(selectedElement.getAttributeNS(null, "data-id"));
    me.x = currentMatrix[4];
    me.y = currentMatrix[5];
    if ("oP" in me) {
        for (var port in me.oP) {
            me.oP[port].forEach(function (d) {
                updateConLine(me.id, port, d[0], d[1]);
            });
        }
    }

    if ("iP" in me) {
        for (var port in me.iP) {
            me.iP[port].forEach(function (d) {
                if (d[0] >= 0) {
                    console.log(d[0], d[1], me.id, port);
                    updateConLine(d[0], d[1], me.id, port);
                }
            });
        }
    }

}

function updateConPreviewLine(x, y) {
    if (!port)return;
    var line = document.getElementById("con_preview");

    if (!x) {
        line.parentNode.removeChild(line);
        return;
    }


    if (!line) {
        line = document.createElementNS("http://www.w3.org/2000/svg", "line");
        line.setAttribute("id", "con_preview");
        line.setAttribute("stroke", "pink");
        line.setAttribute("style", "stroke:rgb(255,0,0);stroke-width:2");
        document.getElementById("isvg").appendChild(line);
    }
    var n1 = getNodePos(port[0], port[1], !port[2]);
    line.setAttribute("x1", n1.x);
    line.setAttribute("y1", n1.y);
    line.setAttribute("x2", x);
    line.setAttribute("y2", y);

}


function updateConLine(outputID, outputN, inputID, inputN) {
    var id = "con_" + outputID + "/" + outputN + "_" + inputID + "/" + inputN;
    var line = document.getElementById(id);


    if (!line) {
        line = document.createElementNS("http://www.w3.org/2000/svg", "line");
        line.setAttribute("id", id);
        line.setAttribute("stroke", "pink");
        line.setAttribute("style", "stroke:rgb(255,0,0);stroke-width:2");
        document.getElementById("isvg").appendFirst(line);
    }
    var n1 = getNodePos(outputID, outputN, true),
        n2 = getNodePos(inputID, inputN, false);

    if (!n2)console.log("input " + inputID + "//" + inputN);
    line.setAttribute("x1", n1.x);
    line.setAttribute("y1", n1.y);
    line.setAttribute("x2", n2.x);
    line.setAttribute("y2", n2.y);

}

function getNodePos(compID, nodeNr, isOutput) {

    var s = "[data-id='" + compID + "']>." + (isOutput ? "outports" : "inports") + ">circle:nth-child(" + (parseInt(nodeNr) + 1) + ")";
    var nodes = document.querySelectorAll(s);
    if (nodes.length == 0) {
        var c = data[compID];
        if (c.type == "struct")
            return {x: c.x, y: c.y};


        console.log(s);
        return undefined;
    }

    var bbox = nodes[0].getBBox(),
        middleX = bbox.x + (bbox.width / 2),
        middleY = bbox.y + (bbox.height / 2);


    // generate a conversion function
    var convert = makeAbsoluteContext(nodes[0], document.getElementsByTagName("svg")[0]);

    // use it to calculate the absolute center of the element
    var absoluteCenter = convert(middleX, middleY);
    return absoluteCenter;
}

