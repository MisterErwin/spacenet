/**
 * Created by alex on 11/10/15.
 */

//var data = [
//    {id: 0, x: 10, y: 10, text: "ONE", iPN: 1, oP: {0: [[1, 0]], 1: []}},
//    {id: 1, x: 110, y: 110, text: "TWO", iPN: 3, oP: {0: []}},
//];

//var data = [
//    {id: 0, x: 10, y: 10, type: "add", oP:{0:[[1,0]]}},
//    {id: 1, x: 210, y: 110, type: "sub", oP:{0:[]}}
//
//];

var data = [
    {id: 0, x: 10, y: 10, type: "kick", oP: {0: []}}
];
data = [{"id": 0, "x": 10, "y": 10, "type": "kick", "oP": {"0": [[1, 0]]}}, {
    "id": 1,
    "x": 295,
    "y": 148,
    "type": "add",
    "oP": {"0": [[3, 0]]},
    "iP": {"0": [], "1": [[-1, "1"]], "2": []}
}, {
    "id": 2,
    "x": 556,
    "y": 246,
    "type": "wait",
    "oP": {"0": [[1, 0]]},
    "iP": {"0": [[-1, "1000"]], "1": []}
}, {
    "id": 3,
    "x": 349,
    "y": 319,
    "type": "condition",
    "oP": {"0": [], "1": [[2, 1]]},
    "iP": {"0": [], "1": [[-1, ">="]], "2": [[-1, "42"]], "3": []}
}];

data = [{"id": 0, "x": 10, "y": 10, "type": "kick", "oP": {"0": [[5, 0]]}}, {
    "id": 1,
    "x": 1023,
    "y": 34,
    "type": "wait",
    "oP": {"0": [[6, 0], [2, 0]]},
    "iP": {
        "0": [[-1, "100"]],
        "1": [[3, 0], [4, 0], [3, 0], [4, 0], [3, 0], [4, 0], [3, 0], [4, 0], [3, 0], [4, 0], [3, 0], [4, 0], [3, 0], [4, 0]]
    }
}, {"id": 2, "x": 1284, "y": 93, "type": "color", "oP": {}, "iP": {"0": [[1, 0]]}}, {
    "id": 3,
    "x": 666,
    "y": 174,
    "type": "store",
    "oP": {"0": [[1, 1]]},
    "iP": {"0": [[-1, "1"]], "1": [[5, 1], [5, 1], [5, 1], [5, 1], [5, 1], [5, 1], [5, 1], [5, 1], [5, 1], [5, 1]]}
}, {
    "id": 4,
    "x": 667,
    "y": 58,
    "type": "add",
    "oP": {"0": [[1, 1]]},
    "iP": {"0": [[5, 0], [5, 0], [5, 0], [5, 0], [5, 0], [5, 0], [5, 0]], "1": [[-1, "1"]], "2": []}
}, {
    "id": 5,
    "x": 260,
    "y": 92,
    "type": "condition",
    "oP": {"0": [[4, 0]], "1": [[3, 1]]},
    "iP": {
        "0": [[0, 0], [0, 0], [7, 0], [0, 0], [7, 0], [0, 0], [7, 0], [0, 0], [7, 0], [0, 0], [7, 0], [0, 0], [7, 0]],
        "1": [[-1, "<"]],
        "2": [[-1, "12"]],
        "3": []
    }
}, {
    "id": 6,
    "x": 1179,
    "y": 395,
    "type": "struct",
    "oP": {"0": [[7, 0]]},
    "iP": {"0": [[1, 0], [1, 0], [1, 0], [1, 0]]}
}, {
    "id": 7,
    "x": 216,
    "y": 402,
    "type": "struct",
    "oP": {"0": [[5, 0]]},
    "iP": {"0": [[6, 0], [6, 0], [6, 0], [6, 0], [6, 0]]}
}];
/*
 data = [{"id": 0, "x": 10, "y": 10, "type": "add", "oP": {"0": [[1, 0]]}}, {
 "id": 1,
 "x": 210,
 "y": 110,
 "type": "sub",
 "oP": {"0": [[2, 0], [3, 1]]},
 "iP": {"0": [[0, 0]], "1": [], "2": []}
 }, {
 "id": 2,
 "x": 330,
 "y": 50,
 "type": "div",
 "oP": {"0": [[4, 0]]},
 "iP": {"0": [[1, 0]], "1": [], "2": []}
 }, {"id": 3, "x": 330, "y": 160, "type": "mult", "oP": {"0": []}, "iP": {"0": [], "1": [[1, 0]], "2": []}}, {
 "id": 4,
 "x": 645,
 "y": 75,
 "type": "wait",
 "oP": {"0": [[5, 2]]},
 "iP": {"0": [[2, 0]], "1": []}
 }, {
 "id": 5,
 "x": 782,
 "y": 134,
 "type": "condition",
 "oP": {"0": [], "1": []},
 "iP": {"0": [[-1, "1"]], "1": [[-1, "=="]], "2": [[4, 0]], "3": []}
 }];*/

function compactData(){
    var v = JSON.parse(JSON.stringify(data));//data.slice();
    v.forEach(function (c) {
        if ("iP" in c) {
            for (var p in c.iP) {
                for (var i = 0; i < c.iP[p].length; ++i)
                    if (c.iP[p][i][0] != -1)
                        c.iP[p].splice(i);
                if (c.iP[p].length == 0)
                    delete c.iP[p];
            }
            if (size(c.iP) == 0)
                delete c.iP;
        }
    });
    return v;
}

function exportP(name) {

    window.setTimeout(function () {
        var n = getQueryVariable("pname");
        if (n == "__new")n = undefined;
        if (n)
            name = n;
        else if (!name)
            name = prompt("Please enter a name");

        if (name == null)
            return;

        name=name.replace(/[^a-zA-Z0-9)]/g, "");

        window.localStorage[name] = JSON.stringify(data);
        if (getQueryVariable("pname") != name)
            window.location.href = updateUrlParameter(window.location.href, "pname", name);
        alert("Saved as " + name);

        var o = (window.localStorage.storedAIs) ? JSON.parse(window.localStorage.storedAIs) : {};
        o[name] = data;
        window.localStorage.storedAIs = JSON.stringify(o);

        //window.localStorage.prog = JSON.stringify(data);
        //if (!name)
        //    window.prompt("Copy to clipboard: Ctrl+C, Enter", JSON.stringify(data));
    }, 0);
}

function clearStage() {
    var myNode = document.getElementById("isvg");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
    data = undefined;
}

function importD(str) {
    clearStage();
    data = JSON.parse(str);
    build();
}

//function importLS() {
//    importD(window.localStorage.prog)
//}


function setTitle(svgE, title) {
    var t = svgE.getElementsByTagName("title")[0];
    if (t)
        t.innerHTML = title;
    else {
        t = document.createElementNS(NSN, "title");
        svgE.appendChild(t);
        t.innerHTML = title;
    }
}

var port = undefined;

var NSN = "http://www.w3.org/2000/svg";


//function getNextFreeID(id){
//    if (id == undefined)id=0;
//    for (var i=0;i<data.length;++i)
//        if (data[i].id == id )
//        return getNextFreeID(id + 1);
//    return id;
//}


function showSelectPrompt(values, val, callback) {
    console.log("showSelectPrompt", values, val);
    selectPromptCallback = callback;
    var sel = document.getElementById("selectPromptIn");
    sel.options.length = 1;
    var n = 0;
    for (var i = 0; i < values.length; ++i) {
        if (values[i] == val) {
            n = sel.options.length;
        }
        sel.options[sel.options.length] = new Option(values[i]);
    }
    document.getElementById("selectPrompt").style.visibility = "visible";
    sel.selectedIndex = n;

}

var selectPromptCallback;

function selectPrompt(val) {
    //var sel = document.getElementById("selectPromptIn");
    document.getElementById("selectPrompt").style.visibility = "hidden";
    if (val != "###" && selectPromptCallback)
        selectPromptCallback(val);
}


function newArray(size, def) {
    var a = [];
    for (var i = 0; i < size; ++i)
        a.push(def);
    return a;
}

function newMap(size) {
    var a = {};
    for (var i = 0; i < size; ++i)
        a[i] = [];
    return a;
}

function findDataI(array, n1, n2) {
    for (var i = 0; i < array.length; ++i)
        if (array[i][0] == n1 && array[i][1] == n2)
            return i;
    return -1;
}

function hasClass(element, cls) {
    return (' ' + ("baseVal" in element.className ? element.className.baseVal : element.className) + ' ').indexOf(' ' + cls + ' ') > -1;
}

function size(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function makeAbsoluteContext(element, svgDocument) {
    return function (x, y) {
        var offset = svgDocument.getBoundingClientRect();
        var matrix = element.getScreenCTM();
        return {
            x: (matrix.a * x) + (matrix.c * y) + matrix.e - offset.left,
            y: (matrix.b * x) + (matrix.d * y) + matrix.f - offset.top
        };
    };
}


function getDID(id) {
    for (var i = 0; i < data.length; ++i)
        if (data[i].id == id)
            return data[i];
    return undefined;
}

window.onmouseup = function (evt) {
    var selectObj = window.getSelection();

    // works in FF3, Safari, Opera, and Chrome
    try {
        selectObj.collapseToStart();
    } catch (e) {
        console.log(e);
    }
    // works in FF3, Safari, Chrome (but not opera)
    /* selectObj.collapse(document.body, 0); */

    // and as the code is native, I have no idea why...
    // ...and it makes me sad
}


HTMLElement.prototype.appendFirst = function (childNode) {
    if (this.firstChild)this.insertBefore(childNode, this.firstChild);
    else this.appendChild(childNode);
};

SVGElement.prototype.appendFirst = function (childNode) {
    if (this.firstChild)this.insertBefore(childNode, this.firstChild);
    else this.appendChild(childNode);
};

