/**
 * Created by alex on 11/1/15.
 */
var game, game_onMSG, rE, client, drawCamera, tooltip, tooltip2, tooltip3, lockedUUID = undefined, controlling = undefined;

var colors = [0xFFFFFF, 0xFF0000, 0x800000, 0xFFFF00, 0x808000, 0x00FF00, 0x008000, 0x00FFFF, 0x008080, 0x0000FF, 0x000080, 0xFF00FF, 0x800080, 0x0c0c0c];

var sX = 0, sY = 0;
var rX = 400, rY = 300, lX = 0, lY = 0;


function getV2(v) {
    return v.split(",");
}

window.onload = function () {
    var bulletR;
    //document.getElementById("nameI").value = getHashCode(navigator.userAgent);

    game = new Phaser.Game(800, 600, Phaser.AUTO, '', {preload: preload, create: create, update: update});

    function preload() {

        game.load.image('logo', 'images/phaser.png');
        game.load.image('bulletR', 'images/enemy-bullet.png');
        game.load.image('player', 'images/player.png');

    }

    function create() {

        this.keyLeft = game.input.keyboard.addKey(Phaser.KeyCode.LEFT);
        this.keyRight = game.input.keyboard.addKey(Phaser.KeyCode.RIGHT);
        this.keyUp = game.input.keyboard.addKey(Phaser.KeyCode.UP);
        this.keyDown = game.input.keyboard.addKey(Phaser.KeyCode.DOWN);


        game.lockRender = false;
        game.canvas.oncontextmenu = function (e) {
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
        rE = {};

        //window.setTimeout(doConnect, 10000);
        doConnect();
    }

    function update() {
        if (controlling != undefined){
            var o = {type: "keycontrol", uuid: controlling, up: this.keyUp.isDown, down: this.keyDown.isDown, left: this.keyLeft.isDown, right: this.keyRight.isDown };

            client.ws.send(JSON.stringify(o))
        }
    }

    function clickOnGameObject(event, sprite, c, d) {
        delGameTooltip();
        var render = this.d_render;
        switch (render) {
           /* case "drone":
                if (this.owner == client.username) {
                } else {

                }
                askForObjectInfo(this.d_uuid, sprite.leftButton.isDown, sprite.rightButton.isDown, sprite.leftButton.ctrlKey, sprite.leftButton.shiftKey)
                //if (sprite.leftButton.isDown) {
                //    if (sprite.leftButton.ctrlKey) {
                //        client.ws.send("{type:'shot', uuid:'" + this.d_uuid + "'}");
                //    } else {
                //        client.ws.send("{type:'watch', uuid:'" + this.d_uuid + "'}");
                //    }
                //} else if (sprite.rightButton.isDown) {
                //    //Force a color change
                //    client.ws.send("{type:'color', uuid:'" + this.d_uuid + "'}");
                //}
                break;
            case "base":
                askForObjectInfo(this.d_uuid, sprite.leftButton.isDown, sprite.rightButton.isDown, sprite.leftButton.ctrlKey, sprite.leftButton.shiftKey)

                break;*/
            default :
                askForObjectInfo(this.d_uuid, sprite.leftButton.isDown, sprite.rightButton.isDown, sprite.leftButton.ctrlKey, sprite.leftButton.shiftKey)
                break;

        }
    }

    function pocesscontrol(uuid){
        controlling = uuid;
    }

    function view(uuid, ndtry) {

        var o = getGameObject(uuid);
        if (o == null) {
            if (ndtry) {
                var o = {type: "getspawninfo", uuid: uuid};
                client.ws.send(JSON.stringify(o));
                //window.setTimeout(function(){
                //    window.location.reload();
                //},5000);
                //alert(uuid + " is not spawned")
                //throw ("2nd try failed:" + uuid);
            }
            window.setTimeout(function () {
                view(uuid, true);
            }, 1000);
            return;
        }
        lX = o.d_pos[0] * -1;
        lY = o.d_pos[1] * -1;
        lockedUUID = uuid;
        //console.log(uuid, "Now viewing", lX, lY);

        updateAll()
    }

    function getGameObject(uuid) {
        for (var i = 0; i < game.world.children.length; ++i)
            if (uuid = game.world.children[i].d_uuid)
                return game.world.children[i];
        return undefined;
    }

    function askForObjectInfo(uuid, leftButton, rightButton, ctrl, shift) {
        var o = {type: 'objectinfo', uuid: uuid};
        if (leftButton)
            o.leftButton = true;
        if (rightButton)
            o.rightButton = true;
        if (ctrl)
            o.ctrl = true;
        if (shift)
            o.shift = true;
        client.ws.send(JSON.stringify(o));
    }

    function showGameTooltip(event, sprite) {
        delGameTooltip();
        tooltip = this.game.add.text(sprite.x, sprite.y, this.d_uuid, {font: "15px Arial", fill: "#ffffff"});
        tooltip2 = this.game.add.text(sprite.x, sprite.y + 10, "Mass:" + this.d_mass, {
            font: "15px Arial",
            fill: "#00ffff"
        });
        if (this.owner)
            tooltip3 = this.game.add.text(sprite.x, sprite.y + 20, "Owner:" + this.owner, {
                font: "15px Arial",
                fill: (this.ownerCol) ? this.ownerCol : "#00ffff"
            });
    }

    function delGameTooltip() {
        if (tooltip != undefined) {
            tooltip.destroy();
        }
        if (tooltip2 != undefined) {
            tooltip2.destroy();
        }
        if (tooltip3 != undefined) {
            tooltip3.destroy();
        }
    }

    drawCamera = function drawCamera() {
        this.cameraG = game.add.graphics(0, 500);
        this.cameraG.beginFill(0xFFFF00);
        this.cameraG.drawRect(0, 0, 100, 100);
    }

    game_onMSG = function (e) {
        switch (e.type) {
            case "openbasecontrolmodal":
                $("#base_res").html(e.resources);
                $("#base_res_max").html(e.resourcesMax);
                $("#base_spawnedInWorld").html(e.spawnedInWorld);
                $("#base_inHangar").html(e.spawnedInBase);
                $("#base_chunkX").html(e.chunkX);
                $("#base_chunkY").html(e.chunkY);


                $("#base-control").modal();
                return true;
            case "startviewing":
                view(e.uuid);
                return true;
            case "startplayercontrol":
                pocesscontrol(e.uuid);
                return true;
            case "all":
                var pos = getV2(e.pos);
                pos[0] = parseFloat(pos[0]);
                pos[1] = parseFloat(pos[1]);

                if (e.uuid in rE) {
                    var n = rE[e["uuid"]];
                    n.d_pos = pos;
                    n.x = sX - pos[0] - lX + rX - n.d_OX;
                    n.y = sY - pos[1] - lY + rY - n.d_OY;
                    return true;
                }
                var graphics = new Phaser.Graphics();//game.add.graphics(sX - pos[0] + rX, sY - pos[1] + rY);

                var OX = 0, OY = 0;


                var t = undefined;

                //console.log(e.rendershape, e.rendersize, e.color, e.rendertexture);

                if (e.rendershape == 0) {
                    //CIRCLE
                    graphics.lineStyle(0);
                    graphics.beginFill(("color" in e) ? e.color : 0xFFFF0B, 0.5);
                    graphics.drawCircle(0, 0, e.rendersize);
                    OX = OY = e.rendersize / 2;
                    graphics.endFill();
                } else if (e.rendershape == 1) {
                    graphics.lineStyle(0);
                    graphics.beginFill(("color" in e) ? e.color : 0xFFFF0B, 0.7);
                    graphics.drawRect(0, 0, e.rendersize, e.rendersize);
                    OX = OY = e.rendersize / 2;
                    graphics.endFill();
                } else if (e.rendershape == 2) {
                    //Texture
                    t = e.rendertexture;
                    OX = OY = e.rendersize / 2;

                } else {
                    console.log("rendershape: ", e.rendershape)
                }
                /*
                 switch (e.render) {
                 case "star":
                 //t = "player";
                 graphics.lineStyle(0);
                 graphics.beginFill(("color" in e) ? (e.color in colors ? colors[e.color] : 0x808080) : 0xFFFF0B, 0.5);
                 graphics.drawCircle(0, 0, 15);
                 OX = OY = 15 / 2;
                 graphics.endFill();
                 break;
                 case "drone":
                 //t = "player";
                 graphics.lineStyle(0);
                 graphics.beginFill(("color" in e) ? (e.color in colors ? colors[e.color] : 0x808080) : 0xFFFF0B, 0.5);
                 graphics.drawCircle(0, 0, 15);
                 OX = OY = 15 / 2;
                 graphics.endFill();
                 break;
                 case "tv":
                 graphics.lineStyle(0);
                 graphics.beginFill(("color" in e) ? (e.color in colors ? colors[e.color] : 0x808080) : 0xFFFF0B, 0.2);
                 graphics.drawCircle(0, 0, 15);
                 OX = OY = 15 / 2;
                 graphics.endFill();
                 break;
                 case "base":
                 graphics.lineStyle(0);
                 graphics.beginFill(("color" in e) ? (e.color in colors ? colors[e.color] : 0x808080) : 0xFFFF0B, 0.7);
                 graphics.drawRect(0, 0, 10, 10);
                 OX = OY = 10 / 2;
                 graphics.endFill();
                 break;
                 case "bullet":
                 t = "bulletR";
                 //
                 //                        graphics.lineStyle(0);
                 //                        graphics.beginFill(0xFF0000, 1);
                 ////                            OX=OY=1/2;
                 //                        graphics.drawCircle(0, 0, 5);
                 //                        graphics.endFill();
                 break;
                 default:
                 graphics.lineStyle(0);
                 graphics.beginFill(("color" in e) ? (e.color in colors ? colors[e.color] : 0x808080) : 0x0BFFFF, 1);
                 graphics.drawRect(0, 0, 5, 5);
                 OX = OY = 5 / 2;
                 graphics.endFill();
                 break;
                 }*/
                var sprite;
                //if (e.render == "bullet"){
                //    sprite = game.add.sprite(sX - pos[0] - lX + rX - OX, sY - pos[1] - lY + rY - OY, "bulletR")
                //
                //}else
                sprite = game.add.sprite(sX - pos[0] - lX + rX - OX, sY - pos[1] - lY + rY - OY, (t == undefined) ? graphics.generateTexture() : t);
                sprite.inputEnabled = true;
                sprite.input.useHandCursor = true;

                sprite.events.onInputDown.add(clickOnGameObject, sprite);
                sprite.events.onInputOver.add(showGameTooltip, sprite);
                sprite.events.onInputOut.add(delGameTooltip, sprite);


                sprite.g = graphics;
                sprite["d_render"] = e.render;
                sprite["d_uuid"] = e.uuid;
                sprite["d_mass"] = e.mass;
                sprite["d_OX"] = OX;
                sprite["d_OY"] = OY;
                sprite["d_pos"] = pos;
                sprite["owner"] = e.owner;
                sprite["ownerCol"] = e.ownerCol;


                rE[e["uuid"]] = sprite;
                //console.log("Added entitiy ", e.uuid, e.render);

                sprite.x = sX - pos[0] - lX + rX - sprite.d_OX;
                sprite.y = sY - pos[1] - lY + rY - sprite.d_OY;

                updateAll();
                return true;
            case "pos":
                if (!(e.uuid in rE)) {
                    //console.log("unknown entity: ", e.uuid);
                    return true;
                }
                pos = getV2(e.pos);
                n = rE[e["uuid"]];

                if (lockedUUID == e.uuid) {
                    lX = pos[0] * -1;
                    lY = pos[1] * -1;
                    updateAll();
                }

                n.x = sX - pos[0] - lX + rX - n.d_OX;
                n.y = sY - pos[1] - lY + rY - n.d_OY;
                n["d_pos"] = pos;
                n["d_mass"] = e.mass;
                n["owner"] = e.owner;
                n["ownerCol"] = e.ownerCol;

                return true;

            case "color":
                if (!(e.uuid in rE))break;

                var gg = rE[e["uuid"]].g;
                gg.clear();
                gg.lineStyle(0);
                gg.beginFill(("color" in e) ? e.color : 0xFFFF0B, 0.5);
                gg.drawCircle(0, 0, 15);
                gg.endFill();
                rE[e["uuid"]].setTexture(gg.generateTexture());
                return true;
            case "remove":
                if (!(e.uuid in rE))
                    return true;
                if (e.uuid == lockedUUID) {
                    lockedUUID = undefined;
                }


                rE[e["uuid"]].destroy();
                delete rE[e["uuid"]];
                return true;
            case "watch_cam":
                if (this.cameraG == undefined)
                    drawCamera();
                this.cameraG.beginFill(0xC0C0C0);
                this.cameraG.drawRect(0, 0, 50, 100);
                var l = e.ccolorData.length, s = 100 / (l + 1);

                for (var i = 0; i < l; i++) {
                    var c = e.ccolorData[i];
                    this.cameraG.beginFill(c in colors ? colors[c] : 0x808080);
                    this.cameraG.drawRect(i * s, 0, i * s + s, 50);
                }

                l = e.cdata.length, s = 100 / (l + 1);
                var max = Math.max.apply(Math, e.cdata);
                this.cameraG.beginFill(0xFF0cFF);

                for (var i = 0; i < l; i++) {
                    if (e.cdata[i] == 0) {
                        this.cameraG.beginFill(0xC0C0C0);
                        this.cameraG.drawRect(i * s, 50, i * s + s, 50);
                    } else {
                        this.cameraG.beginFill(0x000000);
                        this.cameraG.drawRect(i * s, 100 - 50 * e.cdata[i] / 10000, i * s + s, 50);
                    }
                }

                return true;
        }
        return false;
    }


};

function updateAll() {
    for (var i = 0; i < game.world.children.length; ++i) {
        var e = game.world.children[i];
        if (/*lockedUUID == e.d_uuid ||*/ !e.d_pos)continue;
        e.x = sX - e.d_pos[0] - lX + rX - e.d_OX;
        e.y = sY - e.d_pos[1] - lY + rY - e.d_OY;
    }
}