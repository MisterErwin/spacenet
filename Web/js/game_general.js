/**
 * Created by alex on 11/1/15.
 */
var doClose = false;


window.onbeforeunload = function (e) {
    doClose = true;
    console.log("beforeunload");
};

window.oncontextmenu = function (e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
}


$(function () {
    moveProgressBar($('.progress-wrap'));
    // on browser resize...
    $(window).resize(function () {
        moveProgressBar($('.progress-wrap'));
    });
});


function getHashCode(s) {
    var hash = 0, c = (typeof s == 'string') ? s.length : 0, i = 0;
    while (i < c) {
        hash = ((hash << 5) - hash) + s.charCodeAt(i++);
    }
    return ( hash < 0 ) ? ((hash * -1) + 0xFFFFFFFF) : hash; // convert to unsigned
}


function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
    return undefined;
}

function sendChatMSG() {
    client.ws.send('{"type":"chat", "text":"' + document.getElementById("chatI").value.replace(/"/g, "").replace(/'/g, "") + '", "user":\"' + client.username.replace(/"/g, "").replace(/'/g, "") + '"}');
    document.getElementById("chatI").value = "";
}


// SIGNATURE PROGRESS
function moveProgressBar(wrap, time) {
    var getPercent = (wrap.data('progress-percent') / 100);
    var getProgressWrapWidth = wrap.width();
    var progressTotal = getPercent * getProgressWrapWidth;
    var animationLength = time == undefined ? 2500 : time;


    // on page load, animate percentage bar to data percentage length
    // .stop() used to prevent animation queueing
    $('.progress-bar', wrap).stop().animate({
            left: progressTotal
        }, animationLength, "swing",
        function () {
            $('.progress-bar', wrap).css("left", 0);
            var d = wrap.data("progress-finish");
            if (d) {
                var fn = window[d];
                if (typeof fn === 'function') {
                    fn();
                }
            }

        }
    )
    ;
}

var url = undefined;

function doConnect() {
    client = new Client();
    url = getQueryVariable("server");
    if (url == undefined)
        url = "localhost:8123";
    client.openConnection("ws://" + url);

    document.getElementById("openAIeditorlink").setAttribute("href", "graphdev2.html?pname=__new&is=false&server=" + url)
}

//DEBUG: remove this
function showAIs() {
    reloadStoredAIs();
    $("#ai-select").modal();
}


function Client() {

}

Client.prototype.openConnection = function (url) {
    this.ws = new WebSocket(url);
    this.connected = false;
    this.ws.onmessage = this.onMessage.bind(this);
    this.ws.onerror = this.displayError.bind(this);
    this.ws.onclose = this.displayClose.bind(this);

    this.ws.onopen = this.connectionOpen.bind(this);
    this.username = undefined;
};

Client.prototype.connectionOpen = function () {
    this.connected = true;
    console.log("connected");
};

Client.prototype.onMessage = function (message) {
    try {
        var data = JSON.parse(message.data);
        if (!game_onMSG(data))
            if (!schematic_onMSG(data)) {
                switch (data.type) {
                    case "authentification":
                        this.username = data.name;
                        document.getElementById("nameI").value = this.username;
                        console.log("authentification: success [" + this.username+"]")
                        break;
                    case "chat":
                        //TODO:
                        break;
                    case "setbaseprogram":
                        var select = $("#aiselect");
                        select.val(data.pname);
                        select.trigger("chosen:updated");
                        $("#base_autobuild").prop("checked", data.autobuild)
                        break;
                    case "spawnreply":
                        var mod = $("#mod");
                        if (data.status == "ok") {
                            mod.html("Successfully spawned drone<br/> Click <a href='#openDebug' onclick=\"window.open('graphdev2.html?is=true&server=" + url + "&watch=" + data.uuid + "','_blank','directories=0,location=0,status=0,titlebar=0');return false;\">here</a> to debug the program");
                        } else
                            mod.html("Error: " + data.status);

                        mod.modal();
                        break;
                    case "error":
                        if (data.authentification){
                            if (data.authentification == "missing"){
                                var mod = $("#mod");
                                mod.html("Authentification required").modal();
                                doClose=true;
                                window.location.href = "login.html?server="+url;
                            }
                            return true;
                        }
                        if (data.close) {
                            alert(data.msg);
                            var mod = $("#mod");
                            mod.html(data.msg).modal();
                            window.close();
                        }else{
                            var mod = $("#mod");
                            mod.html(data.msg).modal();
                        }
                        break;
                    default:
                        console.log("not handled", data.type, data);

                }
            }
    } catch (e) {
        console.log(message.data);
        console.log(e);
    }
};

Client.prototype.displayError = function (err) {
    console.log('Websocketerror: ' + err);
    var mod = $("#mod");
    mod.html("An error occured </hr> Try to reload the page");
    mod.modal();
    if (!doClose)
        window.location.href = "index.php?websocketerror";
};

Client.prototype.displayClose = function (err) {
    console.log(err);

    if (!doClose){
        var mod = $("#mod");
        mod.html("Lost connection! <hr/> Please reload!");
        mod.modal();
        console.log("forward");
        window.location.replace("index.php?websocketclose");
        window.setTimeout(function(){
            window.location.reload();
        },10000);
    }
};

function openDebugByUUID(uuid){
    console.log("openDebugByUUID", uuid);
    window.open('graphdev2.html?watch='+uuid+'&is=true&server=' + url ,'_blank','directories=0,location=0,status=0,titlebar=0');
    return false;
}

//DEBUG: Allows offline modus
//doClose = true;