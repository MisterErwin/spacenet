/**
 * Created by alex on 11/1/15.
 */
var partList, partListInfo;

var currentSchematic = {maxSize: -1, parts: {MK_1_case: 'CASE'}};


$(function () {
    $("#drone-builder-comp-type").find("div[data-type]").click(function () {
        var newtype = $(this).data("type");
        setPartList(newtype);
    });

    $(".dronePartSelContainer").on("click", ".box:data(pname)", function () {
        var name = $(this).data("pname");
        if (name in currentSchematic.parts) {
            $(this).removeClass("selected");
            delete currentSchematic["parts"][name];
        } else {
            var type = $(this).data("ptype");
            if (type == 'CASE' || type == 'THRUSTER' || type == 'WEAPON' || type == 'CAMERA' || type == 'WAYPOINT' || type == 'BRAIN') {
                $.each(currentSchematic.parts, function (n2, t2) {
                    if (t2 == type) {
                        $(".dronePartSelContainer .box").filterByData('pname', n2).removeClass("selected");
                        delete currentSchematic["parts"][n2];
                    }
                });
            }

            $(this).addClass("selected");
            currentSchematic.parts[name] = type;
        }
        updatePartData();
    });

    //DEBUG: load them later on
    setPartList("CASE");
});

function makeSendSchematicObject() {
    var ret = {};
    for (var name in currentSchematic.parts) {
        var type = currentSchematic.parts[name];
        if (type in ret)
            ret[type].push(name);
        else
            ret[type] = [name];
    }
    return ret;
}

function makeFromSchematicObject(o) {
    var ret = {parts: {}, maxSize: -1};
    for (var type in o) {
        var a = o[type];
        for (var i = 0; i < a.length; ++i)
            ret.parts[a[i]] = type;
    }
    return ret;
}

function schematic_onMSG(e) {
    if (e.type == "partlist") {
        partList = e.parts;
        setPartList("CASE");
        return true;
    } else if (e.type == "setschematic") {
        currentSchematic = makeFromSchematicObject(JSON.parse(e.schematic));
        setPartList("CASE");
        console.log("currentSchematic set ", currentSchematic);
        updatePartData();
        return true;
    }
    return false;
}


function setPartList(type) {
    var container = $(".dronePartSelContainer");
    container.empty();
    if (!(type in partList))
        return;
    if (type in partListInfo)
        container.prepend(partListInfo[type]);

    $.each(partList[type], function (name, part) {
        var box = $("<div/>", {class: name in currentSchematic.parts ? 'box selected' : 'box'});
        box.data("pname", name).data("ptype", type);
        //$("<img/>", {src: 'images/120x100.png'}).prependTo(box);
        $("<img/>", {src: 'images/120x100.png'}).prependTo(box);

        box.append("<p>" + part.description + "</p>");

        //$("<p/>", {html: part.description}).appendTo(box);
        box.appendTo(container);

    });
}

function updatePartData() {
    var mass = sSize = maxSize = cost = time = health = 0;
    $.each(currentSchematic.parts, function (name, type) {
        if (!partList[type]) {
            console.log(type, "not found");
            return true;
        }
        var part = partList[type][name];
        if (type == 'CASE')
            maxSize = part.newSize;
        if ("health" in part)
            health += part.health;
        if ("mass" in part)
            mass += part.mass;
        if ("size" in part)
            sSize += part.size;
        if ("cost" in part)
            cost += part.cost;
        if ("time" in part)
            time += part.time;
    });
    $("#drone-builder-currentMass").text(mass);
    $("#drone-builder-currentSize").text(sSize);
    $("#drone-builder-maxSize").text(maxSize);
    $("#drone-builder-currentCost").text(cost);
    $("#drone-builder-currentTime").text(time + " time units");

    if (sSize > maxSize) {
        $("#drone-builder-save").prop("disabled", true);
        $("#drone-builder-currentSize").css("color", "red");
    } else {
        $("#drone-builder-save").prop("disabled", false);
        $("#drone-builder-currentSize").css("color", "black");
    }

}

function saveDroneSchematic() {
    var o = {type: "setschematic", schematic: makeSendSchematicObject()};
    client.ws.send(JSON.stringify(o));
}


function saveDroneAs(name) {
    if (name == undefined || name == null)return;
    if (name == "§_sel_§") {
        saveDroneAs(window.prompt('Please enter a name for your drone schematic'));
        return;
    }
    reloadStoredSchematics();
    $("#base-control").modal();
}


partListInfo = {
    "CASE": "<div class='box'><img src='images/120x100.png' title='Chassis: &#10; They provide the space for other components'><p><b>Name</b>" +
    "<hr/><b>Size:</b><hr/><b>Costs:</b><hr/><b>Health:</b><hr/>Construction time:</p></div>",
    "THRUSTER": "<div class='box'><img src='images/p_thruster.jpg' title='Thrusters: &#10; They are the engine of your ship'><p><b>Name</b>" +
    "<hr/><b>Size:</b><hr/><b>Costs:</b><hr/><b>Thrust:</b><hr/>Construction time:</p></div>",
    "TV_STATION": "<div class='box'><img src='images/120x100.png' title='TV Station: &#10; They allow you to view nearby objects'><p><b>Name</b>" +
    "<hr/><b>Size:</b><hr/><b>Costs:</b><hr/><b>Range:</b><hr/>Construction time:</p></div>",

    "BRAIN": "<div class='box'><img src='images/120x100.png' title='Computers: &#10; They are the brain of your ship'><p><b>Name</b>" +
    "<hr/><b>Size:</b><hr/><b>Costs:</b><hr/><b>NodeDelay:</b><hr/><b>Running:</b><hr/><b>Nodes:</b><hr/>Construction time:</p></div>"

};

//DEBUG: As there is no loading
partList = {
    "CASE": {
        "MK_1_case": {
            title: "MK 1",
            description: "<b>MK 1</b><hr/>30<hr/>-<hr/>100<hr/>120 tu",
            mass: 0,
            size: 0,
            cost: 0,
            time: 60,
            newSize: 30,
            newHealth: 100
        },
        "MK_1_LIGHT_case": {
            title: "Light MK 1",
            description: "<b>Light MK 1</b><hr/>20<hr/>-<hr/>30<hr/>10 tu",
            mass: 0,
            size: 0,
            cost: 0,
            time: 10,
            newSize: 20,
            newHealth: 30
        },
        "MK_1_HEAVY_case": {title: "Heavy MK 1", description: "todo33", size: 0, newSize: 20}
    },
    "THRUSTER": {
        MK_1_thruster: {title: "MK 1", description: "a thruster", size: 5},
        MK_V_thruster: {title: "MK V", description: "a thruster", size: 100, cost: 100, time: 680}
    },
    "BRAIN": {
        MK_1_NodeAI: {
            title: "MK 1 Node",
            description: "<b>MK 1 NodeAI</b><hr/>5<hr/>5<hr/>10<hr/>100ms<hr/>10tu",
            size: 5,
            cost: 5,
            time: 10
        },
        manualAI: {
            title: "ManualController",
            description: "<b>Manual</b><hr/>50<hr/>150<hr/>-<hr/>-<hr/>100tu",
            size: 50,
            cost: 150,
            time: 10
        }

    }
};