/**
 * Created by alex on 11/14/15.
 */
var client;

var isSpawned, isWatching=false;

function doConnect() {
    var isAlreadySpawned = getQueryVariable("is"),
        name = getQueryVariable("pname"),
        watch = getQueryVariable("watch");


    if (watch){
        console.log("WATCHING of " + watch)
        isWatching = true;
        buildOnConnect = undefined;
        var o = {type: "askforprogram", uuid: watch};
        queue.push(JSON.stringify(o));
        client = new Client();
        var url = getQueryVariable("server");
        client.openConnection("ws://" + (url == undefined ? "localhost:8123" : url) + "/program");

        return;
    }

    if (window.localStorage.savedBeforeReload) {
        name = window.localStorage.savedName;
        data = JSON.parse(window.localStorage.savedBeforeReload);

        buildOnConnect = true;
        window.localStorage.savedBeforeReload = undefined;
        delete window.localStorage.savedBeforeReload;
    } else {

        if (name) {
            if (name == "__new") {
                data = JSON.parse('[{"id":0,"x":382,"y":121,"type":"kick","oP":{"0":[]},"iP":{}}]');
            } else {
                var o = localStorage.getItem("storedAIs");
                if (o === null || !name in (o = JSON.parse(o))) {
                    alert("Could not find " + name);
                    window.location.href = updateUrlParameter(window.location.href, "pname", "__new");
                    return;
                }
                data = o[name];
                buildOnConnect = true;
                isSpawned = isAlreadySpawned;
            }
        }

    }

    client = new Client();
    var url = getQueryVariable("server");
    client.openConnection("ws://" + (url == undefined ? "localhost:8123" : url) + "/program");

    //if (watch){
    //    isWatching = true;
    //    var o = {type: "askfordebug", eid: getQueryVariable("uuid")};
    //    queue.push(JSON.stringify(o));
    //}

}

function runP() {
    if (!isSpawned) {
        sendToWS("spawndrone");
        isSpawned = true;
    } else
        sendToWS("updateprogram");

}
/*
 function exportToWS() {
 if (!client || !client.connected) {
 exportOnConnect = true;
 doConnect();
 return;
 }
 var o = {type: "program", ptype: "start", program: -1};
 var v = JSON.parse(JSON.stringify(data));//data.slice();
 v.forEach(function (c) {
 if ("iP" in c) {
 for (var p in c.iP) {
 for (var i = 0; i < c.iP[p].length; ++i)
 if (c.iP[p][i][0] != -1)
 c.iP[p].splice(i);
 if (c.iP[p].length == 0)
 delete c.iP[p];
 }
 if (size(c.iP) == 0)
 delete c.iP;
 }
 delete c.x;
 delete c.y;
 });
 o.program = JSON.stringify(v);
 client.ws.send(JSON.stringify(o));
 }*/

function killD() {
    if (!client || !client.connected || !isSpawned) {
        alert("Could not despawn");
        return;
    }
    var o = {type: "killDrone"};
    client.ws.send(JSON.stringify(o));
    if (isWatching){
        alert("Drone killed \n Closing debugger");
        window.close();
    }
}

function sendToWS(key) {
    if (!client || !client.connected) {
        exportOnConnect = true;
        doConnect();
        return;
    }
    var o = {type: key, program: -1};
    var v = compactData();
    o.program = JSON.stringify(v);
    o.pname = getQueryVariable("pname");
    o.watch = true;
    client.ws.send(JSON.stringify(o));
}

function handleData(wsdata) {
    switch (wsdata.type) {
        case 'setprogram':
            console.log(wsdata.program);
            data = wsdata.program;
            isSpawned = true;
            window.setTimeout(build, 100);
            var o = {type: "askfordebug", uuid: getQueryVariable("watch")};
            client.ws.send(JSON.stringify(o));
            break;
        case 'qupdate':
            var e = setIP(wsdata.id2, wsdata.port2, wsdata.val);
            var p1 = getNodePos(wsdata.id1, wsdata.port1, true),
                p2 = getNodePos(wsdata.id2, wsdata.port2, false);

            startInterpol(e, p1.x, p1.y, p2.x, p2.y, parseInt(wsdata.del) / 100);
            break;
        case 'status':
            console.log(wsdata.msg);
            break;
        case 'error':
            alert(wsdata.msg);
            if (wsdata.close)
                window.close();
            break;
        case 'setP': //Set Port
            setIP(wsdata.ID, wsdata.port, wsdata.val)
            break;
        case 'clear': //Set Port
            for (var port in wsdata.ports)
                setIP(wsdata.ID, parseInt(port), undefined);
            break;
        case 'set':
            for (var key in wsdata.IP) {
                setIP(wsdata.ID, key, wsdata.IP[key])
            }
            break;
        case 'listcomponents':
            allComponents.children = wsdata.c;
            if (buildOnConnect)
                window.setTimeout(build, 100);
            buildOnConnect = undefined;

            break;
        case 'compinfo':
            alert(wsdata.info);
            break;
        case 'reload': //client.ws.send("{type: 'reload'}")
            window.localStorage.savedBeforeReload = JSON.stringify(data);
            alert("A change was uploaded to the webserver \n Going to reload!");
            window.location.reload();
            return;
        default:
            console.log(wsdata.type);

    }
}

function showInfo(type) {
    if (!client.connected)
        alert("A connection to the server is required \n :(")
    else
        client.ws.send('{"type":"program", "ptype":"getcompinfo", "com":"' + type + '"}');
}

function getComponents() {
    client.ws.send('{"type":"program", "ptype":"getcomponents"}');
}

function setIP(node, port, value) {
    var pos = getNodePos(node, port, false);

    var ip = document.getElementById("IP_" + node + "/" + port);
    if (value == undefined || value == null) {
        console.log("REM");
        if (ip)
            ip.parentNode.removeChild(ip);
        else
            console.log("not found");
        return;
    }
    if (!ip) {
        ip = document.createElementNS(NSN, "text");
        ip.setAttributeNS(null, "id", "IP_" + node + "/" + port);
        ip.appendChild(document.createTextNode(value));
        document.getElementById("isvg").appendChild(ip);
    }
    ip.innerHTML = value;
    ip.setAttributeNS(null, "transform", "translate(" + (pos.x + 5) + "," + (pos.y + 5) + ")");
    return ip;
}

function startInterpol(elem, x1, y1, x2, y2, steps) {
    elem.setAttributeNS(null, "interpol", "1");
    elem.setAttributeNS("interpol", "x1", x1);
    elem.setAttributeNS("interpol", "y1", y1);
    elem.setAttributeNS("interpol", "x2", x2);
    elem.setAttributeNS("interpol", "y2", y2);
    elem.setAttributeNS("interpol", "s", 0);
    elem.setAttributeNS("interpol", "steps", steps);

}

function interpolateText() {
    var n = document.querySelectorAll("[interpol='1']");
    for (var i = 0; i < n.length; ++i) {
        var elem = n[i];
        //console.log(i);
        var x1 = parseInt(elem.getAttributeNS("interpol", "x1")),
            y1 = parseInt(elem.getAttributeNS("interpol", "y1")),
            x2 = parseInt(elem.getAttributeNS("interpol", "x2")),
            y2 = parseInt(elem.getAttributeNS("interpol", "y2")),
            steps = parseInt(elem.getAttributeNS("interpol", "steps")),
            step = parseInt(elem.getAttributeNS("interpol", "s"));
        if (step >= steps || step == undefined || !steps) {
            elem.setAttributeNS(null, "interpol", undefined);
        } else
            elem.setAttributeNS("interpol", "s", step + 1);

        var x = x1 + (x2 - x1) / steps * step;
        var y = y1 + (y2 - y1) / steps * step;
        elem.setAttributeNS(null, "transform", "translate(" + (x) + "," + (y) + ")")

    }
}

function Client() {

}

var exportOnConnect = undefined, buildOnConnect = true, queue=[];

Client.prototype.openConnection = function (url) {
    this.ws = new WebSocket(url);
    this.connected = false;
    this.ws.onmessage = this.onMessage.bind(this);
    this.ws.onerror = this.displayError.bind(this);
    this.ws.onclose = this.displayClose.bind(this);

    this.ws.onopen = this.connectionOpen.bind(this);
};

Client.prototype.connectionOpen = function () {
    this.connected = true;
    console.log("connected");
    //getComponents();
    if (exportOnConnect)
        exportToWS();
    exportOnConnect = undefined;
    while (queue.length>0){
        this.ws.send(queue[0]);
        queue.splice(0,1);
    }
};

Client.prototype.onMessage = function (message) {
    try {
        var data = JSON.parse(message.data);
        if (data instanceof Array) {
            data.forEach(function (d) {
                handleData(d);
            });
        } else {
            handleData(data);
        }
    } catch (e) {
        console.log(message.data);
        console.log(e);
    }
};

Client.prototype.displayError = function (err) {
    console.log('Websocketerror: ' + err);
    this.connected = false;
    console.log("Error with the WS");
    if (!doClose)
        window.location.href = "index.php?websocketerror";
};

Client.prototype.displayClose = function (err) {
    console.log('Websocketerror: ' + err);
    this.connected = false;
    console.log("Closed the WS");
    if (!doClose)
        window.location.href = "index.php?websocketclose";
};

doClose = true;

window.setInterval(interpolateText, 100);