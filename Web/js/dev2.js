/**
 * Created by alex on 10/31/15.
 */



var tree = {
    action: 'set',
    data: 'a 10',
    children: [{action: 'add', data: 'b a', children: [], cc: 0}, {
        action: 'mult',
        data: 'b a',
        children: [],
        cc: 0
    }],
    cc: 0
};

function render() {
    var table = $("#ptable")
    table.empty();

    rP(0, tree, undefined, table);

    $("<tr/>").append(
        $("<td/>"))
        .append($("<td/>"))
        .append($("<td/>"))
        .append($("<td/>", {class: 'linked bottom', html: "+", title: 'Add a row at the end'}))
        .append($("<td/>"))
        .appendTo(table);
}

function rP(index, t, tp, table) {
    $("<tr/>").attr("index", index).
        append(
        $("<td/>", {
            class: index > 0 && tp.cc > 0 ? 'linked left' : 'disabled',
            html: '&lt;',
            title: 'Go one children to the left'
        })
    ).append(
        $("<td/>", {
            class: 'info',
            html: t.action + "<br/>" + t.data,
            title: 'Click to edit'
        })
    ).append(
        $("<td/>", {
            class: 'shorts',
            html: (t.action in shorts ? shorts[t.action] : "")
        })
    ).append(
        $("<td/>", {
            class: 'actions rem',
            html: '-',
            title: 'Click to delete this node \n Shift click to delete this node and its children'
        })
    ).append(
        $("<td/>", {
            class: index == 0 ? "disabled right" : tp.cc < tp.children.length - 1 ? 'linked right' : 'add right',
            html: index == 0 ? "_" : tp.cc < tp.children.length - 1 ? '&gt;' : '+',
            title: 'Click to go the the next node or add one \n Shift + click to insert a node below'
        })
    )
        .appendTo(table);
    if (t.cc in t.children)
        rP(index + 1, t.children[t.cc], t, table);

}
var tC, tCi;

$(function () {
    render();
    rI();
    showStored();

    $("#actionI").chosen({
        no_results_text: 'No action found',
        width: '100%'
    });

    $("#ptable").on("click", ".linked.left", function () {
        setTo(tree, $(this).parent().attr("index") - 1, -1);
        rI();
        render();
    });
    $("#ptable").on("click", ".right", function (e) {
        if (e.shiftKey) { // below
            var index = $(this).parent().attr("index");
            var d = goTo(tree, index);
            var e = {action: 'unknown', data: '&nbsp;', cc: 0, children: d.children};
            d.children = [e];
        } else if ($(this).hasClass("linked"))
            setTo(tree, $(this).parent().attr("index") - 1, 1);
        rI();
        render();
    });
    $("#ptable").on("click", ".linked.bottom", function (e) {
        rI();
        addToLast(tree);
        render();
    });
    $("#ptable").on("click", ".add.right", function (e) {
        if (e.shiftKey) { // below
            var index = $(this).parent().attr("index");
            var d = goTo(tree, index);
            var e = {action: 'unknown', data: '&nbsp;', cc: 0, children: d.children};
            d.children = [e];
        } else
            addTo(tree, $(this).parent().attr("index") - 1);
        rI();
        render();
    });
    $("#ptable").on("click", ".actions.rem", function (e) {
        var index = $(this).parent().attr("index");
        if (index == 0)return;
        var d = goTo(tree, index - 1);
        var t = goTo(d, 1);

        index = d.children.indexOf(t);
        if (index < 0)return;
        console.log(t.children);
        if (!e.shiftKey)
            d.children = d.children.concat(t.children);

        d.children.splice(index, 1);

        rI();
        render();
    });
    $("#ptable").on("click", ".info", function (e) {
        tCi = $(this).parent().attr("index");
        tC = goTo(tree, tCi);
        startChange(tC)
    });

    $('#actionI').on('change', function (e, p) {
        uI(this.value);
    });

    $('#storedAIs').on('click', "[treename] button", function (e, p) {
        var name = $(this).parent().attr("treename");
        if ($(this).hasClass("open")) {
            var all = (localStorage.storedAIs) ? JSON.parse(localStorage.storedAIs) : {};
            if (name in all) {
                loadJSON(all[name]);
                cname = name;
            }
        }else if ($(this).hasClass("clone")) {
            var all = (localStorage.storedAIs) ? JSON.parse(localStorage.storedAIs) : {};
            if (name in all) {
                var n = prompt("Please enter the new name");
                if (n == undefined || n == null)
                    return;
                all[n] = all[name];
                localStorage.storedAIs = JSON.stringify(all);
                loadJSON(all[n]);
                cname = n;
                showStored();
            }
        }else if ($(this).hasClass("rem")) {
            var all = (localStorage.storedAIs) ? JSON.parse(localStorage.storedAIs) : {};
            if (!confirm("Do you want to delete " + name))return;
            if (name in all) {
                delete all[name];
                localStorage.storedAIs = JSON.stringify(all);
                cname = undefined;
                rI();
                tree = {
                    action: 'set',
                    data: 'empty',
                    children: [],
                    cc: 0
                }
                render();
                showStored();
            }
        }

    });


})

function addToLast(t) {
    if (t.children.length == 0)
        t.children.push({action: 'unknown', data: '&nbsp;', cc: 0, children: []})
    else
        addToLast(t.children[t.cc]);
}

function setTo(t, left, n) {
    if (left < 1) {
        t.cc += n;
    } else {
        setTo(t.children[t.cc], left - 1, n);
    }
}

function goTo(t, left) {
    if (left < 1) {
        return t;
    } else {
        return goTo(t.children[t.cc], left - 1);
    }
}

function addTo(t, left) {
    if (left < 1) {
        t.children.push({action: 'unknown', data: '&nbsp;', cc: 0, children: []})
        t.cc = t.children.length - 1;
    } else {
        addTo(t.children[t.cc], left - 1);
    }
}

function simp(t) {
    var r = {params: (t.action.replace("__", " ") + " " + t.data).replace("  "," ").split(" "), children: []};
    $.each(t.children, function (i, v) {
        r.children.push(simp(v));
    });
    return r;
}
function makeNode(jo) {
    var e;
    if (jo.params[0]=="if"||jo.params[0]=="ifnot"){
        e = {
            action: jo.params.length == 0 ? "unknown" :  jo.params[0]+"__"+jo.params[1],
            data: jo.params.length < 3 ? "" : ( jo.params.splice(2, jo.params.length).join(" ")),
            children: [],
            cc: 0
        };
    }else
    e = {
        action: jo.params.length == 0 ? "unknown" :  jo.params[0],
        data: jo.params.length < 2 ? "" : ( jo.params.splice(1, jo.params.length).join(" ")),
        children: [],
        cc: 0
    };
    $.each(jo.children, function (i, v) {
        e.children.push(makeNode(v));
    });
    return e;
}

function outputJSON() {
    var r = {rootNode: simp(tree)};
    $("#json").val(JSON.stringify(r));
}

var cname = undefined;

function store() {

    if (cname == undefined) {
        var n = prompt("Please enter a name");
        if (n == undefined || n == null)
            return;
        cname = n;
    }
    var all = (localStorage.storedAIs) ? JSON.parse(localStorage.storedAIs) : {};
    all[cname] = JSON.stringify({rootNode: simp(tree)});

    localStorage.storedAIs = JSON.stringify(all);
    showStored();
}

function showStored() {
    var ul = $("#storedAIs");
    ul.empty();
    if (localStorage.storedAIs) {
        var all = JSON.parse(localStorage.storedAIs);

        $.each(all, function (name, val) {
            ul.append(
                $("<li/>", {html: name, class: 'loadStored', attr: {treename: name}}).append(
                    $("<button/>", {text:'Open', class:'open'})
                ).append(
                    $("<button/>", {text:'Clone', class:'clone'})
                ).append(
                    $("<button/>", {text:'Remove', class:'rem'})
                )
            );
        });
    }

}

function loadJSON(s) {
    cname = undefined;
    var r = JSON.parse(s)
    tree = makeNode(r.rootNode);
    render();
    rI();
    return false;
}


function startChange(t) {
    $("#actionI").val(t.action).trigger("chosen:updated");
    $("#dataI").val(t.data);
    uI(t.action);
}

function updateD() {
    if (tCi == undefined)return;
    tC.action = $("#actionI").val();
    tC.data = $("#dataI").val();
    render();
}

function rI() {
    tCi = undefined;
    $("#actionI").val([]).trigger("chosen:updated")
    $("#dataI").val("");

}

function delD() {
    if (tCi == undefined)return;
    var p = goTo(tree, tCi - 1);
    var index = p.children.indexOf(tC);
    p.children.splice(index, 1);
    rI();
    render();
}


function uI(val) {
    $("#paraInfo").html(val in ahelper ? ahelper[val][0] : "no documentation found").
        prop("title", val in ahelper ? ahelper[val][1] : "no documentation found");
}

