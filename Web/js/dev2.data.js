/**
 * Created by alex on 10/31/15.
 */

var ahelper = {
    set: ["<b>[result]</b> = <b>[value]</b>]", "Sets [result] to [value]"],

    add: ["<b>[result]</b> = <b>[value1]</b>] + <b>[value2]</b>]", "Sets [result] to the sum of [value1] and [value2]"],
    sub: ["<b>[result]</b> = <b>[value1]</b>] - <b>[value2]</b>]", "Sets [result] to the difference of [value1] and [value2]"],
    mult: ["<b>[result]</b> = <b>[value1]</b>] * <b>[value2]</b>]", "Sets [result] to the product of [value1] and [value2]"],
    div: ["<b>[result]</b> = <b>[value1]</b>] / <b>[value2]</b>]", "Sets [result] to the quotient of [value1] and [value2]"],

    addself: ["<b>[var1]</b> = <b>[var1]</b>] + <b>[value2]</b>]", "Sets [var1] to the sum of [var1] and [value2]"],
    subself: ["<b>[var1]</b> = <b>[var1]</b>] - <b>[value2]</b>]", "Sets [var1] to the difference of [var1] and [value2]"],
    multself: ["<b>[var1]</b> = <b>[var1]</b>] * <b>[value2]</b>]", "Sets [var1] to the product of [var1] and [value2]"],
    divself: ["<b>[var1]</b> = <b>[var1]</b>] / <b>[value2]</b>]", "Sets [var1] to the quotient of [var1] and [value2]"],

    greatest: ["<b>[result]</b> = <b>[var1]</b>...<b>[varN]</b>]", "Sets [result] to greatest amount of [var1] .. [varN]"],


    max: ["<b>[result]</b> = MAX(<b>[array]</b>])", "Sets [result] to the highest value in [array]"],
    maxindex: ["<b>[result]</b> = MAXINDEX(<b>[array]</b>])", "Sets [result] to the index of the highest value in [array]"],
    get: ["<b>[result]</b> = <b>[array][[index]]</b>]", "Sets [result] to the value [index] in [array]"],

    in_color: ["<b>[result]</b> ", "Sets [result] to an array with n entries, where every value represents the colors of a sector\n n varies from camera to camera"],
    in_dist: ["<b>[result]</b> ", "Sets [result] to an array with n entries, where every value represents the distance in a sector \n n varies from radar to radar"],
    in_health: ["<b>[result]</b> ", "Sets [result] to the current health of the drone"],
    in_velocity: ["<b>[direction] [speed]</b> ", "Sets [direction] to the current heading in degrees and [speed] to current speed"],
    in_waypoint: ["<b>[direction] [distance]</b> ", "Sets [direction] to the direction the stored waypoint is in \n [distance] is the distance \n Both results are arrays. Every value is another waypoint"],
    in_capacity: ["<b>[result]</b> ", "Sets [result] to the percentage of cargo"],

    out_cannon: ["<b>[direction]</b> ", "Fires once towards [direction] degrees if [direction] is greater than 0. "],
    out_thrusters: ["<b>[direction] [strength]</b> )", "Sets the thrusters direction to [direction] degrees \n if [strength] is greater than 0 "],
    out_color: ["<b>[color]</b> ", "Sets the drones color to [color] "],
    out_waypoint: ["<b>[number]</b>", "Sets the drones waypoint [number] to its current position"]


}

var shorts = {
    set: "[=]",
    add: "[+]",
    sub: "[-]",
    mult: "[*]",
    div: "[/]",
    addself: "[+=]",
    subself: "[-=]",
    multself: "[*=]",
    divself: "[/=]"

}