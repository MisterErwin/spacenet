/**
 * Created by alex on 11/10/15.
 */

var allowsPort = 1, allowsConstant = 2, required = 4, trigger = 8;

var allComponents = {
    group: 2,
    children: {}
};
//var x={
//    group: 2,
//    children: {
//        core: {
//            title: 'Core functions', group: true, children: {
//                kick: {
//                    title: 'Kick', groups: 'core', inports: [], outports: [{title: 'impulse'}], col: "red"
//                },
//                wait: {
//                    title: 'Wait', groups: 'core', col: 'orange', inports: [
//                        {title: 'delay \n (And trigger)', f: allowsPort | allowsConstant | required | trigger},
//                        {title: 'start', f: allowsPort | trigger}
//                    ], outports: [{title: 'fire \n(Value is the value triggered with)'}]
//                },
//                condition: {
//                    title: 'IF', groups: 'core condition', col: 'aqua', inports: [
//                        {title: 'value A', f: allowsPort | allowsConstant | required | trigger},
//                        {
//                            title: 'condition',
//                            f: allowsConstant | required,
//                            values: ['==', '!=', '>', '<', '>=', '<='],
//                            col: 'blue'
//                        },
//                        {title: 'value B', f: allowsPort | allowsConstant | required | trigger},
//                        {title: 'clear', f: allowsPort}
//                    ], outports: [
//                        {title: 'if \n(Value is the value triggered with)', col: 'green'},
//                        {title: 'else \n(Value is the value triggered with)', col: 'red'}
//                    ]
//                }
//            }
//        },
//        math: {
//            title: 'Math functions', group: true, children: {
//                add: {
//                    title: 'Add', groups: "arithmetic", inports: [
//                        {title: 'addend1', f: allowsPort | allowsConstant | required | trigger},
//                        {title: 'addend2', f: allowsPort | allowsConstant | required | trigger},
//                        {title: 'clear', f: trigger}
//                    ], outports: [{title: 'sum'}]
//                },
//                sub: {
//                    title: 'Sub', groups: "arithmetic", inports: [
//                        {title: 'minuend', f: allowsPort | allowsConstant | required | trigger},
//                        {title: 'subtrahend', f: allowsPort | allowsConstant | required | trigger},
//                        {title: 'clear', f: trigger}
//                    ], outports: [{title: 'difference'}]
//                },
//                mult: {
//                    title: 'Mult', groups: "arithmetic", inports: [
//                        {title: 'multiplier', f: allowsPort | allowsConstant | required | trigger},
//                        {title: 'multiplicand', f: allowsPort | allowsConstant | required | trigger},
//                        {title: 'clear', f: trigger}
//                    ], outports: [{title: 'product'}]
//                },
//                div: {
//                    title: 'Div', groups: "arithmetic", inports: [
//                        {title: 'dividend', f: allowsPort | allowsConstant | required | trigger},
//                        {title: 'divisor', f: allowsPort | allowsConstant | required | trigger},
//                        {title: 'clear', f: trigger}
//                    ], outports: [{title: 'quotient'}]
//                }
//            }
//        }
//    }
//};

var coords = undefined;

function createComp(name) {
    document.getElementById("selComp").style.visibility = "hidden";
    var comp = getComponent(name);
    console.log(comp, name);
    var d = {
        id: -1,
        x: coords[0],
        y: coords[1],
        type: name,
        oP: newMap(comp.outports.length),
        iP: newMap(comp.inports.length)
    };
    console.log(name, d.oP);
    d.id = data.push(d) - 1;


    buildComp(d);
}


function getComponent(name, a) {
    if (a == undefined)a = allComponents;
    if ("group" in a) {
        return getComponent(name, a.children);
    }
    for (var key in a) {
        if (key == name && !("group" in a[key]))
            return a[key];
        else if ("group" in a[key]) {
            var r = getComponent(name, a[key]);
            if (r)return r;
        }
    }
    return undefined;
}

function newCompMenu(x, y) {
    var s = document.getElementById("selComp");
    coords = [x, y];
    s.style.left = x + "px";
    s.style.top = y + "px";
    s.style.visibility = "visible";
    document.getElementById("selCompIn").value = "";
    compSelAutoSel("");
}


function compSelAutoSel(str) {
    if (typeof str === "undefined") {
        setTimeout(function () {
            document.getElementById("livesearch").innerHTML = "";
            document.getElementById("livesearch").style.border = "0px";
        }, 100);
        return;
    }
    document.getElementById("livesearch").style.border = "1px solid #A5ACB2";
    var html = "";
    for (var k in allComponents.children) {
        var o = allComponents.children[k];
        var h = addCompToSel(k, o, str);
        if (h)
            html += h;
    }
    document.getElementById("livesearch").innerHTML = html;

}

function addCompToSel(name, o, str) {
    var html = "";
    var isIn = str.length == 0 || o.title.toLowerCase().indexOf(str) > -1 || ("groups" in o && o.groups.indexOf(str) > -1);
    //console.log(o.title, str, "|", str.length == 0 , o.title.toLowerCase().indexOf(str) > -1 , ("groups" in o && o.groups.indexOf(str) > -1));


    if ("group" in o) {
        for (var k in o.children) {
            var r = addCompToSel(k, o.children[k], isIn ? "" : str);
            if (r)
                html += r;
        }
        if (html.length > 1)
            return "<div class='selGroup'><b>" + o.title + "</b><br/>" + html + "</div>";
        else return undefined;
    } else if (!isIn)
        return undefined;
    return "<div class='selItem' onclick=\"createComp('" + name + "')\"><u>" + o.title + "</u></div>";

    //return "<div class='selItem' onclick=\"document.getElementById('selCompIn').value='" + name + "'\"><u>" + o.title + "</u></div>";
}

function build() {
    try {
        data.forEach(buildComp);
        //Node connections
        data.forEach(function (d) {
            for (var p in d.oP) {
                p = parseInt(p);
//                    var op = {0: [otherID, portNR]};
                d["oP"][p].forEach(function (pd) {
                    var other = getDID(pd[0]);
                    if (!("iP" in other)) {
                        other.iP = newMap(getComponent(other.type).inports.length/*other.iPN*/);
                    }
                    other.iP[pd[1]].push([d.id, p]);
                    updateConLine(d.id, p, pd[0], pd[1]);

                });
            }
        });

        document.getElementsByTagName("svg")[0].setAttributeNS(null, "onmousemove", "svgMove(evt)");
    }catch(e){
        console.log(e);
        if (confirm("An error occured. \n Delete cache?")){
            window.localStorage.removeItem("storedAIs");
            window.location.reload();
        }
    }
}

function delComp(id) {
    console.log("deleting comp ", id);

    var comp = data[id];
    if ("oP" in comp) {
        for (var port in comp.oP) {
            var portCons = comp.oP[port];
            portCons.forEach(function (ref) {
                data[ref[0]].iP[ref[1]].forEach(function (ref2, index, arr) {
                    if (ref2[0] == id && ref2[1] == port) {
                        arr.splice(index, 1);
                        delcon("con_" + id + "/" + port + "_" + ref[0] + "/" + ref[1], true);
                    }
                });
            });
        }
    }
    if ("iP" in comp) {
        for (var port in comp.iP) {
            var portCons = comp.iP[port];
            portCons.forEach(function (ref) {
                if (ref[0] >= 0) {
                    data[ref[0]].oP[ref[1]].forEach(function (ref2, index, arr) {
                        if (ref2[0] == id && ref2[1] == port) {
                            arr.splice(index, 1);
                            delcon("con_" + ref[0] + "/" + ref[1] + "_" + id + "/" + port, true);
                        }
                    });
                }
            });
        }
    }



    var n = document.querySelectorAll("g[data-id='" + id + "']")[0];
    n.parentNode.removeChild(n);
    data.splice(id, 1);



    data.forEach(function (compD, index) {
        //console.log("||", compD.id, index, "----", id, compD.type);
        var oldIndex = compD.id;
        if (compD.id > index) {
            compD.id = index;
            //console.log(oldIndex + "--->" + index);
            document.querySelectorAll("g[data-id='" + oldIndex + "']")[0].setAttributeNS(null, "data-id", index);
            console.log("Set index of " + oldIndex + " to " + index);
        }
        if ("oP" in compD) {
            for (var port in compD.oP) {
                var portCons = compD.oP[port];
                portCons.forEach(function (ref) {
                    //console.log(compD.id + "(" + port + ") ->", ref);
                    var oldRID = ref[0];
                    if (ref[0] > id || oldIndex != index) {
                        if (ref[0]>id)
                            ref[0] = ref[0] - 1;
                        //console.log(index, compD.id, "change", ref[0] + 1, "-->", ref[0], " on port", port + "/" + ref[1]);
                        document.getElementById("con_" + oldIndex + "/" + port + "_" + oldRID + "/" + ref[1]).
                            setAttribute("id", "con_" + index + "/" + port + "_" + ref[0] + "/" + ref[1]);
                        var otherArray = data[ref[0]].iP[ref[1]];
                        otherArray.forEach(function (oRef) {
                            if (oRef[0] == oldIndex && oRef[1] == port)
                                oRef[0] = index;
                        })
                    }
                });
            }
        }
    });
}


function setConstant(id, port, val) {
    console.log("setConstant", id, port, val);

    var cD = getComponent(data[id].type);

    var pp = cD.inports[port];
    if (!val) {
        if (pp && "values" in pp) {
            var v = undefined;
            if (data[id].iP[port][0][0] == -1)
                v = data[id].iP[port][0][1];
            console.log(v,val, data[id].iP[port]);
            showSelectPrompt(pp.values, v, function (val) {
                //alert(val);
                setConstant(id, port, val);
            });
            return;
        } else {
            var currentValue = (data[id].iP[port][0] && data[id].iP[port][0][0] == -1) ? data[id].iP[port][0][1] : "/";

            var val = prompt("Please enter a constant", currentValue);
            if (!val)return;
        }
    }

    if (pp && "values" in pp) {
        if (pp.values.indexOf(val) == -1) {
            alert("Invalid value " + val);
            return;
        }
    }


    //port = parseInt(port);

    //reset old outports
    data[id].iP[port].forEach(function (d) {
        if (d[0] >= 0) {
            var a = data[d[0]].oP[d[1]];
            for (var i = 0; i < a.length; ++i)
                if (a[i][0] == id && a[i][1] == port) {
                    delcon("con_" + d[0] + "/" + d[1] + "_" + id + "/" + port);
                    a.splice(i, 1);
                    return false;
                }
        }
    });

    data[id].iP[port] = [[-1, val]];
    var p = document.querySelectorAll("g[data-id='" + id + "'] > g.inports :nth-child(" + ( port + 1) + ")")[0];
    console.log("g[data-id='" + id + "'] > g.inports :nth-child(" + (port + 1) + ")");
    //p.setAttribute("title", cD.title + "\nValue:" + val);
    setTitle(p, cD.title + "\nValue:" + val);
    p.setAttribute("data-val", val);
}

function buildComp(comp) {

    var svg = document.getElementById("isvg");
    var g = document.createElementNS(NSN, "g");
    svg.appendFirst(g);

    var cI = getComponent(comp.type);
    if (!cI) {
        console.log("Error in data", "Cannot find:" , comp.type);
        return;
    }
    g.setAttributeNS(null, "transform", "matrix(1 0 0 1 " + comp.x + " " + comp.y + ")");
    g.setAttributeNS(null, "data-id", comp.id);

    if (comp.type == "struct") {
        g.setAttributeNS(null, "class", "component struct");
        var e = document.createElementNS(NSN, "circle");
        e.setAttributeNS(null, "fill", "col" in cI ? cI.col : "black");
        e.setAttributeNS(null, "r", "25");
        var pg = document.createElementNS(NSN, "g");
        var p = document.createElementNS(NSN, "circle");
        p.setAttributeNS(null, "fill", "gray");
        p.setAttributeNS(null, "r", "10");
        p.setAttributeNS(null, "class", "port port-allowsPort port-struct");
        g.appendChild(e);
        pg.appendChild(p);
        g.appendChild(pg);

        /*
         <circle r="20" fill="gray"
         class="port port-allowsPort port-struct"
         ><title>Connect anything to me</title></circle>

         */

        return;
    }

    g.setAttributeNS(null, "class", "component");
    var e = document.createElementNS(NSN, "rect");
    e.setAttributeNS(null, "fill", "col" in cI ? cI.col : "green");
    e.setAttributeNS(null, "width", "100");
    e.setAttributeNS(null, "height", "100");
    e.setAttributeNS(null, "ry", "5");
    e.setAttributeNS(null, "rx", "20");

    g.appendChild(e);
    //
    e = document.createElementNS(NSN, "text");
    e.setAttributeNS(null, "y", "10");
    e.appendChild(document.createTextNode(cI.title));
    g.appendChild(e);
    //
    e = document.createElementNS(NSN, "image");
    e.setAttributeNS("xlink", "href", "http://placehold.it/30x30");
    e.setAttributeNS(null, "width", "30px");
    e.setAttributeNS(null, "height", "30px");
    g.appendChild(e);
    //
    e = document.createElementNS(NSN, "g");
    e.setAttributeNS(null, "class", "inports");
    for (var i = 0; i < cI.inports.length; ++i) {
        var c = document.createElementNS(NSN, "circle");
        var pI = cI.inports[i];
        c.setAttributeNS(null, "r", 5);
        c.setAttributeNS(null, "fill", ("col" in pI) ? pI.col : (pI.f & required ? "red" : "gray"));
        var cl = "port port-in";
        if ("f" in pI) {
            if (pI.f & trigger) cl += " port-trigger";
            if (pI.f & required) cl += " port-required";
            if (pI.f & allowsConstant) cl += " port-allowsConstant";
            if (pI.f & allowsPort) cl += " port-allowsPort";
        }

        c.setAttributeNS(null, "class", cl);
        //c.setAttributeNS(null, , pI.title);

        var t = document.createElementNS(NSN, "title");
        t.appendChild(document.createTextNode(pI.title));
        c.appendChild(t);

        e.appendChild(c);
    }
    g.appendChild(e);
    //
    e = document.createElementNS(NSN, "g");
    e.setAttributeNS(null, "class", "outports");

    for (var i = 0; i < cI.outports.length; ++i) {
        var c = document.createElementNS(NSN, "circle");
        pI = cI.outports[i];
        c.setAttributeNS(null, "r", 5);
        c.setAttributeNS(null, "fill", "col" in pI ? pI.col : "gray");
        var cl = "port port-out";
        if ("f" in pI) {
            if (pI.f & trigger) cl += " port-trigger";
            if (pI.f & required) cl += " port-required";
            if (pI.f & allowsConstant) cl += " port-allowsConstant";
            if (pI.f & allowsPort) cl += " port-allowsPort";
        }
        c.setAttributeNS(null, "class", cl);
        //c.setAttributeNS(null, "title", pI.title);

        var t = document.createElementNS(NSN, "title");
        t.appendChild(document.createTextNode(pI.title));
        c.appendChild(t);

        e.appendChild(c);
    }

    g.appendChild(e);


    updateNodes(comp.id);
}

function updateAllNodes() {
    var n = document.querySelectorAll("#isvg > [data-id]");
    for (var i = 0; i < n.length; ++i)
        updateNodes(n[i].getAttribute("data-id"));
}

function updateNodes(compID) {
    console.log("updating", compID);

    var n = document.querySelectorAll("#isvg > [data-id='" + compID + "'] > g.inports > .port");
    for (var i = 0; i < n.length; ++i)
        n[i].setAttribute("transform", "matrix(1 0 0 1 0 " + 100 / (n.length + 1) * (i + 1) + ")");
    n = document.querySelectorAll("#isvg > [data-id='" + compID + "'] > g.outports > .port");
    for (var i = 0; i < n.length; ++i)
        n[i].setAttribute("transform", "matrix(1 0 0 1 100 " + 100 / (n.length + 1) * (i + 1) + ")");

}

