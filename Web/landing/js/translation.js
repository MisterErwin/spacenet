/**
 * Created by L�pges on 27.02.2016.
 */

function loadTranslation(language) {
    if (typeof language != 'object') {
        if (typeof language === 'undefined' || language == undefined)
            language = 'de';

        $.getJSON("lang/" + language + ".json")
            .done(loadTranslation)
            .fail(function () {
                console.log("Could not load translation file");
                loadTranslation({});
            });
        return;
    }

    console.log(language);
    $("[data-trans]").each(function () {
        var l = getChildLang($(this).data('trans'), language);
        if (l == undefined)
            $(this).text("{" + $(this).data('trans') + "}");
        else
            $(this).html(l);
    });

}

function getChildLang(key, lang) {
    if (lang == undefined)return undefined;
    if (key in lang)
        return lang[key];
    var x = key.split(".");
    if (x.length == 1)
        return undefined;
    key = x.shift();
    return getChildLang( x.join("."), lang[key]);

}