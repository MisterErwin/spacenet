 <?php
require_once("connect.inc.php");

if (isset($_REQUEST['works'])) {
    con();

    mysql_query("UPDATE sn_server SET WORKS = WORKS + 2 WHERE url='" . addslashes($_REQUEST['failed']) . "' AND WORKS < 10 ") OR DIE ("++update failed");

    die($_REQUEST['works'] . "works");
} else if (isset($_REQUEST['failed'])) {
    con();
    mysql_query("UPDATE sn_server SET WORKS = WORKS - 1 WHERE url='" . addslashes($_REQUEST['failed']) . "'") OR DIE ("-update failed");
    mysql_query("DELETE FROM sn_server WHERE WORKS < 0") OR DIE ("del failed");

    die($_REQUEST['failed'] . "failed");
}
?>
<html>
<head>
    <script>
        function load() {
            var pinger = new WSPinger();
            pinger.next();
        }
        function WSPinger() {
            this.all = ["ws://localhost:8080", "ws://localhost:8123"];

        }
        WSPinger.prototype.next = function () {
            if (this.all.length == 0) {
                console.log("All tested");
                return;
            }
            console.log(this.all[0]);
            try {
                this.ws = new WebSocket(this.all[0]);
            } catch (e) {
            }
            this.ws.onerror = this.conFail.bind(this);
            this.ws.onopen = this.conSuc.bind(this);
            var that = this;
            this.timeout = setTimeout(function () {
                this.conFail();
            }, 1800);
        };

        WSPinger.prototype.conFail = function () {
            clearTimeout(this.timeout);
            this.ws.close();
            console.log(this.all[0] + " failed");
            callPHP('failed=' + this.all[0]);
            this.all.shift();
            this.next();
        };
        WSPinger.prototype.conSuc = function () {
            clearTimeout(this.timeout);
            this.ws.close();
            console.log(this.all[0] + " works");
            callPHP('works=' + this.all[0]);
            this.all.shift();
            this.next();
        };
        function callPHP(params) {
            var httpc = new XMLHttpRequest(); // simplified for clarity
            var url = "pingServer.php";
            httpc.open("POST", url, true); // sending as POST

            httpc.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            httpc.setRequestHeader("Content-Length", params.length); // POST request MUST have a Content-Length header (as per HTTP/1.1)

            httpc.onreadystatechange = function () { //Call a function when the state changes.
                if (httpc.readyState == 4 && httpc.status == 200) { // complete and no errors
                    alert(httpc.responseText); // some processing here, or whatever you want to do with the response
                }
            }
            httpc.send(params);
        }
    </script>
</head>
<body onload="load()"></body>
</html>