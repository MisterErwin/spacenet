<?php
require_once("php/connect.inc.php");
con();
?>
<!DOCTYPE html>
<html>
<w lang="en">
    <meta charset="UTF-8">
    <title></title>
</w>
<body>
<h1>SpaceNET Server Selection</h1>
<?php
if (isset($_GET['websocketerror'])) {
    ?>
    <p style="background-color: orangered"><b>An error occurred with the server connection</b></p>
    <?php
}else if (isset($_GET['websocketclose'])) {
    ?>
    <p style="background-color: orangered"><b>The server connection was closed</b></p>
    <?php
}

?>


<table>
    <thead>
    <tr>
        <th>#</th>
        <th>URL</th>
        <th>Provided by</th>
        <th>Last ping</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $res = mysql_query("SELECT url, owner,port, WORKS, TIMESTAMPDIFF(MINUTE,last_sucessfull_ping,last_ping) as ping FROM sn_server");
    $i = 1;
    while ($row = mysql_fetch_object($res)) {
        $url = $row->url . ":" . $row->port;
        $online = $row->ping < 5 ? "<5 min": $row->ping . " minutes ago";
        $online = $row->ping < 30?$online : ">30 min ago";
        echo "<tr><td>$i</td><td class='urltd'><a href='game.html?server=$url'>$url</a></td><td>$row->owner</td><td>$online</td></tr>";
        $i++;
    }
    ?>
    </tbody>
</table>
<hr>
<button onclick="checkServer()">Ping servers</button>
<hr>
No suitable server found? </br>
<a href="javascript:void(0);" title="Comming soon(tm)">Host your own one!</a>
<script>
    function checkServer() {
        var cusid_ele = document.getElementsByClassName('urltd');
        for (var i = 0; i < cusid_ele.length; ++i) {
            var item = cusid_ele[i];
            try {
                var ws = new WebSocket("ws://" + item.children[0].innerHTML);
                var tt = setTimeout(function () {
                    ws.close();
                    item.parentElement.style.background = 'orange';
                    item.children[0].hrefOld = item.children[0].href;
                    item.children[0].href = 'javascript:void(0);';
                }, 30000);
                ws.onerror = function () {
                    clearTimeout(tt);
                    item.parentElement.style.background = 'red';
                    item.children[0].hrefOld = item.children[0].href;
                    item.children[0].href = 'javascript:void(0);';
                    ws.close();
                }
                ws.onopen = function () {
                    clearTimeout(tt);
                    item.parentElement.style.background = 'green';
                    if ("hrefOld" in  item.children[0])
                        item.children[0].href = item.children[0].hrefOld;

                    ws.close();
                }

            } catch (e) {
            }
        }
    }

</script>
</body>
</html>